Example of RTS resource gathering

Left click to select an unit, and right click on a resource to start harvest.

The unit will automatically search another similar resource (in a range) when the previous is destroyed.