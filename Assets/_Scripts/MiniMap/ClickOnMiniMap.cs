﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickOnMiniMap : MonoBehaviour, IPointerClickHandler
{
    public RectTransform minimapCameraRectTransform;
    public Camera mainCamera;
    public RTS_Cam.RTS_Camera rtsCamera;
    public GameObject pinkPixel;
    public void OnPointerClick(PointerEventData eventData)
    {
        /*Vector2 localClick;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(textureRectTransform, eventData.position, Camera.main, out localClick);*/
        // RectTransform minimapCameraRectTransform = minimapCamera.GetComponent<RectTransform>();
        RectTransformUtility.ScreenPointToLocalPointInRectangle(minimapCameraRectTransform, eventData.position, null, out Vector2 localClick);
        localClick.y = (minimapCameraRectTransform.rect.yMin * -1) - (localClick.y * -1);

        Vector2 viewportClick = new Vector2(localClick.x / minimapCameraRectTransform.rect.xMax, localClick.y / (minimapCameraRectTransform.rect.yMin * -1));

        Debug.Log("ViewPort : " + viewportClick);
        Ray ray = mainCamera.ViewportPointToRay(new Vector3(viewportClick.x, viewportClick.y, 0));
        if (Physics.Raycast(ray, out RaycastHit hit, LayerMask.GetMask("Ground")))
        {
            Debug.Log("hit : " + hit.point) ;
            GameObject clickPoint = Instantiate(pinkPixel);
            clickPoint.transform.position = hit.point;
            rtsCamera.SetFocusPoint(new Vector3(hit.point.x, mainCamera.transform.position.y, hit.point.z));
        }

        /*Debug.Log(Input.mousePosition);
        Debug.Log(eventData.pressPosition);*/

        /*Ray topLeftCorner = this.minimapCamera.ScreenPointToRay(eventData.pressPosition);
        Vector3 topLeftPosition = ResourceManager.InvalidPosition;*/
        /*RaycastHit hit = new RaycastHit();
        if (this.floorCollider.Raycast(topLeftCorner, out hit, Mathf.Infinity))
        {
            topLeftPosition = Input.mousePosition;
            this.lastTopLeftSuccessfullPosition = hit[0].point;
        }*/
        /*Debug.Log(topLeftPosition);
        topLeftPosition = this.minimapCamera.ViewportToWorldPoint(topLeftPosition);
        Debug.Log(topLeftPosition);
        // topLeftPosition.z = -1f;

        mainCamera.SetFocusPoint(topLeftPosition);
        RaycastHit hit;
        Debug.Log("------------------ Step1 ------------------");
        Ray ray = minimapCamera.ScreenPointToRay(Input.mousePosition);
        Debug.Log("Step2");
        if (Physics.Raycast(ray, out hit))
        {
            // mainCamera.SetFocusPoint(hit.point);
            mainCamera.SetFocusPoint(new Vector3(hit.point.x, mainCamera.transform.position.y, hit.point.z));
            // itsMainCamera.transform.position = new Vector3(hit.point.x, transform.position.y, transform.position.z);
            // itsMainCamera.transform.position = hit.point;
            Debug.Log("Step3");
            // hit.point contains the point where the ray hits the
            // object named "MinimapBackground"
            Debug.Log(hit.point);
        }
        //itsMainCamera.transform.position = Vector3.Lerp(itsMainCamera.transform.position, hit.point, 0.1f);*/
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
