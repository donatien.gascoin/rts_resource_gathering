﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour
{

    Player player;

    public Renderer miniMapRenderer;
    
    public void SetPlayer(Player p)
    {
        player = p;
        ChangeMiniMapMode(MiniMapManager.getCurrentMode());
    }

    public void ChangeMiniMapMode(MiniMapMode mode)
    {
        if (player == null)
            return;

        miniMapRenderer.material = ColorManager.getMiniMaterial(player.color, mode, GameManager.getPlayerStatus(player));
    }

    public void ShowMiniMapRenderer(bool show)
    {
        if (player == null)
            return; //Non player (resource, NPC should still be visible)
        miniMapRenderer.enabled = show;
    }
}
