﻿using UnityEngine;
using UnityEngine.EventSystems;

public class CameraScreen : MonoBehaviour, IPointerClickHandler
{
    public RTS_Cam.RTS_Camera cam;
    public Camera playerCamera;
    private RectTransform _screenRectTransform;
    public RectTransform _groundRectTransform;

    private void Awake()
    {
        _screenRectTransform = GetComponent<RectTransform>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(_screenRectTransform, eventData.position, null, out Vector2 localClick);
        localClick.y = (_screenRectTransform.rect.yMin * -1) - (localClick.y * -1);

        Vector2 viewportClick = new Vector2(localClick.x / _screenRectTransform.rect.xMax, localClick.y / _screenRectTransform.rect.yMax);
        Vector3 point = new Vector3( _groundRectTransform.rect.width * viewportClick.x, cam.transform.position.y, _groundRectTransform.rect.height * viewportClick.y);

        if (eventData.button == PointerEventData.InputButton.Left)
            cam.SetFocusPoint(point);
        else if (eventData.button == PointerEventData.InputButton.Right)
            UIManager.moveSquad(point);            
    }
}
