﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ShowMiniMapObjects : MonoBehaviour, IPointerClickHandler
{

    public Sprite showObjects;
    public Sprite hideObjects;
    public Image buttonImage;
    private bool show;

    private void Start()
    {
        show = true;
        buttonImage.sprite = hideObjects;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        show = !show;
        if (show)
            buttonImage.sprite = hideObjects;
        else
            buttonImage.sprite = showObjects;
        MiniMapManager.showObjects(show);
    }
}
