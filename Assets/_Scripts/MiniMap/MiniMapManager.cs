﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MiniMapManager : MonoBehaviour
{
    public MiniMapMode mode;

    void Awake()
    {
        getCurrentMode += GetMiniMapMode;
        showObjects += ShowMiniMapObjects;
    }

    private void OnDestroy()
    {
        getCurrentMode -= GetMiniMapMode;
        showObjects -= ShowMiniMapObjects;
    }

    public delegate MiniMapMode GetCurrentMode();
    public static GetCurrentMode getCurrentMode;

    public delegate void ShowObjects(bool show);
    public static ShowObjects showObjects;

    public MiniMapMode GetMiniMapMode()
    {
        return mode;
    }

    public void ChangeMiniMapMode()
    {
        switch (mode)
        {
            case MiniMapMode.TEAM_COLOR:
                PropagateMinimapMode(MiniMapMode.EACH_COLOR);
                break;
            case MiniMapMode.EACH_COLOR:
            default:
                PropagateMinimapMode(MiniMapMode.TEAM_COLOR);
                break;
        }
    }

    public void ShowMiniMapObjects(bool show)
    {
        MiniMap[] refs = FindObjectsOfType<MiniMap>();
        if (refs == null || refs.Length == 0)
            return;
        for (int i = 0; i < refs.Length; i++)
            refs[i].ShowMiniMapRenderer(show);
    }

    public void PropagateMinimapMode(MiniMapMode mode)
    {
        this.mode = mode;
        MiniMap[] refs = FindObjectsOfType<MiniMap>();
        if (refs == null || refs.Length == 0)
            return;

        for(int i = 0; i < refs.Length; i++)
            refs[i].ChangeMiniMapMode(this.mode);

    }
}

[System.Serializable]
public enum MiniMapMode
{
    EACH_COLOR, TEAM_COLOR
}