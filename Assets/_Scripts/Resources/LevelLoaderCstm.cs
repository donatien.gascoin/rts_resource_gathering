﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class LevelLoaderCstm : MonoBehaviour
{
    private static int nextObjectId = 0;
    public List<GameObject> startupPosition;

    public Players playersContainer;
    public GameObject humanPlayerPrefab;
    public GameObject aiPlayerPrefab;

    public UIManager uiManager;
    public PointerManager pointerManager;
    public GameManager gameManager;

    public RTS_Cam.RTS_Camera cam;

    public bool isTesting;

    public VictoryCondition victoryCondition;

    public FoW.FogOfWarTeam foWTeam;

    public bool useFoW = true;
    public FoW.FogOfWarLegacy[] fowPlanes;

    public delegate bool UseFoW();
    public static UseFoW isFoW;
    
    void Awake()
    {
        isFoW += IsFoWEnabled;

        if (!Application.isEditor)
        {
            useFoW = true;
            isTesting = false;

            // Remove test players if some still remain active
            Player[] players = playersContainer.GetComponentsInChildren<Player>();
            if (players != null && players.Length > 0)
                for (int i = 0; i < players.Length; i++)
                    players[i].gameObject.SetActive(false);
        }
    }

    private void OnDestroy()
    {
        isFoW -= IsFoWEnabled;
    }

    private void Start()
    {

        SetObjectIds();
        List<Player> players = AddPlayers();
        AddVictoryConditions(players.ToArray());
        if (!useFoW)
        {
            DisableFoW();
        }
    }

    private List<Player> AddPlayers()
    {
        List<Player> gamePlayers = new List<Player>();
        if (!isTesting)
        {
            string[] players = PlayerPrefsX.GetStringArray("GameStartupDetails");
            RandomManager.Shuffle(startupPosition);
            for (int i = 0; i < players.Length; i++)
            {
                if (i < startupPosition.Count)
                {
                    PlayerDetailsJSON pd = JsonUtility.FromJson<PlayerDetailsJSON>(players[i]);
                    GameObject go;
                    if (pd.human)
                    {
                        go = Instantiate(humanPlayerPrefab, startupPosition[i].transform.position, startupPosition[i].transform.rotation, playersContainer.transform);
                    } else
                    {
                        go = Instantiate(aiPlayerPrefab, startupPosition[i].transform.position, startupPosition[i].transform.rotation, playersContainer.transform);
                    }
                    Player player = go.GetComponent<Player>();
                    player.username = pd.playerName;
                    player.color = ColorManager.MatchColor(pd.playerColor);

                    player.team = pd.playerTeam;
                    if (pd.human)
                    {
                        player.human = true;
                        uiManager.SetPlayer(player);
                        pointerManager.SetPlayer(player);
                        cam.SetCameraPosition(startupPosition[i].transform.position);
                        foWTeam.team = player.team;
                    } else
                    {
                        player.human = false;
                    }
                    gamePlayers.Add(player);
                }
            }
        } else
        {
            Player[] p = FindObjectsOfType<Player>() as Player[];
            gamePlayers = new List<Player>(p);
        }
        gameManager.SetPlayers(gamePlayers);
        return gamePlayers;
    }

    private void AddVictoryConditions(Player[] players)
    {
        List<VictoryCondition> victoryConditions = new List<VictoryCondition>();
        if (victoryCondition != null)
        {
            victoryCondition.SetPlayers(players);
            victoryConditions.Add(victoryCondition);
        } else
        {
            victoryConditions.Add(new Conquest(players));
        }
        gameManager.SetVictoryConditions(victoryConditions);
    }

    private void DisableFoW()
    {
        if (fowPlanes != null && fowPlanes.Length == 0)
            return;

        for (int i = 0; i < fowPlanes.Length; i++)
        {
            fowPlanes[i].enabled = false;
        }
    }

    public int GetNewObjectId()
    {
        nextObjectId++;
        if (nextObjectId >= int.MaxValue) nextObjectId = 0;
        return nextObjectId;
    }

    private void SetObjectIds()
    {
        WorldObject[] worldObjects = GameObject.FindObjectsOfType(typeof(WorldObject)) as WorldObject[];
        foreach (WorldObject worldObject in worldObjects)
        {
            worldObject.ObjectId = nextObjectId++;
            if (nextObjectId >= int.MaxValue) nextObjectId = 0;
        }
    }

    public bool IsFoWEnabled()
    {
        return useFoW;
    }
}
