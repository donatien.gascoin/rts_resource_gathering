﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectList : MonoBehaviour
{
    public GameObject[] buildings;
    public GameObject[] units;
    public GameObject[] upgrades;
    public GameObject[] worldObjects;
    public GameObject player;
    public Texture2D[] avatars;

    private static bool created = false;

    void Awake()
    {
        ResourceManager.SetGameObjectList(this);
        /*if (!created)
        {
            // DontDestroyOnLoad(transform.gameObject);
            *//*PlayerManager.Load();
            PlayerManager.SetAvatarTextures(avatars);*//*
            created = true;
        }
        else
        {
            Destroy(this.gameObject);
        }*/
    }

    public GameObject GetBuilding(CustomActionName name)
    {
        for (int i = 0; i < buildings.Length; i++)
        {
            Building building = buildings[i].GetComponent<Building>();
            if (building && building.actionName == name) return buildings[i];
        }
        Debug.Log("No building with action name : '" + name + "' found, check building objectName and ObjectManager");
        return null;
    }

    public GameObject GetUnit(CustomActionName name)
    {
        for (int i = 0; i < units.Length; i++)
        {
            Unit unit = units[i].GetComponent<Unit>();
            if (unit && unit.actionName == name) return units[i];
        }
        Debug.Log("No unit with action name : '" + name + "' found, check unit objectName and ObjectManager");
        return null;
    }

    public UpgradeAction GetUpgrade(CustomActionName name)
    {
        for (int i = 0; i < upgrades.Length; i++)
        {
            UpgradeAction upgrade = upgrades[i].GetComponent<UpgradeAction>();
            if (upgrade != null && upgrade.action.GetActionNameData() == name) return upgrade;
        }
        Debug.Log("No upgrade with name : '" + name + "' found, check upgrades objectName and ObjectManager");
        return null;
    }

    public GameObject GetWorldObject(string name)
    {
        foreach (GameObject worldObject in worldObjects)
        {
            if (worldObject.name == name) return worldObject;
        }
        return null;
    }

    public GameObject GetPlayerObject()
    {
        return player;
    }

    public Sprite GetBuildImage(string name)
    {
        for (int i = 0; i < buildings.Length; i++)
        {
            Building building = buildings[i].GetComponent<Building>();
            if (building && building.name == name) return building.image;
        }
        for (int i = 0; i < units.Length; i++)
        {
            Unit unit = units[i].GetComponent<Unit>();
            if (unit && unit.name == name) return unit.image;
        }
        return null;
    }

    public Texture2D[] GetAvatars()
    {
        return avatars;
    }
}
