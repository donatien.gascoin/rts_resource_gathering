﻿using UnityEngine;
using System.Collections;

/**
 * To implement
 */
public class BuildWonder : VictoryCondition
{
    public BuildWonder(Player[] players) : base(players)
    {
    }

    public override string GetDescription()
    {
        return "Building Wonder";
    }

    public override bool PlayerMeetsConditions(Player player)
    {
        /*Wonder wonder = player.GetComponentInChildren<Wonder>();
        return player && !player.IsDead() && wonder && !wonder.UnderConstruction();*/
        return false;
    }
}