﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AccumulateResources : VictoryCondition
{
    public List<ResourceNeeded> resources;

    public AccumulateResources(Player[] players, List<ResourceNeeded> resources) : base(players)
    {
        this.resources = resources;
    }

    public override string GetDescription()
    {
        return "Accumulating resources";
    }

    public override bool PlayerMeetsConditions(Player player)
    {
        return player && !player.IsDead() && PlayerAccumulateEnoughResources(player);
    }

    private bool PlayerAccumulateEnoughResources(Player player)
    {
        if (resources != null && resources.Count > 0 )
        {
            bool won = true;
            for(int i = 0; i < resources.Count; i++)
            {
                int currentAmount = player.GetResourceAmount(resources[i].type);
                if (currentAmount < resources[i].amount)
                    won = false;
            }
            return won;
        } else
        {
            Debug.Log("Maybe you forgot to assign resources..");
            return false;
        }
    }
}

[System.Serializable]
public class ResourceNeeded {
    public ResourceType type;
    public int amount;

    public ResourceNeeded(){}
    public ResourceNeeded(ResourceType type, int amount)
    {
        this.type = type;
        this.amount = amount;
    }
}