﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyEnemiesQuest : Quest
{

    public List<Player> enemies;

    public override void InitQuest()
    {
        List<Player> players = GameManager.getPlayers();

        for (int i = 0; i < players.Count; i++)
        {
            if (!players[i].human)
            {
                enemies.Add(players[i]);
                break;
            }
        }
    }

    public override bool IsQuestCompleted(Player player)
    {
        bool isSuccess = true;
        for (int i = 0; i < enemies.Count; i++)
        {
            if (!enemies[i].IsDead())
            {
                isSuccess = false;
                break;
            }
        }

        return isSuccess;
    }

    public override void QuestCompletedActions()
    {
        // No actions
    }
}
