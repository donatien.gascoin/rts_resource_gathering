﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * This quest will check is the player is still alive after the duration (in minutes)
 * 
 */
public class StayAliveQuest : Quest
{
    public float duration;
    public float time;
    public bool startTimer = false;

    private void LateUpdate()
    {
        if (startTimer)
        {
            if (time >= duration)
            {
                MaskTheQuestAsCompleted();
                startTimer = false;
                time = duration;
            }
            else
            {
                time += Time.deltaTime;
            }
        }
    }

    public override bool IsQuestCompleted(Player player)
    {
        if (isActive && time >= duration && !player.IsDead())
        {
            return true;
        }
        return false;
    }

    public override void InitQuest()
    {
        time = 0f;
        startTimer = true;
    }

    public override void QuestCompletedActions()
    {
        // Nothing to do
    }
}
