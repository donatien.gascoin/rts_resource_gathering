﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * This quest need to be placed on the GameObject defining the area to reach
 * The object must have a collider with "isTrigger" enabled
 */
public abstract class ReachPointQuest : Quest
{

    private void OnTriggerEnter(Collider other)
    {
        if (isActive && !other.CompareTag(DataManager.GROUND_TAG) && !isCompleted)
        {
            SelectableObject obj = other.GetComponent<SelectableObject>();
            if (obj != null && obj.player == player)
            {
                MaskTheQuestAsCompleted();
            }
        }
    }

    public override bool IsQuestCompleted(Player player)
    {
        return isCompleted;
    }

    public override void InitQuest()
    {
        isCompleted = false;
    }
}
