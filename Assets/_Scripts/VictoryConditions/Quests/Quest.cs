﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Quest : MonoBehaviour
{
    // Player who need to complete the quest
    [HideInInspector]
    public Player player;

    public string questName;

    [TextArea(5,8)]
    public string description;

    // public bool showInQuestInterface = true;

    // public bool isVictoryCondition = true;

    public bool isActive;

    [HideInInspector]
    public bool isCompleted = false;

    public List<Quest> nextQuests;

    public void SetPlayer(Player p)
    {
        this.player = p;
    }

    public abstract bool IsQuestCompleted(Player player);

    public abstract void QuestCompletedActions();

    public abstract void InitQuest();

    public void MaskTheQuestAsCompleted()
    {
        isCompleted = true;
        QuestCompletedActions();
        AudioManager.playSound(SoundCategorie.QUEST_COMPLETED);
        ChatManager.getChatManager().SendFinishedQuestMessage(questName);
    }
}
