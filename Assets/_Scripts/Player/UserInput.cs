﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using UnityEngine.EventSystems;
using System.Linq;
using RTS_Cam;

public class UserInput : MonoBehaviour
{

    bool isSelecting = false;
    Vector3 mousePosition1;

    private Player player;
    private bool clicked;
    private float doubleClickTimeLimit = 0.25f;

    private int nbClick = 0;

    public float timeBetweenHighlight = .1f;
    private float currentTime = 0f;

    void Start()
    {
        selectObject += SelectObj;
        triggerShortcut += ApplyShortcut;

        StartCoroutine(InputListener());
        player = transform.GetComponent<Player>();
        clicked = false;
    }

    private void OnDestroy()
    {
        selectObject -= SelectObj;
        triggerShortcut -= ApplyShortcut;
    }

    void Update()
    {
        MouseActivity();
    }

    #region Mouse management

    private void MouseActivity()
    {
        if (!player.IsUnderConstruction())
        {
            if (player.IsDoingCustomAction())
            {
                if (EventSystem.current.IsPointerOverGameObject())
                    return;
                else if (Input.GetMouseButtonUp(0)) player.StopCustomAction();
                else if (Input.GetMouseButtonUp(1)) CustomActionRightMouseClick();
            } else
            {
                if (nbClick < 2)
                {
                    if (Input.GetMouseButtonDown(0)) LeftMouseClickDown();
                    else if (Input.GetMouseButtonUp(0) && clicked) LeftMouseClickUp();
                    else if (Input.GetMouseButtonUp(1)) RightMouseClick();
                }

                if (isSelecting)
                {
                    GetAndHighlightAllObjectInSquare();
                }
            }
        }
    }

    private IEnumerator InputListener()
    {
        while (enabled)
        { //Run as long as this is activ
            nbClick = 0;
            if (Input.GetMouseButtonUp(0))
            {
                if (WorkManager.FindHitObject(Input.mousePosition).GetComponent<SelectableObject>() != null)
                {
                    nbClick++;
                }
                yield return ClickEvent();
            }

            yield return null;
        }
    }

    private IEnumerator ClickEvent()
    {
        // Debug.Log("CLick event generated at " + Time.time + ", for frame n°" + Time.frameCount);
        //pause a frame so you don't pick up the same mouse down event.
        yield return null; // wait for the next frame
        float count = 0f;
        while (count < doubleClickTimeLimit)
        {
            if (Input.GetMouseButtonUp(0))
            {
                nbClick++;
                if (nbClick >= 2)
                {
                    // Debug.Log("Second click generated at " + Time.time + ", for frame n°" + Time.frameCount);
                    nbClick = 0;
                    DoubleClick();
                }
                yield break;
            }
            count += Time.deltaTime;// increment counter by change in time between frames
            yield return null; // wait for the next frame
        }
        nbClick = 0;
    }

    private void DoubleClick()
    {
        FindObjects();
    }

    private void LeftMouseClickDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;
        clicked = true;
        isSelecting = true;
        mousePosition1 = Input.mousePosition;
        if (!IsHoldingShift() && nbClick < 2)
        {
            UIManager.hideActions();
            UnSelectObjects();
        }
    }

    private void LeftMouseClickUp()
    {
        clicked = false;
        List<SelectableObject> selectedObjects = new List<SelectableObject>();
        Vector3 mousePosition2 = Input.mousePosition;
        if (mousePosition2 == mousePosition1)
        {
            GameObject hitObject = WorkManager.FindHitObject(mousePosition2);
            Vector3 hitPoint = WorkManager.FindHitPoint(mousePosition2);
            if (hitObject && hitPoint != ResourceManager.InvalidPosition)
            {
                if (!hitObject.CompareTag(DataManager.GROUND_TAG))
                {
                    SelectableObject _object = hitObject.transform.GetComponentInParent<SelectableObject>();
                    if (IsHoldingShift() && _object && player.squad.GetSquadUnits().Contains(_object))
                        player.squad.RemoveSquadUnit(_object); 
                    else if (_object && _object.CanBeSelected())
                        selectedObjects.Add(_object);

                    Resource resource = hitObject.GetComponentInParent<Resource>();
                    if (resource && resource.CanBeSelected())
                    {
                        UIManager.selectResource(resource);
                    }
                }
            }
        }
        else
        {
            selectedObjects = GetAndHighlightAllObjectInSquare(true);
        }
        if (IsHoldingShift())
            player.AddUnitsToCurrentSelection(selectedObjects);
        else
            player.ChangeSelection(selectedObjects);
        isSelecting = false;
    }

    private void RightMouseClick()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;
        Vector3 mouseP = Input.mousePosition;
        GameObject hitObject = WorkManager.FindHitObject(mouseP);
        Vector3 hitPoint = WorkManager.FindHitPoint(mouseP);
        if (hitObject && hitPoint != ResourceManager.InvalidPosition)
        {
            if (player.squad.GetSquadSize() > 0)
            {
                if (player.squad.CanMove())
                {
                    if (hitObject.CompareTag(DataManager.GROUND_TAG))
                        PointerManager.mouseRightClickAnimation(hitPoint);
                }
                player.squad.RightClick(hitPoint, hitObject, Behaviour.NORMAL);
            }
        }
    }

    private void CustomActionRightMouseClick()
    {
        Vector3 mouseP = Input.mousePosition;
        GameObject hitObject = WorkManager.FindHitObject(mouseP);
        Vector3 hitPoint = WorkManager.FindHitPoint(mouseP);
        if (hitObject && hitPoint != ResourceManager.InvalidPosition)
        {
            if (player.squad.GetSquadSize() > 0)
            {
                player.squad.ExecuteCustomAction(player.GetCustomActionObject().GetActionNameData(), hitPoint, hitObject, player.GetCustomActionType());
            }
        }
    }

    #endregion

    #region Privates methods, mouse management helpers

    private void FindObjects()
    {
        SelectableObject clickedObject = null;
        Vector3 mousePosition = Input.mousePosition;
        if (mousePosition != ResourceManager.InvalidPosition)
        {
            GameObject hitObject = WorkManager.FindHitObject(mousePosition);
            if (hitObject)
            {
                SelectableObject obj = hitObject.GetComponentInParent<SelectableObject>();
                if (obj)
                {
                    if (obj.player && player == obj.player)
                    {
                        clickedObject = obj;
                    }
                }
            }
        }

        if (clickedObject == null)
        {
            nbClick = 0;
            return;
        }

        SelectableObject[] selectableObjects = FindObjectsOfType<SelectableObject>();
        // List<SelectableObject> selectableObjects = GameManager.getAllObjects();
        selectableObjects = selectableObjects.Where(c => c.objectName == clickedObject.objectName).ToArray();

        List<Renderer> visibleRenderers = new List<Renderer>();
        for (int s = 0; s < selectableObjects.Count(); s++)
        {
            if (player == selectableObjects[s].player)
            {
                Renderer sceneRenderer = selectableObjects[s].GetComponentInChildren<Renderer>();
                    if (IsVisibleOnCamera(sceneRenderer))
                        visibleRenderers.Add(sceneRenderer);
            }
        }

        List<SelectableObject> visibleObjects = new List<SelectableObject>();
        foreach (Renderer renderer in visibleRenderers)
        {
            SelectableObject so = renderer.GetComponentInParent<SelectableObject>();
            if (so != null)
                visibleObjects.Add(so);
        }
        visibleObjects = visibleObjects.OrderBy(x => (clickedObject.transform.position - x.gameObject.transform.position).sqrMagnitude).ToList();

        player.ChangeSelection(visibleObjects);
        nbClick = 0;
    }

    private bool IsVisibleOnCamera(Renderer renderer)
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
        return (GeometryUtility.TestPlanesAABB(planes, renderer.bounds)) ? true : false;
    }

    private bool IsHoldingShift()
    {
        return Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
    }

    private List<SelectableObject> GetAndHighlightAllObjectInSquare(bool force = false)
    {
        List<SelectableObject> objectInSquare = new List<SelectableObject>();
        if (currentTime >= timeBetweenHighlight || force)
        {
            currentTime = 0f;
            Selection currentStatus = Selection.UNKNOWN;
            int maxUnit = DataManager.MAX_UNIT_PER_SELECTION;
            int nbUnitSelected = 0;
            List<SelectableObject> hits = FindObjectsOfType<SelectableObject>().ToList();
            // List<SelectableObject> hits = GameManager.getAllObjects();
            // hits = hits.OrderBy(x => Vector2.Distance(mousePosition1, x.gameObject.transform.position)).ToList();
            hits = hits.OrderBy(x => (mousePosition1 - x.gameObject.transform.position).sqrMagnitude).ToList();

            foreach (var _object in hits)
            {
                if (IsWithinSelectionBounds(_object.gameObject) && _object.CanBeSelected())
                {
                    /*Debug.Log("Object in bounds : " + _object.name + ", magnitude : " + (mousePosition1 - _object.gameObject.transform.position).sqrMagnitude);*/
                    Selection unitStatus = GetObjectSelectionStatus(_object);
                    switch(unitStatus)
                    {
                        case Selection.UNKNOWN:
                            Debug.Log("Don't on what you've clicked, but nothing good");
                            break;
                        case Selection.BUILDING:
                            if (Selection.ENEMY == currentStatus || Selection.UNKNOWN == currentStatus)
                            {
                                objectInSquare = new List<SelectableObject>();
                                nbUnitSelected = 0;
                            }
                            // Unit have priority over buildings
                            if (Selection.UNIT != currentStatus && nbUnitSelected < maxUnit)
                            {
                                objectInSquare.Add(_object);
                                currentStatus = unitStatus;
                                nbUnitSelected++;
                            }
                            break;
                        case Selection.ENEMY:
                            if (Selection.UNKNOWN == currentStatus)
                            {
                                // Highlight only one enemy unit, is nothing else has been already selected
                                if (objectInSquare.Count == 0)
                                {
                                    objectInSquare.Add(_object);
                                    currentStatus = unitStatus;
                                    nbUnitSelected++;
                                }
                            }
                            break;
                        case Selection.UNIT:
                            if (Selection.UNIT != currentStatus)
                            {
                                objectInSquare = new List<SelectableObject>();
                                nbUnitSelected = 0;
                            }
                            if (nbUnitSelected < maxUnit)
                            {
                                objectInSquare.Add(_object);
                                currentStatus = unitStatus;
                                nbUnitSelected++;
                            }
                            break;
                    }
                }
                if (!IsHoldingShift())
                    _object.SetSelection(false);
                else if (IsHoldingShift() && !player.squad.GetSquadUnits().Contains(_object))
                    _object.SetSelection(false);
            }
            foreach (SelectableObject obj in objectInSquare)
            {
                obj.SetSelection(true);
            }
        }
        currentTime += Time.deltaTime;
        return objectInSquare;
    }

    private Selection GetObjectSelectionStatus(SelectableObject obj)
    {
        if (obj.player != player)
            return Selection.ENEMY;
        foreach (SelectableObject so in player.units)
        {
            if (obj.ObjectId == so.ObjectId)
                return Selection.UNIT;
        }
        foreach (SelectableObject so in player.buildings)
        {
            if (obj.ObjectId == so.ObjectId)
                return Selection.BUILDING;
        }
        // Should never happen
        return Selection.UNKNOWN;
    }

    private void UnSelectObjects()
    {
        player.squad.RemoveAllSquadUnits();
    }

    private bool IsWithinSelectionBounds(GameObject gameObject)
    {
        if (!isSelecting)
            return false;

        var camera = Camera.main;
        var viewportBounds = Utils.GetViewportBounds(camera, mousePosition1, Input.mousePosition);
        return viewportBounds.Contains(camera.WorldToViewportPoint(gameObject.transform.position));
    }

    #endregion

    void OnGUI()
    {
        if (isSelecting)
        {
            // Create a rect from both mouse positions
            var rect = Utils.GetScreenRect(mousePosition1, Input.mousePosition);

            // Utils.DrawScreenRect(rect, new Color(0.8f, 0.8f, 0.95f, 0.25f));
            Utils.DrawScreenRectBorder(rect, 2f, new Color(0.8f, 0.8f, 0.95f));
        }
    }

    #region Delegate Select object

    public delegate void SelectObject(SelectableObject obj, bool focus);
    public static SelectObject selectObject;

    public void SelectObj(SelectableObject obj, bool focus)
    {
        List<SelectableObject> selectedObjects = new List<SelectableObject>();
        selectedObjects.Add(obj);
        player.ChangeSelection(selectedObjects);
        if (focus)
        {
            RTS_Camera cam = FindObjectOfType<RTS_Camera>();
            cam.SetFocusPoint(obj.transform.position);
        }
    }

    #endregion

    #region Delegate ShortcutManager

    public delegate void TriggerShortcut(KeyCode code);
    public static TriggerShortcut triggerShortcut;

    public void ApplyShortcut(KeyCode code)
    {
        if (KeyCode.None == code)
            return;

        
        SelectableObject obj = player.squad.GetFirstSquadUnitInUnitsSelectedOnUI();
        if (obj == null)
            return;

        for (int i = 0; i < obj.actions.Length; i++)
        {
            if (obj.actions[i] != null && code.Equals(obj.actions[i].GetShortcut()))
                obj.PerformLeftClickAction(obj.actions[i].GetActionNameData());
        }
    }

    #endregion

    enum Selection
    {
        UNIT, BUILDING, ENEMY, UNKNOWN
    }

}

[System.Serializable]
public enum UserAction
{
    SAME_SQUAD_UNITS, SQUAD_ACTION, ONE_UNIT
}