﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(AudioSource))]
public class Player : MonoBehaviour
{
    public string username;
    public PlayerColor color;
    public bool human;
    public int team;
    public int startMoney, startWood;

    #region Resources
    public Dictionary<ResourceType, int> resources;
    private int consummedFood, maxAllowedFood;
    #endregion

    public List<Unit> units;
    public SelectableObject hero;
    public List<Building> buildings;
    public Squad squad;

    public UpgradesStatus upgrades;

    public bool useCustomeAction;
    public CustomActionObject customAction;
    public UserAction customActionType;

    #region Building to create

    private Building tempBuilding;
    private Unit tempCreator;

    private bool findingPlacement = false;
    private bool canPlaceBuilding = false;

    #endregion

    public Sounds sounds;

    [HideInInspector]
    private AudioSource audioSource { get { return GetComponent<AudioSource>(); } }

    private float timeBetweenUnitCheck = .1f, currentTimeBetweenUnitCheck = 0f;

    #region Monobehavior methods

    void Awake()
    {
        resources = InitResourceList();
        squad = new Squad(this);
        useCustomeAction = false;

    }

    void Start()
    {
        // upgrades.Init();
        InitUnits();
        InitBuildings();
        UpdateResourcesOnUI();
    }

    void Update()
    {
        if (human)
        {
            if (tempBuilding == null)
                findingPlacement = false;
            if (findingPlacement && tempBuilding != null)
            {
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hitInfo, Mathf.Infinity, LayerMask.GetMask(DataManager.GROUND_TAG) ))
                {
                    tempBuilding.transform.position = new Vector3(hitInfo.point.x, hitInfo.point.y, hitInfo.point.z);
                }

                canPlaceBuilding = IsLegalPosition();
                if (EventSystem.current.IsPointerOverGameObject())
                    return;

                if (Input.GetMouseButtonUp(0))
                {
                    if (canPlaceBuilding)
                        StartConstruction();
                    else
                        sounds.PlaySound(audioSource, SoundCategorie.IMPOSSIBLE_TO_CONSTRUCT_HERE);
                } else if (Input.GetMouseButtonUp(1))
                {
                    CancelBuildingPlacement();
                }
            }
        }
        if (currentTimeBetweenUnitCheck >= timeBetweenUnitCheck)
        {
            currentTimeBetweenUnitCheck = 0f;
            units = units.Where(item => item != null).ToList();
            buildings = buildings.Where(item => item != null).ToList();
        } else
        {
            currentTimeBetweenUnitCheck += Time.deltaTime;
        }
    }

    #endregion

    #region Init

    private Dictionary<ResourceType, int> InitResourceList()
    {
        Dictionary<ResourceType, int> list = new Dictionary<ResourceType, int>();
        list.Add(ResourceType.GOLD, startMoney);
        list.Add(ResourceType.WOOD, startWood);
        return list;
    }

    private void InitBuildings()
    {
        Buildings buildingsWrapper = GetComponentInChildren<Buildings>();
        Building[] buildings = buildingsWrapper.GetComponentsInChildren<Building>();

        for (int i = 0; i < buildings.Length; i++)
        {
            buildings[i].SetPlayer(this);
//             AddBuildingToPlayer(buildings[i]);
            buildings[i].SetObstacle(true);
            buildings[i].SetPlaceableMarker(false);
            buildings[i].ChangeBuildingBanner(this);
            maxAllowedFood += buildings[i].additionalFood;
        }
    }

    private void InitUnits()
    {
        Units unitsWrapper = GetComponentInChildren<Units>();
        Unit[] units = unitsWrapper.GetComponentsInChildren<Unit>();

        for (int i = 0; i < units.Length; i++)
        {
            units[i].SetPlayer(this);
            // AddUnitToPlayer(units[i]);
            Cost cost = Array.Find(units[i].ChooseObjectStat().GetCosts().ToArray(), (Cost c) => c.type == ResourceType.FOOD);
            consummedFood += cost.amount;
        }
    }

    #endregion

    #region Getter

    public bool IsDead()
    {
        if (buildings != null && buildings.Count > 0) return false;
        if (units != null && units.Count > 0) return false;
        return true;
    }


    public Transform GetBuildingsTransform()
    {
        return GetComponentInChildren<Buildings>().transform;
    }

    public Transform GetUnitsTransform()
    {
        return GetComponentInChildren<Units>().transform;
    }

    #endregion

    #region Units/buildings upgrades

    public UpgradeAction GetUpgradeActionFromActionName(CustomActionName actionName)
    {
        return upgrades.GetUpgradeActionFromActionName(actionName);
    }

    public Upgrades GetUpgradeStatus(UpgradeType type)
    {
        return upgrades.GetUpgradeStatus(type);
    }

    public void MakeUpgrade(Upgrades upgradeCurrentlyConstructed)
    {
        upgradeCurrentlyConstructed.Upgrade();
        for (int i = 0; i < units.Count; i++)
        {
            units[i].CheckUpgrade();
        }
        for (int i = 0; i < buildings.Count; i++)
        {
            buildings[i].CheckUpgrade();
        }
    }

    #endregion

    #region Resources

    public int GetConsumedFood()
    {
        return consummedFood;
    }

    public int GetMaxFoodAllowed()
    {
        return maxAllowedFood;
    }

    public void RemoveConsummedFood(int amount)
    {
        consummedFood -= amount;
        UpdateResourcesOnUI();
    }

    public void UpdateMaxAllowedFood()
    {
        int newMax = 0;
        for (int i = 0; i < buildings.Count; i++)
        {
            if (!buildings[i].isUnderConstruction)
                newMax += buildings[i].additionalFood;
        }

        maxAllowedFood = newMax;
        UpdateResourcesOnUI();
    }
    
    public void AddResource(ResourceType type, int amount)
    {
        switch (type)
        {
            case ResourceType.FOOD:
                // Food not added this way, use UpdateFood() instead
                break;
            case ResourceType.WOOD:
            case ResourceType.GOLD:
                resources[type] += amount;
                break;
            default:
                Debug.Log("Impossible to add resource : " + type);
                break;
        }
        UpdateResourcesOnUI();
    }

    public void AddResources(List<Cost> costs)
    {
        foreach (Cost c in costs)
        {
            AddResource(c.type, c.amount);
        }
    }

    public int GetResourceAmount(ResourceType type)
    {
        return resources[type];
    }

    public bool RemoveResources(List<Cost> costs)
    {
        bool isResources = true;
        // Check wood and gold
        foreach (Cost item in costs)
        {
            if (ResourceType.FOOD == item.type)
            {
                if ((maxAllowedFood - consummedFood) < item.amount)
                {
                    isResources = false;
                    if (human)
                    {
                        sounds.PlaySound(audioSource, SoundCategorie.ADDITIONAL_FOOD_REQUIRED);
                        UIManager.highlightResource(ResourceType.FOOD);
                    }
                }
            } else
            {
                if (resources[item.type] < item.amount)
                {
                    isResources = false;
                    if (human)
                    {
                        sounds.PlaySound(audioSource, ResourceType.GOLD.Equals(item.type) ? SoundCategorie.ADDITIONAL_GOLD_REQUIRED : SoundCategorie.ADDITIONAL_WOOD_REQUIRED);
                        UIManager.highlightResource(item.type);
                    }
                    break;
                }
            }
            
        }

        if (isResources)
        {
            foreach (Cost item in costs)
            {
                if (ResourceType.FOOD == item.type)
                    consummedFood += item.amount;
                else
                    resources[item.type] -= item.amount;
            }
        }
        UpdateResourcesOnUI();
        return isResources;
    }

    private List<Building> GetWarehouses()
    {
        List<Building> warehouses = new List<Building>();

        for (int i = 0; i < buildings.Count; i++)
        {
            if (buildings[i].IsWarehouse())
            {
                warehouses.Add(buildings[i]);
            }
        }
        return warehouses;
    }

    public List<Building> GetAllWarehousesAcceptingThisResource(ResourceType type)
    {
        List<Building> warehouses = GetWarehouses();
        List<Building> acceptedWarehouses = new List<Building>();

        for (int i = 0; i < warehouses.Count; i++)
        {
            if (warehouses[i].resourcesAccepted.Contains(type) && !warehouses[i].IsUnderConstruction())
            {
                acceptedWarehouses.Add(warehouses[i]);
            }
        }
        return acceptedWarehouses;
    }

    #endregion

    #region Unit / building management

    public SelectableObject GetHero()
    {
        return hero;
    }

    public bool HaveHero()
    {
        return hero != null ? true : false;
    }

    public void AddUnit(CustomActionName unitName, Vector3 spawnPoint, Vector3 rallyPoint, Quaternion rotation, Building creator)
    {
        Units units = GetComponentInChildren<Units>();

        GameObject newUnit = (GameObject)Instantiate(ResourceManager.GetUnit(unitName), spawnPoint, rotation);
        newUnit.transform.parent = units.transform;
        Unit unitObject = newUnit.GetComponent<Unit>();
        if (unitObject)
        {
            unitObject.ObjectId = ResourceManager.GetNewObjectId();
            unitObject.SetPlayer(this);
            unitObject.PlayCreatedSound();
            if (unitName == CustomActionName.HERO)
                hero = unitObject;

            if (spawnPoint != rallyPoint) unitObject.StartMove(rallyPoint);
        }
        else Destroy(newUnit);
    }

    public List<SelectableObject> GetNonWorkingWorkers()
    {
        List<SelectableObject> harvesters = new List<SelectableObject>();
        foreach (Unit u in units)
        {
            Worker h = u as Worker;
            if (h != null && !h.IsWorking())
            {
                harvesters.Add(h);
            }
        }
        return harvesters;
    }

    public List<SelectableObject> GetWorkers()
    {
        List<SelectableObject> harvesters = new List<SelectableObject>();
        foreach (Unit u in units)
        {
            Worker h = u as Worker;
            if (h != null)
            {
                harvesters.Add(h);
            }
        }
        return harvesters;
    }

    public void AddBuildingToPlayer(Building building)
    {
        building.player = this;
        if (buildings == null)
        {
            buildings = new List<Building>();
        }
        buildings.Add(building);
        building.SetMaterials();
        if (!human)
        {
            EnvironmentController.checkPosition(building);
        }
        
    }
    
    public void AddUnitToPlayer(Unit unit)
    {
        unit.player = this;
        if (units == null)
        {
            units = new List<Unit>();
        }
        unit.ObjectId = ResourceManager.GetNewObjectId();
        unit.SetMaterials();
        units.Add(unit);

        if (unit.objectName == "Hero")
            hero = unit;
    }

    public void AddUnitsToCurrentSelection(List<SelectableObject> selectedObjects)
    {
        if (squad.IsUnitSelectedAHumanPlayerUnit())
        {
            for (int i = 0; i < selectedObjects.Count; i++)
                squad.AddSquadUnit(selectedObjects[i]);
        } else
        {
            ChangeSelection(selectedObjects);
        }
    }

    public void ChangeSelection(List<SelectableObject> newSelectedObjects)
    {
        squad.SetSquadUnits(newSelectedObjects);
    }

    public void RemoveFromSelectionAtPosition(int removePosition)
    {
        if (removePosition > squad.GetSquadSize())
            return;
        squad.RemoveSquadUnitAtPosition(removePosition);
    }

    public void SelectAtPosition(int selectPosition)
    {
        if (selectPosition > squad.GetSquadSize())
            return;
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            squad.SelectThisTypeOfUnit(selectPosition);
        else
            squad.SelectUnitAtPosition(selectPosition);
    }

    public void ManageClickOnUI(int position)
    {
        if (position > squad.GetSquadSize())
            return;
        if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
            RemoveFromSelectionAtPosition(position);
        else
            SelectAtPosition(position);
    }

    public void AddToSelectedObjects(SelectableObject obj)
    {
        squad.AddSquadUnit(obj);
    }

    public void RemoveUnit(Unit unit)
    {
        Cost cost = Array.Find(unit.ChooseObjectStat().GetCosts().ToArray(), (Cost c) => c.type == ResourceType.FOOD);
        consummedFood -= cost.amount;
        UpdateResourcesOnUI();
        units.Remove(unit);
    }

    public void RemoveBuilding(Building building)
    {
        buildings.Remove(building);
        // maxAllowedFood -= building.additionalFood;
        UpdateMaxAllowedFood();
        UpdateResourcesOnUI();
    }
    public void CancelUpgrade()
    {
        Building b = squad.GetFirstSquadUnitInUnitsSelectedOnUI() as Building;
        if (b != null)
            b.CancelUpgrade();
    }

    #endregion

    #region Building placement

    public bool IsUnderConstruction()
    {
        return findingPlacement;
    }

    public bool IsPlacingThisBuilding(Building building)
    {
        if (tempBuilding == null || building == null)
            return false;
        return findingPlacement && tempBuilding.ObjectId == building.ObjectId;
    }

    public bool IsLegalPosition()
    {
        bool isLegal = false;
        if (tempBuilding.colliders.Count == 0)
        {
            isLegal = true;
        }
        tempBuilding.SetPlaceableMarker(true);
        tempBuilding.placeableMarker.material = UIManager.getBuildMaterial(isLegal);
        return isLegal;
    }

    

    public void CreateBuilding(CustomActionName buildingName, Vector3 buildPoint, Unit creator)
    {
        if (tempBuilding != null)
        {
            CancelBuildingPlacement();
        }
        GameObject newBuilding = (GameObject)Instantiate(ResourceManager.GetBuilding(buildingName), buildPoint, new Quaternion());
        tempBuilding = newBuilding.GetComponent<Building>();
        tempBuilding.SetCurrentlyPlaced(true);
        // tempBuilding.SetPlayer(this);
        tempBuilding.SetMaterials(this);
        tempBuilding.ChangeBuildingBanner(this);
        tempBuilding.gameObject.name = tempBuilding.objectName;
        if (tempBuilding)
        {
            tempBuilding.ObjectId = ResourceManager.GetNewObjectId();
            tempCreator = creator;
            findingPlacement = true;
            tempBuilding.hitPoints = 0;
        }
        else Destroy(newBuilding);
    }
    
    public void StartConstruction()
    {
        if (RemoveResources(tempBuilding.ChooseObjectStat().GetCosts()))
        {
            tempBuilding.SetCurrentlyPlaced(false);
            /*findingPlacement = false;*/
            Buildings buildings = GetComponentInChildren<Buildings>();
            if (buildings) tempBuilding.transform.parent = buildings.transform;
            tempCreator.SetBuilding(tempBuilding);
            tempBuilding.SetPlayer(this);
            tempBuilding.StartConstruction();
            tempBuilding = null;
        }
    }

    public void CancelBuildingPlacement()
    {
        // findingPlacement = false;
        Destroy(tempBuilding.gameObject);
        tempBuilding = null;
        tempCreator = null;
    }

    #endregion

    #region Bonus/Requirements

    public bool CheckRequirement(CustomActionName requirement)
    {
        bool isOk = false;
        if (CustomActionName.KEEP.Equals(requirement))
        {
            isOk = SearchBuilding(CustomActionName.KEEP) || SearchBuilding(CustomActionName.CASTLE);
        }
        else
        {
            isOk = SearchBuilding(requirement);
        }

        return isOk;
    }

    private bool SearchBuilding(CustomActionName v)
    {
        bool isBuilding = false;
        for (int i = 0; i < buildings.Count; i++)
        {
            if (buildings[i].actionName.Equals(v) && !buildings[i].IsUnderConstruction())
            {
                isBuilding = true;
                break;
            }
        }
        return isBuilding;
    }

    #endregion

    #region Custom actions

    public bool IsDoingCustomAction()
    {
        return useCustomeAction;
    }

    public CustomActionObject GetCustomActionObject()
    {
        return customAction;
    }

    public UserAction GetCustomActionType()
    {
        return customActionType;
    }

    public void SetCustomActionClick(CustomActionObject action, UserAction actiontype)
    {
        this.useCustomeAction = true;
        this.customAction = action;
        this.customActionType = actiontype;
    }

    public void StopCustomAction()
    {
        this.useCustomeAction = false;
        this.customAction = null;
    }

    #endregion

    #region UI

    private void UpdateResourcesOnUI()
    {
        if (human)
        {
            foreach (ResourceType type in resources.Keys)
            {
                if (ResourceType.FOOD != type)
                    UIManager.updateResource(type, resources[type]);
            }
            UIManager.updateFood(consummedFood, maxAllowedFood);
        }
    }

    public void UpdateUISquad()
    {
        /*if (human && squad.GetSquadSize() > 0)
            UIManager.setUI(squad.GetSquadUnits());*/
    }

    #endregion

}
