﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(Button))]
public class ButtonSound : MonoBehaviour, IPointerEnterHandler
{

    private Button button { get { return GetComponent<Button>(); } }
    public SoundCategorie buttonClickAction = SoundCategorie.BUTTON_CLICK_VALIDATE;
    [Space(10)]
    public bool playSoundOnHover = false;
    public SoundCategorie buttonHoverAction = SoundCategorie.BUTTON_HOVER;

    private void Awake()
    {
        button.onClick.AddListener(() => AudioManager.playSound(buttonClickAction));
    }

    private void OnDestroy()
    {
        button.onClick.RemoveAllListeners();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (playSoundOnHover)
        {
            AudioManager.playSound(buttonHoverAction);
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(ButtonSound))]
public class ButtonSoundEditor : Editor
{
    override public void OnInspectorGUI()
    {
        var buttonSound = target as ButtonSound;

        buttonSound.buttonClickAction = (SoundCategorie)EditorGUILayout.EnumPopup("On click clip", buttonSound.buttonClickAction);

        buttonSound.playSoundOnHover = GUILayout.Toggle(buttonSound.playSoundOnHover, "Play sound on hover");

        if (buttonSound.playSoundOnHover)
            buttonSound.buttonHoverAction = (SoundCategorie)EditorGUILayout.EnumPopup("Hover clip", buttonSound.buttonHoverAction);


    }
}

#endif