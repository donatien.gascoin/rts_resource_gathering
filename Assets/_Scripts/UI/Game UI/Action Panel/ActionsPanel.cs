﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ActionsPanel : MonoBehaviour
{
    
    public List<ActionButton> actionButtons;

    public void UpdateUI(SelectableObject obj)
    {
        if (obj.player.human)
        {
            int nbActions = obj.actions.Length;
            for (int i = 0; i < actionButtons.Count; i++)
            {
                actionButtons[i].leftClick.RemoveAllListeners();
                actionButtons[i].rightClick.RemoveAllListeners();
                if (i < nbActions && obj.CanDoActions() && obj.actions[i] != null)
                {
                    CustomActionObject action = obj.actions[i];// .GetComponent<CustomAction>();
                    actionButtons[i].UpdateUI(obj, action);
                }
                else
                {
                    actionButtons[i].button.gameObject.SetActive(false);
                }
            }
        } else
        {
            for (int i = 0; i < actionButtons.Count; i++)
            {
                actionButtons[i].button.gameObject.SetActive(false);
            }
        }
    }
}