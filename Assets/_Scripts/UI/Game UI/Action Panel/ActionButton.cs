﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class ActionButton : MonoBehaviour, IPointerClickHandler
{
    [HideInInspector]
    public UnityEvent leftClick;
    [HideInInspector]
    public UnityEvent rightClick;

    public Image image;
    public Image automaticImage;
    public Button button;

    // Action description
    public TMP_Text actionName;

    public TMP_Text actionDescription;
    public TMP_Text requirements;

    public ActionResource resource1;
    public ActionResource resource2;
    public ActionResource resource3;

    private bool shouldCheckShortcut;
    private KeyCode shortCut;

    private bool isInteractable;

    public ResourceImage[] resourcesImages;

    public Color normalButtonColor;
    public Color isDoingColor;

    public HoverActionButton hoverButton;

    public Image cooldownImage;


    public void UpdateUI(SelectableObject obj, CustomActionObject action)
    {
        CustomActionName actionText = action.GetActionNameData();
        image.sprite = action.GetImage();
        button.gameObject.SetActive(true);

        leftClick.AddListener(delegate { obj.PerformLeftClickAction(actionText); });
        rightClick.AddListener(delegate { obj.PerformRightClickAction(actionText); });

        if (action.disableAction)
        {
            button.interactable = false;
        } else
        {
            button.interactable = true;
        }

        if (KeyCode.None.Equals(action.GetShortcut()))
        {
            actionName.text = action.GetActionName();
            shouldCheckShortcut = false;
        }
        else
        {
            actionName.text = action.GetActionName() + " (" + action.GetShortcut() + ")";
            shouldCheckShortcut = true;
            shortCut = action.GetShortcut();
        }
        actionDescription.text = action.GetDescription();

        if (!action.disableAction)
        {
            if (action.automaticAction)
                automaticImage.gameObject.SetActive(true);
            else
                automaticImage.gameObject.SetActive(false);
        } else
        {
            automaticImage.gameObject.SetActive(false);
        }


        resource1.gameObject.SetActive(false);
        resource2.gameObject.SetActive(false);
        resource3.gameObject.SetActive(false);

        SpellActionObject spell = action as SpellActionObject;
        if (spell != null) {
            if (spell.GetManaRequired() != 0)
            {
                resource3.UpdateUI(spell.GetManaRequired().ToString(), GetSpriteFromResourceName(ResourceName.MANA));
                resource3.gameObject.SetActive(true);
            }

            if (spell.CanDoAction() || spell.handleAction)
            {
                cooldownImage.gameObject.SetActive(false);
            } else
            {
                cooldownImage.gameObject.SetActive(true);
                cooldownImage.fillAmount = spell.GetCoolDownPercentage();
            }

        } 
        else
        {
            bool checkUpgrades = true;

            SelectableObject so = GetObjectFromAction(action);
            if (so != null)
            {
                checkUpgrades = false;
                PrintCosts(so.ChooseObjectStat().GetCosts());
                Building b = so.GetComponent<Building>();
                if (b != null && b.additionalFood != 0)
                {
                    resource3.UpdateUI("+" + b.additionalFood, GetSpriteFromResourceName(ResourceName.FOOD));
                    resource3.gameObject.SetActive(true);
                }

                PrintRequirement(action.GetRequirements(), obj.player, action);

            }

            UpgradeAction upgrade = GetUpgradeFromAction(action, obj.player);
            if (checkUpgrades && upgrade != null)
            {
                PrintCosts(upgrade.upgradeCost);
                PrintRequirement(upgrade.requirements, obj.player, action);
            }
        }

        if (button.IsInteractable())
            isInteractable = true;
        else
            isInteractable = false;

        if (action.isDoingThisAction)
            button.image.color = isDoingColor;
        else
            button.image.color = normalButtonColor;
    }

    private void PrintCosts(List<Cost> costs)
    {
        foreach (Cost c in costs)
        {
            switch (c.type)
            {
                case ResourceType.GOLD:
                    if (c.amount != 0)
                    {
                        resource1.UpdateUI(c.amount.ToString(), GetSpriteFromResourceName(ResourceName.GOLD));
                        resource1.gameObject.SetActive(true);
                    }
                    break;

                case ResourceType.WOOD:
                    if (c.amount != 0)
                    {
                        resource2.UpdateUI(c.amount.ToString(), GetSpriteFromResourceName(ResourceName.WOOD));
                        resource2.gameObject.SetActive(true);
                    }
                    break;
                case ResourceType.FOOD:
                    if (c.amount != 0)
                    {

                        resource3.UpdateUI(c.amount.ToString(), GetSpriteFromResourceName(ResourceName.FOOD));
                        resource3.gameObject.SetActive(true);
                    }
                    break;
            }
        }
    }

    private void PrintRequirement(List<CustomActionName> reqs, Player player, CustomActionObject action)
    {
        if (reqs.Count > 0)
        {
            string r = "Need" + (reqs.Count > 1 ? "s : " : " : ");
            for (int i = 0; i < reqs.Count; i++)
            {
                if (i > 0)
                    r += ", ";
                r += DataManager.GetBuildingRequirementName(reqs[i]);
            }
            requirements.text = r;

            bool isOk = true;
            for (int i = 0; i < reqs.Count; i++)
            {
                isOk = player.CheckRequirement(reqs[i]);
                if (!isOk)
                    break;
            }
            if (isOk)
            {
                requirements.text = "";
            }
            else
            {
                /*requirements.color = requirementNeededColor;*/
                if (!action.disableAction)
                {
                    button.interactable = false;     
                }
            }


        }
        else
        {
            requirements.text = "";
        }
    }

    private UpgradeAction GetUpgradeFromAction(CustomActionObject action, Player player)
    {
        return player.GetUpgradeActionFromActionName(action.GetActionNameData());
    }

    private SelectableObject GetObjectFromAction(CustomActionObject action)
    {
        SelectableObject obj = null;
        if (action == null)
            return obj;
        switch (action.GetActionType())
        {
            case ActionType.CREATE_UNIT:
                obj = ResourceManager.GetUnit(action.GetActionNameData()).GetComponent<SelectableObject>();
                break;

            case ActionType.CREATE_BUILDING:
                obj = ResourceManager.GetBuilding(action.GetActionNameData()).GetComponent<SelectableObject>();
                break;
        }
        return obj;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (isInteractable)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                if (hoverButton)
                    hoverButton.HidePanel();
                leftClick.Invoke();
            }
            else if (eventData.button == PointerEventData.InputButton.Right)
                rightClick.Invoke();
        }
    }

    private void Update()
    {
        if (shouldCheckShortcut && Input.GetKeyUp(shortCut))
        {
            UserInput.triggerShortcut(shortCut);
        }
    }

    private Sprite GetSpriteFromResourceName(ResourceName name)
    {
        for(int i = 0; i < resourcesImages.Length; i++)
        {
            if (name.Equals(resourcesImages[i].name))
                return resourcesImages[i].image;
        }
        return null;
    }
}

[System.Serializable]
public class ResourceImage
{
    public ResourceName name;
    public Sprite image;
}

[System.Serializable]
public enum ResourceName
{
    GOLD,
    WOOD,
    FOOD,
    MANA
}