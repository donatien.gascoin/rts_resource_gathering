﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ActionResource : MonoBehaviour
{
    public Image resourceImage;
    public TMP_Text value;

    public void UpdateUI(string value, Sprite sprite)
    {
        this.value.text = value;
        resourceImage.sprite = sprite;
    }
}
