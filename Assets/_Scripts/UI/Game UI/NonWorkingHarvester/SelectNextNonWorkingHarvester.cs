﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectNextNonWorkingHarvester : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F8))
        {
            SelectNextWorker();
        }
    }

    public void SelectNextWorker()
    {
        UIManager.selectNonWorkerWorker();
    }
}
