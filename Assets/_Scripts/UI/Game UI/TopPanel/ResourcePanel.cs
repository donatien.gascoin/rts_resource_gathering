﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Globalization;

public class ResourcePanel : MonoBehaviour
{
    public TMP_Text resourceText;
    public Image resourcePanel;

    public void UpdateResource(int amount)
    {
        resourceText.text = FormatUINumber(amount);
    }

    public void UpdateResource(string data)
    {
        resourceText.text = data;
    }

    public void HighlightResource()
    {
        StartCoroutine(HighlightCoroutine(resourcePanel, resourceText));
    }

    private IEnumerator HighlightCoroutine(Image panel, TMP_Text text)
    {

        Color warningColor = new Color32(181, 65, 44, 255);
        Color baseColor = new Color32(255, 255, 255, 0);

        for (int i = 0; i < 3; i++)
        {
            panel.color = warningColor;
            yield return new WaitForSeconds(0.3f);
            panel.color = baseColor;
            yield return new WaitForSeconds(0.3f);
        }

        panel.color = baseColor;
    }

    private string FormatUINumber(int number)
    {
        var nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
        nfi.NumberGroupSeparator = " ";
        string formatted = number.ToString("#,0", nfi);
        // return number.ToString("# ### ### ###");
        return formatted;
    }
}
