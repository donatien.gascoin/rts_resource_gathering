﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ResourcesPanel : MonoBehaviour
{
    public ResourcePanel gold;
    public ResourcePanel wood;
    public ResourcePanel food;

    public void UpdateGoldAmount(int _gold)
    {
        gold.UpdateResource(_gold);
    }

    public void UpdateWoodAmount(int _wood)
    {
        wood.UpdateResource(_wood);
    }

    public void UpdateFoodAmount(int consummedFood, int maxAllowedFood)
    {
        food.UpdateResource(consummedFood + " / " + maxAllowedFood);
    }

    public void HighlightGoldUI()
    {
        gold.HighlightResource();
    }

    public void HighlightWoodUI()
    {
        wood.HighlightResource();
    }

    public void HighlightFoodUI()
    {
        food.HighlightResource();
    }
}
