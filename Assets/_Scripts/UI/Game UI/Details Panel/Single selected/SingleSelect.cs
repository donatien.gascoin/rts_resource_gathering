﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SingleSelect : MonoBehaviour
{
    [Header("Basic panel")]
    public GameObject basicPanel;
    public TMP_Text attack;
    public TMP_Text defense;
    public TMP_Text type;
    public TMP_Text selectedName;
    public UpgradeButton upgradeButton;
    public BonusesUI bonuses;

    [Header("Upgrade panel")]
    public GameObject upgradePanel;
    public TMP_Text upgradeName;
    public Image upgradeImage;
    public UpgradeSlider upgradeSlider;

    public void UpdateUI(SelectableObject obj)
    {
        Building b = obj as Building;

        if (b == null || !b.IsUpgrading())
        {
            if (obj.GetStats().minAttack == 0 && obj.GetStats().maxAttack == 0)
            {
                attack.text = obj.GetStats().minAttack.ToString();
            } else
            {
                Bonus attackBonus = obj.GetBonus(BonusEffects.ATTACK);
                if (attackBonus != null)
                    attack.text = (int)(obj.GetStats().minAttack * attackBonus.GetEffectValue()) + " - " + (int)(obj.GetStats().maxAttack * attackBonus.GetEffectValue());
                else
                    attack.text = obj.GetStats().minAttack.ToString() + " - " + obj.GetStats().maxAttack.ToString();
            }

            Bonus defenseBonus = obj.GetBonus(BonusEffects.DEFENSE);
            if (defenseBonus != null)
                defense.text = (/*(int)*/obj.GetStats().defense * defenseBonus.GetEffectValue()).ToString();
            else
                defense.text = obj.GetStats().defense.ToString();

            type.text = obj.GetAttackType().ToString();
            selectedName.text = obj.objectName;
            if (obj.player.human)
                upgradeButton.UpdateUI(obj);
            upgradePanel.SetActive(false);
            basicPanel.SetActive(true);

            bonuses.UpdateUI(obj);
        } else
        {
            upgradeImage.sprite = b.upgradeImage;
            upgradeName.text = b.GetUpgradeName();
            upgradeSlider.UpdateUI(b.currentUpgradeTime, b.upgradeTime);
            basicPanel.SetActive(false);
            upgradePanel.SetActive(true);

        }
    }
}
