﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UpgradeButton : MonoBehaviour
{
    public Button button;

    // Upgrade description
    public TMP_Text upgradeName;
    
    public GameObject goldPanel;
    public TMP_Text gold;

    public GameObject woodPanel;
    public TMP_Text wood;

    public HoverText hoverText;

    public CustomActionName previousShowedUpgrade;

    private void Awake()
    {
        previousShowedUpgrade = CustomActionName.NONE;
    }

    public void UpdateUI(SelectableObject obj)
    {
        Building building = obj as Building;
        if (building != null)
        {
            if (!building.IsUnderConstruction() && building.upgradedBuilding != null && !building.IsUpgrading())
            {
                button.gameObject.SetActive(true);
                button.onClick.RemoveAllListeners();
                button.onClick.AddListener(building.UpgradeBuilding);
                button.onClick.AddListener(hoverText.HideText);

                upgradeName.text = "Upgrade to " + building.GetUpgradeName();
                if (!previousShowedUpgrade.Equals(building.actionName))
                {
                    previousShowedUpgrade = building.actionName;
                    PrintCosts(building.upgradedBuilding.GetComponent<SelectableObject>().ChooseObjectStat().GetCosts());
                }
            }
            else
            {
                button.GetComponent<HoverText>().HideText();
                button.gameObject.SetActive(false);
            }
        } else
        {
            button.GetComponent<HoverText>().HideText();
            button.gameObject.SetActive(false);
        }
        
    }

    private void PrintCosts(List<Cost> costs)
    {
        goldPanel.SetActive(false);
        woodPanel.SetActive(false);
        foreach (Cost c in costs)
        {
            switch (c.type)
            {
                case ResourceType.GOLD:
                    if (c.amount != 0)
                    {
                        gold.text = c.amount.ToString();
                        goldPanel.SetActive(true);
                    }
                    break;

                case ResourceType.WOOD:
                    if (c.amount != 0)
                    {
                        wood.text = c.amount.ToString();
                        woodPanel.SetActive(true);
                    }
                    break;
            }
        }
    }
}
