﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusesUI : MonoBehaviour
{
    public BonusUI[] bonuses;

    public void UpdateUI(SelectableObject obj)
    {
        List<Bonus> bs = obj.bonuses.GetAllBonus();
        for (int i = 0; i < bonuses.Length; i++)
        {
            if (i < bs.Count && bs[i] != null)
            {
                bonuses[i].UpdateUI(bs[i]);
                bonuses[i].gameObject.SetActive(true);
            }
            else
            {
                bonuses[i].gameObject.SetActive(false);
            }
        } 
    }
}
