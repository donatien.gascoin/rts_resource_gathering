﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class BonusUI : MonoBehaviour
{
    public Image miniature;
    public Image detailImage;

    public TMP_Text detailText;

    public void UpdateUI(Bonus bonus)
    {
        detailText.text = bonus.GetBonusName();
        miniature.sprite = bonus.GetBonusImage();
        detailImage.sprite = bonus.GetBonusImage();
    }
}
