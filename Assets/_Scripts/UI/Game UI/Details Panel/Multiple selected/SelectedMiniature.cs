﻿using UnityEngine;
using UnityEngine.UI;

public class SelectedMiniature : MonoBehaviour
{
    public GameObject selectedPanel;
    public Image selectedPanelImage;
    /*public Slider selectedHealth;
    public Image selectedFill;*/
    public Image selectedImage;
    public Button selectedButton;
    public HealthSlider healthSlider;
    public ManaSlider manaSlider;

    public SelectedMiniatureButton button;

    /*public delegate void ClickOnButton();
    public static CancelUpgrade cancelUpgrade;*/
}
