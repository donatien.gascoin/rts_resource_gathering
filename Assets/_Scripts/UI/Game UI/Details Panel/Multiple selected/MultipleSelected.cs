﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleSelected : MonoBehaviour
{
    [SerializeField]
    private List<SelectedMiniature> miniatures;

    #region Switch unit group in selection panel
    [Space]
    private bool canSwitch;
    private int groupId = 0;

    private int currentlySelectedInUICount;
    private Player player;
    private float doubleClickTime = .3f;
    private float time = 0f;

    #endregion

    // 1 click : select group, 2 click : selecte unit
    private int currentNbSquadUnit = 0;
    private int clickOnPosition;

    private void Awake()
    {
        currentlySelectedInUICount = 0;
        clickOnPosition = -1;
    }

    private void Update()
    {
        if (canSwitch) // Different type of units selected
        {
            time += Time.deltaTime;
            if (currentlySelectedInUICount != player.squad.GetUnitsSelectedInUI().Count)
            {
                FindGroup(groupId);
            }
            if ((Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) && Input.GetKeyUp(KeyCode.Tab))
            {
                player.StopCustomAction();
                groupId = FindGroup(--groupId);
            }
            else if (Input.GetKeyUp(KeyCode.Tab))
            {
                player.StopCustomAction();
                groupId = FindGroup(++groupId);
            }
        }
    }

    public void UpdateUI(Player player)
    {
        this.player = player;
        List<SelectableObject> objs = player.squad.GetSquadUnits();
        for (int i = 0; i < miniatures.Count; i++)
        {
            miniatures[i].selectedButton.onClick.RemoveAllListeners();
            if (i < objs.Count)
            {
                miniatures[i].selectedPanel.SetActive(true);
                int x = i;
                miniatures[i].selectedButton.onClick.AddListener(() => ManageClickOnUI(x));
                miniatures[i].selectedImage.sprite = objs[i].image;
                miniatures[i].healthSlider.UpdateUI(objs[i]);
                if (objs[i].GetStats().useMana)
                {
                    miniatures[i].manaSlider.gameObject.SetActive(true);
                    miniatures[i].manaSlider.UpdateUI(objs[i]);
                } else
                {
                    miniatures[i].manaSlider.gameObject.SetActive(false);
                }
            } else
            {
                miniatures[i].selectedPanel.SetActive(false);
            }
        }
        

        if (player.squad.IsNewSelection())
        {
            groupId = 0;
            FindGroup(groupId);
            player.squad.CheckNewSelection();
        } else
        {
            int currentSquadSize = player.squad.GetSquadSize();
            if (currentNbSquadUnit != currentSquadSize)
            {
                currentNbSquadUnit = currentSquadSize;
                // Redo current UI Group, an unit has been added or removed from the selection
                FindGroup(groupId);
            }
        }

    }

    /**
     * One click : Select UI Group / Double click : Select unit
     */
    public void ManageClickOnUI(int position)
    {
        if (position > player.squad.GetSquadSize())
            return;

        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            player.RemoveFromSelectionAtPosition(position);
            return;
        }

        if (clickOnPosition == position && time < doubleClickTime)
        {
            player.SelectAtPosition(position);
        } else
        {
            clickOnPosition = position;
            time = 0f;
            FindGroup(FindUnitGroup(position));
        }
    }

    private int FindUnitGroup(int position)
    {
        int group = 0;
        List<SelectableObject> objs = player.squad.GetSquadUnits();
        SelectableObject obj = objs[position];

        Dictionary<int, List<SelectableObject>> dic = player.squad.GetSquadUnitsByUiNumber();
        foreach (KeyValuePair <int, List<SelectableObject>> k in dic) {
            if (k.Value.Contains(obj))
            {
                group = k.Key;
            }
        }

        return group;
    }

    private int FindGroup(int showGroup)
    {
        List<SelectableObject> units = player.squad.GetSquadUnits();
        Dictionary<int, List<SelectableObject>> dic = player.squad.GetSquadUnitsByUiNumber();
        List<int> keys = new List<int>();

        foreach (int i in dic.Keys)
        {
            keys.Add(i);
        }
        canSwitch = dic.Keys.Count > 1;
        if (keys.Count != 0) {
            if (showGroup > keys[keys.Count - 1])
            {
                showGroup = 0;
            }
            else if (showGroup < 0)
            {
                showGroup = keys[keys.Count - 1];
            }

            List<SelectableObject> selectedUnits = dic[showGroup];
            int counter = 0;

            for (int i = 0; i < DataManager.MAX_UNIT_PER_SELECTION; i++)
            {
                if (counter < selectedUnits.Count && units[i].GetComponent<SelectableObject>().Equals(selectedUnits[counter].GetComponent<SelectableObject>()))
                {
                    Color color = miniatures[i].selectedPanelImage.color;
                    miniatures[i].selectedPanelImage.color = new Color(color.r, color.g, color.b, 0.5f);
                    counter++;
                }
                else
                {
                    Color color = miniatures[i].selectedPanelImage.color;
                    miniatures[i].selectedPanelImage.color = new Color(color.r, color.g, color.b, 0f);
                }
            }
            player.squad.SetUnitsSelectedInUI(selectedUnits);
            currentlySelectedInUICount = selectedUnits.Count;
        }
        return showGroup;
    }
}
