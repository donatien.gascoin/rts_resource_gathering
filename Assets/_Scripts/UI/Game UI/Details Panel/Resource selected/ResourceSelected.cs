﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ResourceSelected : MonoBehaviour
{
    public TMP_Text resourceName;
    public TMP_Text resourceRemaining;
    public Image image;
    public void UpdateUI(Resource resource)
    {
        resourceName.text = resource.objectName;
        resourceRemaining.text = resource.amountLeft + " / " + resource.capacity;
        image.sprite = resource.image;
    }
}
