﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class SelectedImage : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Image image;
    public TMP_Text health;
    public TMP_Text mana;
    private SelectableObject obj;

    private const string EMPTY = "";
    private const string SEPARATOR = " / ";

    public void UpdateUI(SelectableObject obj)
    {
        this.obj = obj;
        image.sprite = obj.image;

        health.text = (int)obj.hitPoints + SEPARATOR + (int)obj.GetStats().maxHitPoint;
        health.color = ResourceManager.ColorSlider(obj.hitPoints / (float)obj.GetStats().maxHitPoint);

        if (obj.GetStats().useMana)
        {
            mana.text = (int)obj.manaPoints + SEPARATOR + (int)obj.GetStats().maxManaPoint;
        } else
        {
            mana.text = EMPTY;
        }
    }

    // Target unit with camera
    public void OnPointerDown(PointerEventData eventData)
    {
        TargetManager.targetObject(obj);
    }

    // Release target
    public void OnPointerUp(PointerEventData eventData)
    {
        TargetManager.releaseTarget();
    }
}
