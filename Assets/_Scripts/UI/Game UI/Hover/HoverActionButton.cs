﻿using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class HoverActionButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject actionDetailsPanel;

    public void OnPointerEnter(PointerEventData eventData)
    {
        ShowPanel();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        HidePanel();
    }

    public void HidePanel()
    {
        actionDetailsPanel.SetActive(false);
    }

    public void ShowPanel()
    {
        actionDetailsPanel.SetActive(true);
    }
}
