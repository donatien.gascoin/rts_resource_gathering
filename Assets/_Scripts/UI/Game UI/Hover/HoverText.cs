﻿using UnityEngine;
using UnityEngine.EventSystems;

public class HoverText : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject textToHover;

    public void OnPointerEnter(PointerEventData eventData)
    {
        ShowText();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        HideText();
    }

    public void HideText()
    {
        textToHover.SetActive(false);
    }

    public void ShowText()
    {
        textToHover.SetActive(true);
    }
}
