﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class ConstructionQueue : MonoBehaviour
{
    [SerializeField]
    private TMP_Text selectedName;
    [SerializeField]
    private List<CreationSlot> creationSlots;
    [SerializeField]
    private Slider creationTime;

    public void UpdateUI(Building obj)
    {
        selectedName.text = obj.objectName;
        // Set slider
        if (obj.buildQueue.Count != 0)
        {
            creationTime.gameObject.SetActive(true);
            creationTime.maxValue = obj.currentBuildCreationTime;
            creationTime.value = obj.currentBuildProgress;

        } else
        {
            creationTime.gameObject.SetActive(false);
        }
        // Set queue
        BuildObject[] queue = obj.buildQueue.ToArray();
        for(int i = 0; i < creationSlots.Count; i++)
        {
            creationSlots[i].creationButton.onClick.RemoveAllListeners();
            if (i < queue.Length)
            {
                int x = i;
                creationSlots[i].creationButton.image.sprite = queue[i].image;
                creationSlots[i].creationButton.onClick.AddListener(() => obj.RemoveFromConstructionQueue(x));
                creationSlots[i].gameObject.SetActive(true);
            } else
            {
                creationSlots[i].gameObject.SetActive(false);
            }
        }
    }
}
