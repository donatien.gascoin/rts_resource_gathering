﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeSlider : MonoBehaviour
{
    public Slider slider;

    public void UpdateUI(float currentTime, float maxTime)
    {
        slider.maxValue = maxTime;
        slider.value = currentTime;
    }
}
