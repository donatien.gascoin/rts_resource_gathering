﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthSlider : MonoBehaviour
{
    public Slider slider;
    public Image fillImage;

    public void UpdateUI(SelectableObject obj)
    {
        slider.maxValue = obj.GetStats().maxHitPoint;
        slider.value = obj.hitPoints;
        fillImage.color = ResourceManager.ColorSlider(obj.hitPoints / (float)obj.GetStats().maxHitPoint);
    }
}
