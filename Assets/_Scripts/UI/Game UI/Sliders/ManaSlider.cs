﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaSlider : MonoBehaviour
{
    public Slider slider;

    public void UpdateUI(SelectableObject obj)
    {
        slider.maxValue = obj.GetStats().maxManaPoint;
        slider.value = obj.manaPoints;
    }
}
