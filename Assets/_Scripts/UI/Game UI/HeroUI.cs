﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HeroUI : MonoBehaviour
{
    public GameObject heroSlot;
    public Image heroImage;
    public HealthSlider healthSlider;

    public void UpdateUI(SelectableObject obj)
    {
        if (obj != null)
        {
            heroSlot.SetActive(true);
            heroImage.sprite = obj.image;
            healthSlider.UpdateUI(obj);
        } else
        {
            heroSlot.SetActive(false);
        }
    }
}
