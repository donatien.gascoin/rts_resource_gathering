﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CancelUpgrade : MonoBehaviour
{
    public HoverText hoverText;

    public void DoCancel()
    {
        if (hoverText)
            hoverText.HideText();
        UIManager.cancelUpgrade();
    }
}
