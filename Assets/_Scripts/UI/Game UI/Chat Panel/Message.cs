﻿using UnityEngine;
using TMPro;

public class Message : MonoBehaviour
{
    public float messageDuration = 3f;
    private float messageTime = 0f;
    private bool startTimer = false;
    public string text;
    public TMP_Text textObject;
    public MessageType type;

    private void Update()
    {
        if (startTimer)
        {
            if (messageTime >= messageDuration)
            {
                Destroy(gameObject);
            }
            else
            {
                messageTime += Time.deltaTime;
            }
        }
    }

    public void SetupMessage(string text)
    {
        this.text = text;

        textObject = this.GetComponent<TMP_Text>();
        textObject.text = this.text;

        this.startTimer = true;
    }

    public enum MessageType
    {
        playerMessage,
        info,
        quest_new,
        quest_updated,
        quest_finished,
        cheat
    }
}
