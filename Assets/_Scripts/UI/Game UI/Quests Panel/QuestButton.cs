﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class QuestButton : MonoBehaviour
{
    public Button questButton;

    public TMP_Text questName;
    public Image questStatus;

    public Sprite succedQuest;
    public Sprite ongoingQuest;
    public Sprite unknownQuest;

    private Quest quest;
    private QuestsPanel questsPanel;

    public void UpdateUI(Quest quest)
    {
        questButton.onClick.RemoveAllListeners();
        questButton.onClick.AddListener(ShowQuestDetails);
        if (quest.isActive)
        {
            questName.text = PrintQuestName(quest.questName);
            // questStatus.sprite = ongoingQuest;
            questStatus.gameObject.SetActive(false);
        }
        else if (quest.isCompleted)
        {
            questName.text = PrintQuestName(quest.questName);
            questStatus.sprite = succedQuest;
            questStatus.gameObject.SetActive(true);
        }
        else
        {
            questButton.onClick.RemoveAllListeners();
            questName.text = "Undiscovered";
            questStatus.sprite = unknownQuest;
            questStatus.gameObject.SetActive(true);
        }

        

        this.quest = quest;
    }

    private string PrintQuestName(string questName)
    {
        return questName.Length > 18 ? questName.Substring(0, 15) + "..." : questName;
    }

    public void SetQuestsPanel(QuestsPanel panel)
    {
        this.questsPanel = panel;
    }

    public void ShowQuestDetails()
    {
        questsPanel.ShowQuestDetails(quest);
    }
}
