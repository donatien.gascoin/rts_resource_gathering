﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenQuestsPanel : MonoBehaviour
{

    private void Update()
    {
        if (!GameManager.isGameOver() && Input.GetKeyUp(KeyCode.F12))
        {
            OpenQuests();
        }
    }


    public void OpenQuests()
    {
        QuestsPanel.changeQuestsPanelState();
    }
}
