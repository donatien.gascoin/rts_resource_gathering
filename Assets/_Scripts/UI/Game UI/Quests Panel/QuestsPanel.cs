﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class QuestsPanel : MonoBehaviour
{
    public GameObject questPanel;

    public QuestButton[] questButtons;

    public TMP_Text questDetailsName;
    public TMP_Text questDetailsDescription;

    public delegate void UpdateQuestsInterface(Quest[] quests);
    public static UpdateQuestsInterface updateQuestsInterface;

    public delegate void ShowQuests();
    public static ShowQuests changeQuestsPanelState;

    private void Awake()
    {
        updateQuestsInterface += UpdateUI;
        changeQuestsPanelState += ChangeQuestsState;
    }

    private void Start()
    {
        for (int i = 0; i < questButtons.Length; i++)
            questButtons[i].SetQuestsPanel(this);

        questDetailsName.text = "";
        questDetailsDescription.text = "";
    }

    private void OnDestroy()
    {
        updateQuestsInterface -= UpdateUI;
        changeQuestsPanelState -= ChangeQuestsState;
    }

    public void UpdateUI(Quest[] quests)
    {
        for(int i = 0; i < questButtons.Length; i++)
        {
            if (i < quests.Length)
            {
                questButtons[i].gameObject.SetActive(true);
                questButtons[i].UpdateUI(quests[i]);
            } else
            {
                questButtons[i].gameObject.SetActive(false);
            }
        }

        if (questDetailsName.text == "" && quests.Length > 0)
        {
            ShowQuestDetails(quests[0]);
        }
    }

    public void ShowQuestDetails(Quest quest)
    {
        questDetailsName.text = quest.questName;
        questDetailsDescription.text = quest.description;
    }

    public void ShowQuestsPanel()
    {
        this.questPanel.SetActive(true);
    }

    public void HideQuestsPanel()
    {
        this.questPanel.SetActive(false);
    }

    public void ChangeQuestsState()
    {
        if (this.questPanel.activeSelf)
            HideQuestsPanel();
        else
            ShowQuestsPanel();
    }
}
