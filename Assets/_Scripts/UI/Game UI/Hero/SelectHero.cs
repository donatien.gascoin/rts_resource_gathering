﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectHero : MonoBehaviour, IPointerClickHandler
{
    private float doubleClickTimeLimit = 0.25f;

    protected void Start()
    {
        StartCoroutine(InputListener());
    }
    private IEnumerator InputListener()
    {
        while (enabled)
        { //Run as long as this is activ

            if (Input.GetKeyUp(KeyCode.F1))
            {
                SingleClick();
                yield return ClickEvent();
            }

            yield return null;
        }
    }
    private IEnumerator ClickEvent()
    {
        //pause a frame so you don't pick up the same mouse down event.
        // yield return new WaitForEndOfFrame();
        yield return null; // wait for the next frame

        float count = 0f;
        while (count < doubleClickTimeLimit)
        {
            if (Input.GetKeyUp(KeyCode.F1))
            {
                DoubleClick();
                yield break;
            }
            count += Time.deltaTime;// increment counter by change in time between frames
            yield return null; // wait for the next frame
        }
    }


    private void SingleClick()
    {
        Select(false);
    }

    private void DoubleClick()
    {
        Select(true);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        int clickCount = eventData.clickCount;

        if (clickCount == 1)
            Select(false);
        else if (clickCount >= 2)
            Select(true);
    }

    public void Select(bool focus)
    {
        UIManager.selectHero(focus);
    }
}
