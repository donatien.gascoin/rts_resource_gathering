﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScrollBarSize : MonoBehaviour
{
    public Scrollbar scrollbar;
    public float size;
    // Start is called before the first frame update
    void Start()
    {
        scrollbar.size = size;
    }

    // Update is called once per frame
    void Update()
    {
        scrollbar.size = size;
    }

    private void LateUpdate()
    {
        scrollbar.size = size;
    }

    private void FixedUpdate()
    {
        scrollbar.size = size;
    }
}
