﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class PlayerResultDetails : MonoBehaviour
{
    public Image winStatus;
    public Sprite winImage;
    public Sprite looseImage;

    public TMP_Text playerName;
    public TMP_Text unitCreated;
    public TMP_Text unitKilled;
    public TMP_Text buildingCreated;
    public TMP_Text buildingKilled;
    public TMP_Text largestArmy;

    public void UpdateUI(Player player)
    {
        if (!player.IsDead())
            winStatus.sprite = winImage;
        else
            winStatus.sprite = looseImage;

        playerName.text = player.username;

        unitCreated.text = "0";
        unitKilled.text = "0";
        buildingCreated.text = "0";
        buildingKilled.text = "0";
        largestArmy.text = "0";

    }

}
