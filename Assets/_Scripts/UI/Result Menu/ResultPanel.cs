﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class ResultPanel : MonoBehaviour
{
    public GameObject resultPanel;
    public GameObject endGamePanel;

    public GameObject scoreDefaultPanel;
    public List<PlayerResult> playerResults;
    public GameObject scoreDetailPanel;
    public List<PlayerResultDetails> playerResultsDetails;
    public TMP_Text gameDuration;
    // Start is called before the first frame update
    public void UpdateUI (List<Player> players, float gameDurationInSeconds)
    {
        scoreDefaultPanel.SetActive(true);
        scoreDetailPanel.SetActive(false);
        for (int i = 0; i < playerResults.Count; i++)
        {
            if ( i < players.Count)
            {
                playerResults[i].gameObject.SetActive(true);
                playerResults[i].UpdateUI(players[i]);

                playerResultsDetails[i].gameObject.SetActive(true);
                playerResultsDetails[i].UpdateUI(players[i]);
            } else
            {
                playerResults[i].gameObject.SetActive(false);
                playerResultsDetails[i].gameObject.SetActive(false);
            }
        }

        int displayMinutes = (int)(gameDurationInSeconds / 60) % 60;
        int displayHours = (int)(gameDurationInSeconds / 3600) % 24;
        gameDuration.text = displayHours + "h" + (displayMinutes < 10 ? "0" + displayMinutes.ToString() : displayMinutes.ToString());


        resultPanel.SetActive(false);
        endGamePanel.SetActive(true);
    }

    public void ReturnToMenu()
    {
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex);
        Resources.UnloadUnusedAssets();
        Time.timeScale = 1f;
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    public void ShowStats()
    {
        Debug.Log("show stats");
        endGamePanel.SetActive(false);
        resultPanel.SetActive(true);
    }
}
