﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class PlayerResult : MonoBehaviour
{
    public Image winStatus;
    public Sprite winImage;
    public Sprite looseImage;

    public TMP_Text playerName;
    public TMP_Text team;

    public void UpdateUI(Player player)
    {
        if (!player.IsDead())
            winStatus.sprite = winImage;
        else
            winStatus.sprite = looseImage;

        playerName.text = player.username;

        team.text = player.team.ToString();

    }
}
