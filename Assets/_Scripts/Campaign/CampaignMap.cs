﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Campaign map", menuName = "Maps/Campaign map")]
public class CampaignMap : ScriptableObject
{
    public string mapName;
    public string sceneName;
    [TextArea()]
    public string mapDescription;
}
