﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class ChatManager : MonoBehaviour
{
    public string userName;
    public int maxMessages = 25;

    public GameObject chatPanel, textObject;
    public TMP_InputField chatBox;

    public Color playerMessage, info;

    // [SerializeField]
    List<Message> messages = new List<Message>();

    private float checkTime = .5f, currentTime = 0f;

    public GameObject chatInput;
    public TMP_InputField chatInputText;
    public KeyCode openChatKey;
    private bool isOpen = false;
    List<Cost> additionalResources = new List<Cost>();

    public delegate bool IsChatOpen();
    public static IsChatOpen isChatOpen;

    public delegate ChatManager GetChat();
    public static GetChat getChatManager;

    private void Awake()
    {
        getChatManager += GetChatManager;
        isChatOpen += IsChatInputOpen;
        isOpen = false;
        chatInput.SetActive(false);

        additionalResources.Add(new Cost(ResourceType.GOLD, 10000));
        additionalResources.Add(new Cost(ResourceType.WOOD, 10000));
    }

    private void OnDestroy()
    {
        getChatManager -= GetChatManager;
        isChatOpen -= IsChatInputOpen;
    }

    private void Update()
    {
        if (Input.GetKeyUp(openChatKey))
        {
            if (isOpen)
            {
                if (CheckCheatCode())
                {
                    SendMessageToChat("Cheat code used", Message.MessageType.cheat);
                } else
                {
                    SendMessageToChat(GameManager.getHumanPlayer().username + ": " + chatInputText.text, Message.MessageType.playerMessage);
                }

            }
            isOpen = !isOpen;
            chatInput.SetActive(isOpen);
            if (isOpen)
            {
                chatInputText.text = "";
                chatInputText.Select();
                chatInputText.ActivateInputField();
            }
        }/* else
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                SendMessageToChat("You pressed space !", Message.MessageType.info);
            }
            else if (Input.GetKeyDown(KeyCode.F1))
            {
                SendMessageToChat("You must destroy the rebel base.For that, you will need to cross the river, your army is waiting for you.Keep in mind that they will not wait for you, and you may be under attack sooner than expected...", Message.MessageType.quest_new);
            }
            else if (Input.GetKeyDown(KeyCode.F2))
            {
                SendMessageToChat("Quest updated", Message.MessageType.quest_updated);
            }
            else if (Input.GetKeyDown(KeyCode.F3))
            {
                SendMessageToChat("Quest finished !", Message.MessageType.quest_finished);
            }
        }*/

        if (currentTime >= checkTime)
        {
            currentTime = 0f;
            for (int i = messages.Count - 1; i > 0; i--)
            {
                if (messages[i] == null)
                {
                    messages.RemoveAt(i);
                }
            }
        }
        else
        {
            currentTime += Time.deltaTime;
        }

    }

    private bool CheckCheatCode()
    {
        if (chatInputText.text.Equals("money"))
        {
            List<Player> players = GameManager.getPlayers();
                
            for (int i = 0; i < players.Count; i++)
            {
                players[i].AddResources(additionalResources);
            }
            return true;
        }

        return false;
    }

    private ChatManager GetChatManager()
    {
        return this;
    }

    private bool IsChatInputOpen()
    {
        return isOpen;
    }

    private void SendMessageToChat(string text, Message.MessageType messageType)
    {
        if (messages.Count >= maxMessages)
        {
            if (messages[0] != null)
            {
                Destroy(messages[0].textObject.gameObject);
            }
            messages.RemoveAt(0);
        }

        GameObject newText = Instantiate(textObject, chatPanel.transform);
        Message m = newText.GetComponent<Message>();
        m.SetupMessage(DecorateMessage(messageType, text));


        messages.Add(m);
    }

    private string DecorateMessage(Message.MessageType type, string text)
    {
        string decoratedText = text;
        switch (type)
        {
            case Message.MessageType.quest_new:
                decoratedText = "<size=+2><color=orange><b>New quest : " + text + " </b></color></size>";
                break;
            case Message.MessageType.quest_updated:
                decoratedText = "<size=+2><color=blue><b>Quest " + text + " updated</b></color></size>";
                break;
            case Message.MessageType.quest_finished:
                decoratedText = "<size=+2><color=green><b>Quest " + text + " finished !</b></color></size>";
                break;
            case Message.MessageType.cheat:
                decoratedText = "<color=yellow><b>Cheat code applied</b></color>";
                break;
        }
        return decoratedText;
    }

    public void SendNewQuestMessage(string questName, string questDetails)
    {
        SendMessageToChat(questName, Message.MessageType.quest_new);
        SendMessageToChat(questDetails, Message.MessageType.info);
    }

    public void SendFinishedQuestMessage(string questName)
    {
        SendMessageToChat(questName, Message.MessageType.quest_finished);
    }

}
