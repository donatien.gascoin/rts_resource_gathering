﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ColorManager : MonoBehaviour
{
    public List<PlayerTextures> playerObjectColor;

    public Material currentPlayer;
    public Material allyPlayer;
    public Material enemyPlayer;


    public delegate Material GetMaterial(PlayerColor color, string obj);
    public static GetMaterial getMaterial;

    public delegate Material GetMiniMaterial(PlayerColor color, MiniMapMode mode, string team);
    public static GetMiniMaterial getMiniMaterial;

    public delegate GameObject GetBanner(PlayerColor color);
    public static GetBanner getBanner;

    private void Awake()
    {
        getMaterial += GetMaterialForUnitAndColor;
        getBanner += GetBannerForColor;
        getMiniMaterial += GetMiniMapMaterialForUnitAndColor;
    }

    private void OnDestroy()
    {
        getMaterial -= GetMaterialForUnitAndColor;
        getBanner -= GetBannerForColor;
        getMiniMaterial -= GetMiniMapMaterialForUnitAndColor;
    }

    private Material GetMaterialForUnitAndColor(PlayerColor color, string obj)
    {
        Material mat = null;
        foreach (PlayerTextures pt in playerObjectColor)
        {
            if (pt.color == color)
            {
                switch (obj)
                {
                    case DataManager.UNIT_MATERIAL:
                        mat = pt.unitMat;
                        break;
                    case DataManager.BUILDING_MATERIAL:
                        mat = pt.buildingMat;
                        break;
                    case DataManager.MINIMAP_MATERIAL:
                        mat = pt.miniMapMat;
                        break;
                }
            }
        }
        return mat;
    }

    private Material GetMiniMapMaterialForUnitAndColor(PlayerColor color, MiniMapMode mode, string team)
    {
        Material mat = null;
        switch(mode)
        {
            case MiniMapMode.TEAM_COLOR:
                switch (team)
                {
                    case DataManager.CURRENT_PLAYER:
                        mat = currentPlayer;
                        break;
                    case DataManager.ENEMY_PLAYER:
                        mat = enemyPlayer;
                        break;
                    case DataManager.ALLY_PLAYER:
                        mat = allyPlayer;
                        break;
                }
                break;
            case MiniMapMode.EACH_COLOR:
            default:
                foreach (PlayerTextures pt in playerObjectColor)
                    if (pt.color == color)
                    {
                        mat = pt.miniMapMat;
                        break;
                    }
            break;
        }
        return mat;
    }

    public static PlayerColor MatchColor(string color)
    {
        PlayerColor pColor;
        bool success = Enum.TryParse<PlayerColor>(color, out pColor);

        return success ? pColor : PlayerColor.BLUE;
    }

    private GameObject GetBannerForColor(PlayerColor color)
    {
        GameObject obj = null;
        foreach (PlayerTextures pt in playerObjectColor)
        {
            if (pt.color == color)
            {
                obj = pt.banner;
            }
        }
        return obj;
    }
}
[System.Serializable]
public class PlayerTextures
{
    public PlayerColor color;

    public Material unitMat;
    public Material buildingMat;
    public Material miniMapMat;
    public GameObject banner;
}

[System.Serializable]
public enum PlayerColor
{
    BLUE, BLUE_B, RED, ORANGE, PURPLE, GREEN, BLACK, BROWN, GREEN_B, PINK, WHITE, YELLOW
}
