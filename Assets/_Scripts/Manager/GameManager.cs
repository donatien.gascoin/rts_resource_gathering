﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public List<VictoryCondition> victoryConditions;
    public List<Player> players;
    private Player humanPlayer;
    public ResultPanel resultPanel;


    public QuestManager questsManager;
    public GameObject openQuestPanel;

    private bool gameIsOver;

    public delegate string GetPlayerStatus(Player p);
    public static GetPlayerStatus getPlayerStatus;

    public delegate List<SelectableObject> GetAllObjects();
    public static GetAllObjects getAllObjects;

    public delegate List<Player> GetPlayers();
    public static GetPlayers getPlayers;

    public delegate int GetHumanPlayerTeam();
    public static GetHumanPlayerTeam getHumanPlayerTeam;

    public delegate Player GetHumanPlayer();
    public static GetHumanPlayer getHumanPlayer;

    public delegate bool IsGameOver();
    public static IsGameOver isGameOver;

    private void Awake()
    {
        gameIsOver = false;
        getPlayerStatus += IsPlayerEnemyOrAlly;
        getAllObjects += GetPlayersUnits;
        getPlayers += GetGamePlayers;
        getHumanPlayerTeam += GetHumanTeam;
        isGameOver += IsOver;

        getHumanPlayer += GetHuman;
    }

    private void OnDestroy()
    {
        getPlayerStatus -= IsPlayerEnemyOrAlly;
        getAllObjects -= GetPlayersUnits;
        getPlayers -= GetGamePlayers;
        getHumanPlayerTeam -= GetHumanTeam;
        isGameOver -= IsOver;

        getHumanPlayer -= GetHuman;
    }

    void LateUpdate()
    {
        if (victoryConditions != null && !gameIsOver)
        {
            foreach (VictoryCondition victoryCondition in victoryConditions)
            {
                if (victoryCondition.GameFinished())
                {
                    Time.timeScale = 0.0f;
                    gameIsOver = true;
                    UIManager.hideGameUIElement();

                    // Show victory type ?
                    /*resultPanel.SetMetVictoryCondition(victoryCondition);*/
                    resultPanel.UpdateUI(players, Time.timeSinceLevelLoad);
                }
            }
        }
    }

    public void SetPlayers(List<Player> players)
    {
        this.players = players;

        for (int i = 0; i < players.Count; i++)
            if (players[i].human)
                humanPlayer = players[i];
    }

    public void SetVictoryConditions(List<VictoryCondition> conditions)
    {
        bool isQuest = false;
        victoryConditions = conditions;
        for (int i = 0; i < conditions.Count; i++)
        {
            if (conditions[i] as QuestManager != null)
            {
                isQuest = true;
                (conditions[i] as QuestManager).StartQuests();
                break;
            }
        }

        openQuestPanel.SetActive(isQuest);


    }

    public string IsPlayerEnemyOrAlly(Player p )
    {
        if (humanPlayer == p)
        {
            return DataManager.CURRENT_PLAYER;
        } else
        {
            if (humanPlayer.team == p.team)
                return DataManager.ALLY_PLAYER;
            else
                return DataManager.ENEMY_PLAYER;
        }
    }

    public List<SelectableObject> GetPlayersUnits()
    {
        List<SelectableObject> objs = new List<SelectableObject>();

        for (int i = 0; i < players.Count; i++)
        {
            objs.AddRange(players[i].units);
            objs.AddRange(players[i].buildings);
        }
        return objs;
    }

    public int GetHumanTeam()
    {
        return humanPlayer.team;
    }

    public Player GetHuman()
    {
        return humanPlayer;
    }

    public List<Player> GetGamePlayers()
    {
        return players;
    }

    public bool IsOver()
    {
        return gameIsOver;
    }

}
