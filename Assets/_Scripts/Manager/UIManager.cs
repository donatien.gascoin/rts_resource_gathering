﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using System.Globalization;
using UnityEngine.Events;

public class UIManager : MonoBehaviour
{
    // The human player
    public Player player;

    private bool stopShowingUI;

    #region Switch unit group in selection panel

    [HideInInspector]
    public bool showNewSelection = true;

    #endregion

    #region Resources

    public ResourcesPanel resourcesPanel;

    /*[SerializeField]
    private TMP_Text goldTxt = null;
    [SerializeField]
    private TMP_Text woodTxt = null;
    [SerializeField]
    private TMP_Text foodTxt = null;

    [SerializeField]
    private Image goldPanel = null;
    [SerializeField]
    private Image woodPanel = null;
    [SerializeField]
    private Image foodPanel = null;*/

    #endregion

    #region Non working harvester

    // UI Componenent
    [SerializeField]
    private GameObject notWorkingHarvester;
    // Use to show the next non-working harvester, and not only the first one
    private int notWorkingHarvesterPosition;

    #endregion

    #region Building creation material

    public Material canBuild;
    public Material cantBuild;

    #endregion

    #region UI Panels

    public HeroUI heroUI;

    private Resource selectedResources;
    private bool isResourceSelected;

    #region Action panel
    public GameObject actionPanel;
    public ActionsPanel actionsPanel;

    #endregion

    #region Details panel

    public GameObject detailsPanel;

    public GameObject selectedPanel;
    public SelectedImage selectedImage;

    public GameObject buildingPanel;
    public ConstructionQueue constructionQueue;

    public GameObject multipleSelectedPanel;
    public MultipleSelected multipleSelected;

    public GameObject singleSelectPanel;
    public SingleSelect singleSelect;

    public GameObject resourcePanel;
    public ResourceSelected resourceSelected;

    #endregion

    #endregion



    #region MonoBehavior methods

    private void Awake()
    {

        updateResource += UpdateResourceOnUI;
        updateFood += UpdateFoodOnUI;
        highlightResource += HighlightResourceOnUI;

        setUI += SelectObjects;
        selectResource += ClickOnResource;
        hideActions += HideSelectedObject;
        removeUnitFromUI += RemoveUnit;
        replaceUnitFromUI += ReplaceUnit;

        getBuildMaterial += GetMaterial;
        hideGameUIElement += HideGameUIElementsWhenGameIsOver;

        selectNonWorkerWorker += ShowNextNonWorkingWorker;
        selectHero += SelectHero;

        moveSquad += MoveSquadFromMiniMap;

        cancelUpgrade += StopUpgrade;

        stopShowingUI = false;
    }

    void Start()
    {

        notWorkingHarvesterPosition = 0;

        actionPanel.SetActive(false);
        detailsPanel.SetActive(false);
        multipleSelectedPanel.SetActive(false);
        singleSelectPanel.SetActive(false);
        selectedPanel.SetActive(false);
        resourcePanel.SetActive(false);
    }

    void Update()
    {
        if (!stopShowingUI)
        {
            CheckNonWorkingWorkers();
            heroUI.UpdateUI(player.GetHero());

            if (isResourceSelected)
            {
                SetResourceOnUI();
                if (player.squad.IsNewSelection())
                {
                    isResourceSelected = false;
                    selectedResources = null;
                }

            } else
            {
                SetDetailsOnUI();
                SetActionsOnUI();
            }
        }
    }

    void OnDestroy()
    {
        updateResource -= UpdateResourceOnUI;
        highlightResource -= HighlightResourceOnUI;
        updateFood -= UpdateFoodOnUI;

        setUI -= SelectObjects;
        selectResource -= ClickOnResource;
        hideActions -= HideSelectedObject;
        removeUnitFromUI -= RemoveUnit;
        replaceUnitFromUI -= ReplaceUnit;

        getBuildMaterial -= GetMaterial;

        hideGameUIElement -= HideGameUIElementsWhenGameIsOver;

        selectNonWorkerWorker -= ShowNextNonWorkingWorker;
        selectHero -= SelectHero;

        moveSquad -= MoveSquadFromMiniMap;

        cancelUpgrade -= StopUpgrade;
    }

    public void SetPlayer(Player player)
    {
        this.player = player;
    }

    #endregion

    #region Selection UI

    public delegate void SetUI(List<SelectableObject> objs);
    public static SetUI setUI;

    public delegate void SelectResource(Resource resource);
    public static SelectResource selectResource;

    public delegate void HideActionsUI();
    public static HideActionsUI hideActions;

    public delegate void RemoveUnitFromUI(SelectableObject obj);
    public static RemoveUnitFromUI removeUnitFromUI;

    public delegate void ReplaceUnitFromUI(SelectableObject oldObj, SelectableObject newObj);
    public static ReplaceUnitFromUI replaceUnitFromUI;

    public delegate void HideUI();
    public static HideUI hideGameUIElement;

    public void ClickOnResource(Resource r)
    {
        selectedResources = r;
        isResourceSelected = true;
    }

    public void HideGameUIElementsWhenGameIsOver()
    {
        stopShowingUI = true;
        detailsPanel.SetActive(false);
        actionPanel.SetActive(false);
    }

    public void RemoveUnit(SelectableObject obj)
    {
        player.squad.RemoveSquadUnit(obj);
    }

    private void ReplaceUnit(SelectableObject oldObj, SelectableObject newObj)
    {
        player.squad.ReplaceSquadUnit(oldObj, newObj);
    }

    public void SelectObjects(List<SelectableObject> objs)
    {
        if(objs != null && objs.Count > 0)
        {
            SetActionsOnUI();
        }
    }

    public void SetActionsOnUI()
    {
        actionPanel.SetActive(true);
        if (player.squad.GetFirstSquadUnitInUnitsSelectedOnUI() && player.squad.IsUnitSelectedAHumanPlayerUnit())
            actionsPanel.UpdateUI(player.squad.GetFirstSquadUnitInUnitsSelectedOnUI());
        else
            actionPanel.SetActive(false);
    }

    public void SetResourceOnUI()
    {
        if (selectedResources != null)
        {
            selectedPanel.SetActive(false);
            buildingPanel.SetActive(false);
            multipleSelectedPanel.SetActive(false);
            singleSelectPanel.SetActive(false);

            detailsPanel.SetActive(true);
            resourcePanel.SetActive(true);

            resourceSelected.UpdateUI(selectedResources);
            return;
        } else
        {
            detailsPanel.SetActive(false);
            resourcePanel.SetActive(false);
        }
    }

    public void SetDetailsOnUI()
    {
        if (player.squad.GetSquadSize() == 0)
        {
            detailsPanel.SetActive(false);
            return;
        }

        resourcePanel.SetActive(false);
        detailsPanel.SetActive(true);

        if (player.squad.GetSquadSize() == 1)
        {
            player.squad.UseFirstUnitToSelectedUnitsOnUI();
        }

        selectedPanel.SetActive(true);
        if (player.squad.GetFirstSquadUnitInUnitsSelectedOnUI() != null)
            selectedImage.UpdateUI(player.squad.GetFirstSquadUnitInUnitsSelectedOnUI());
        if (player.squad.GetSquadSize() == 1)
        {
            SelectableObject obj = player.squad.GetFirstSquadUnitInUnitsSelectedOnUI();
            if (obj == null)
                return;
            Building b = obj.GetComponent<Building>();
            if (b != null && b.player.human && b.buildQueue.Count > 0)
            {
                constructionQueue.UpdateUI(b);

                multipleSelectedPanel.SetActive(false);
                singleSelectPanel.SetActive(false);
                buildingPanel.SetActive(true);
                return;

            }

            singleSelect.UpdateUI(player.squad.GetFirstSquadUnitInUnitsSelectedOnUI());

            buildingPanel.SetActive(false);
            multipleSelectedPanel.SetActive(false);

            singleSelectPanel.SetActive(true);

        } else
        {
            multipleSelected.UpdateUI(player);

            singleSelectPanel.SetActive(false);
            buildingPanel.SetActive(false);
            multipleSelectedPanel.SetActive(true);

        }

    }

    #region Hide panels

    public void HideSelectedObject()
    {
        /*currentlySelectedObjects = null;*/
        HideDetailsOnUI();
        HideActionsOnUI();
    }

    public void HideActionsOnUI()
    {
        actionPanel.SetActive(false);
    }

    public void HideDetailsOnUI()
    {
        detailsPanel.SetActive(false);
    }

    #endregion

    #endregion

    #region Select hero


    public delegate void UISelectHero(bool focus);
    public static UISelectHero selectHero;

    public void SelectHero(bool focus)
    {
        SelectableObject hero = player.GetHero();
        UserInput.selectObject(hero, focus);
    }

    #endregion

    #region Non working peasant


    public delegate void SelectNextNonWorkingWorker();
    public static SelectNextNonWorkingWorker selectNonWorkerWorker;

    private void CheckNonWorkingWorkers()
    {
        if (IsWorkerNotWorking())
        {
            notWorkingHarvester.SetActive(true);
        }
        else
        {
            notWorkingHarvester.SetActive(false);
        }
    }

    private bool IsWorkerNotWorking()
    {
        foreach (Unit u in player.units)
        {
            if (u == null)
                continue;
            Worker h = u.GetComponent<Worker>();
            if (h != null && !h.IsWorking())
            {
                return true;
            }
        }
        return false;
    }

    public void ShowNextNonWorkingWorker()
    {
        List<SelectableObject> harvesters = player.GetNonWorkingWorkers();

        if (notWorkingHarvesterPosition > (harvesters.Count - 1))
            notWorkingHarvesterPosition = 0;
        UserInput.selectObject(harvesters[notWorkingHarvesterPosition], true);
        notWorkingHarvesterPosition++;
    }

    #endregion

    #region Right click on minimap

    public delegate void MoveSquad(Vector3 position);
    public static MoveSquad moveSquad;

    public void MoveSquadFromMiniMap(Vector3 position)
    {
        player.squad.RightClick(position, FindObjectOfType<Terrain>().gameObject, Behaviour.NORMAL);
    }

    #endregion

    #region Update resources

    public delegate void HighlightResource(ResourceType type);
    public static HighlightResource highlightResource;

    public delegate void UpdateResource(ResourceType type, int _gold);
    public static UpdateResource updateResource;

    public delegate void UpdateFood(int consummedFood, int maxAllowedFood);
    public static UpdateFood updateFood;

    public void UpdateResourceOnUI(ResourceType item, int amount)
    {
        switch (item)
        {
            case ResourceType.WOOD:
                resourcesPanel.UpdateWoodAmount(amount);
                break;
            case ResourceType.GOLD:
                resourcesPanel.UpdateGoldAmount(amount);
                break;
        }
    }

    public void HighlightResourceOnUI(ResourceType item)
    {
        switch (item)
        {
            case ResourceType.WOOD:
                resourcesPanel.HighlightWoodUI();
                break;
            case ResourceType.GOLD:
                resourcesPanel.HighlightGoldUI();
                break;
            case ResourceType.FOOD:
                resourcesPanel.HighlightFoodUI();
                break;
        }
    }

    private void UpdateFoodOnUI(int consumed, int max)
    {
        resourcesPanel.UpdateFoodAmount(consumed, max);
    }

    #endregion

    #region Building construction

    public delegate Material CanBuild(bool canBuild);
    public static CanBuild getBuildMaterial;
    public Material GetMaterial(bool _canBuild)
    {
        return _canBuild ? canBuild : cantBuild;
    }

    #endregion

    #region Cancel upgrade

    public delegate void CancelUpgrade();
    public static CancelUpgrade cancelUpgrade;

    private void StopUpgrade()
    {
        player.CancelUpgrade();
    }

    #endregion
}

[Serializable]
public class ObjectCustomAction
{
    public Sprite image;
    public string text;
}