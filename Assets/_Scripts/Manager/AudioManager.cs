﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    public AudioMixerGroup audioMixerGroup;
    public AudioMixerGroup audioMixerGroupUnitSounds;

    public List<Sound> clips;

    public delegate AudioMixerGroup GetUnitsAudioMixer();
    public static GetUnitsAudioMixer getAudioMixer;

    public delegate void PlaySound(SoundCategorie clip);
    public static PlaySound playSound;


    private void Awake()
    {
        if (instance == null)
            DontDestroyOnLoad(this);
        else
            Destroy(this);

        playSound += PlaySoundClip;
        getAudioMixer += GetUnitAudioMixerGroup;
        for (int i = 0; i < clips.Count; i++)
        {
            AudioSource audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.playOnAwake = false;
            clips[i].SetAudioSource(audioSource);
            clips[i].SetAudioMixerGroup(audioMixerGroup);
        }

        PlaySoundClip(SoundCategorie.THEME);
    }

    private void OnDestroy()
    {
        playSound -= PlaySoundClip;
        getAudioMixer -= GetUnitAudioMixerGroup;
    }

    public void PlaySoundClip(SoundCategorie action)
    {
        for (int i = 0; i < clips.Count; i++)
            if (clips[i].soundCategorie == action)
                clips[i].PlaySound();
    }

    public void StopSoundClip(SoundCategorie action)
    {
        for (int i = 0; i < clips.Count; i++)
            if (clips[i].soundCategorie == action)
                clips[i].StopSound();
    }

    public AudioMixerGroup GetUnitAudioMixerGroup()
    {
        return audioMixerGroupUnitSounds;
    }
}

