﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : VictoryCondition
{
    protected Player humanPlayer;
    public Quest[] quests;

    public delegate Quest[] GetQuests();
    public static GetQuests fetchQuests;

    public void StartQuests()
    {

        if (quests.Length > 0)
        {
            ActiveQuest(quests[0]);
        }
        QuestsPanel.updateQuestsInterface(quests);
    }

    public QuestManager (Player[] players) : base(players)
    {
         
    }
    
    private void Awake()
    {
        fetchQuests += GetPlayerQuests;
    }

    private void OnDestroy()
    {
        fetchQuests -= GetPlayerQuests;
    }

    /*private void Start()
    {
        QuestsPanel.updateQuestsInterface(quests);

        if (quests.Length > 0)
        {
            ActiveQuest(quests[0]);
        }
    }*/

    public void SetHumanPlayer(Player player)
    {
        this.humanPlayer = player;
        for (int i = 0; i < quests.Length; i++)
            quests[i].SetPlayer(humanPlayer);
    }

    public Player GetHumanPlayer()
    {
        return humanPlayer;
    }

    public bool IsQuestsFinished()
    {
        if (humanPlayer == null)
        {
            for (int i = 0; i < players.Length; i++)
                if (players[i].human)
                {
                    SetHumanPlayer(players[i]);
                    break;
                }
            if (humanPlayer == null)
            {
                Debug.LogError("Impossible to find a human player");
                return true;
            }
        }
        bool isQuestSuccess = true;
        for(int j = 0; j < quests.Length; j++)
        {
            if (quests[j].isActive)
            {
                if (!quests[j].IsQuestCompleted(humanPlayer))
                {
                    isQuestSuccess = false;
                    break;
                } else
                {
                    quests[j].isActive = false;
                    if (quests[j].nextQuests != null)
                    {
                        isQuestSuccess = false;
                        // Current quest has been completed, and there is next quest to do
                        for (int x = 0; x < quests[j].nextQuests.Count; x++)
                            ActiveQuest(quests[j].nextQuests[x]);

                    }
                    QuestsPanel.updateQuestsInterface(quests);
                }
            }
        }

        return isQuestSuccess;
    }

    private void ActiveQuest(Quest quest)
    {
        Debug.LogWarning("Need to inform (MISSING SOUNDS) the player about the new quest");
        ChatManager.getChatManager().SendNewQuestMessage(quest.questName, quest.description);
        AudioManager.playSound(SoundCategorie.QUEST_NEW);
        quest.InitQuest();
        quest.isActive = true;
    }

    public override string GetDescription()
    {
        return "";
    }

    public override bool PlayerMeetsConditions(Player player)
    {
        return IsQuestsFinished();
    }

    public Quest[] GetPlayerQuests()
    {
        return quests;
    }
}
