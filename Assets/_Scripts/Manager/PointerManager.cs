﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerManager : MonoBehaviour
{
    [SerializeField]
    private GameObject animationMoveClick;
    [SerializeField]
    private Camera overFOWCamera;

    [SerializeField]
    private Texture2D gamePointer;
    [SerializeField]
    private Texture2D enemyPointer;
    [SerializeField]
    private Texture2D allyPointer;
    [SerializeField]
    private Texture2D customActionPointer;
    [SerializeField]
    private Texture2D removeFromSquadPointer;

    public Vector2 hotSpot = Vector2.zero;
    public CursorMode cursorMode = CursorMode.Auto;

    public delegate void MouseRightClickAnimation(Vector3 position);
    public static MouseRightClickAnimation mouseRightClickAnimation;

    public Player player;

    private void Start()
    {

        mouseRightClickAnimation += RightClickAnimation;
    }

    void OnDestroy()
    {
        mouseRightClickAnimation -= RightClickAnimation;
    }

    private void Update()
    {
        bool useDefaultCursor = true;
        Vector3 mousePosition = Input.mousePosition;
        if (mousePosition != ResourceManager.InvalidPosition)
        {
            GameObject hitObject = WorkManager.FindHitObject(mousePosition);
            if (hitObject)
            {
                if (player.IsDoingCustomAction()) {
                    useDefaultCursor = false;
                    Texture2D pointer = player.GetCustomActionObject().ManagerPointer(hitObject, player);
                    
                    Cursor.SetCursor(pointer != null ? pointer : customActionPointer, hotSpot, cursorMode);
                    /*switch (player.squad.GetNextBehaviour())
                    {
                        case Behaviour.NORMAL:
                        case Behaviour.DEFENSIVE:
                            SelectableObject obj = hitObject.GetComponentInParent<SelectableObject>();
                            if (obj && obj.player)
                            {
                                if (obj.player == player)
                                {
                                    if (IsHoldingShift())
                                    {
                                        if (player.squad.IsUnitOnSquad(obj))
                                        {
                                            // Remove unit to selection
                                            useDefaultCursor = false;
                                            Cursor.SetCursor(removeFromSquadPointer, hotSpot, cursorMode);
                                        } 
                                        else if (player.squad.IsUnitSelectedAHumanPlayerUnit())
                                        {
                                            // Add unit to selection
                                            useDefaultCursor = false;
                                            Cursor.SetCursor(allyPointer, hotSpot, cursorMode);
                                        }
                                    }
                                } else
                                {
                                    if (player.squad.IsUnitSelectedAHumanPlayerUnit())
                                    {
                                        Cursor.SetCursor(enemyPointer, hotSpot, cursorMode);
                                        useDefaultCursor = false;
                                    }
                                }
                            }
                            break;
                        case Behaviour.AGGRESSIVE:
                            Cursor.SetCursor(enemyPointer, hotSpot, cursorMode);
                            useDefaultCursor = false;
                            break;
                    }*/
                }
            }

        }
        
        if (useDefaultCursor)
        {
            Cursor.SetCursor(null, hotSpot, cursorMode);
        }
    }
    
    public void SetPlayer(Player player)
    {
        this.player = player;
    }

    private bool IsHoldingShift()
    {
        return Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
    }

    public void RightClickAnimation(Vector3 position)
    {
        if (player.squad.IsUnitSelectedAHumanPlayerUnit())
            Instantiate(animationMoveClick, position, transform.rotation);
    }
}
