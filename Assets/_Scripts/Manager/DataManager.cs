﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager
{
    public const int MAX_UNIT_PER_QUEUE = 8;
    public const int MAX_UNIT_PER_SELECTION = 12;
    public const int MAX_ACTIONS = 9;

    public const string UNIT_TAG = "Unit";
    public const string BUILDING_TAG = "Building";
    public const string GROUND_TAG = "Ground";

    public static Color32 COLOR_HIGH_HEALTH = new Color32(61, 183, 97, 255);
    public static Color32 COLOR_MIDDLE_HEALTH = new Color32(183, 141, 61, 255);
    public static Color32 COLOR_LOW_HEALTH = new Color32(203, 40, 46, 255);

    public const string UNIT_MATERIAL = "unit";
    public const string BUILDING_MATERIAL = "building";
    public const string BANNER_MATERIAL = "banner";
    public const string MINIMAP_MATERIAL = "banner";

    public const string CURRENT_PLAYER = "current_player";
    public const string ENEMY_PLAYER = "enemy_player";
    public const string ALLY_PLAYER = "ally_player";

    public const int BARRACKS_UI_NUMBER = 1;
    public const int MAGE_TOWER_UI_NUMBER = 2;
    public const int TOWN_HALL_UI_NUMBER = 3;
    public const int BLACKSMITH_UI_NUMBER = 5;
    public const int LUMBER_MILL_UI_NUMBER = 9;
    public const int FARM_UI_NUMBER = 10;
    public const int TOWER_UI_NUMBER = 10;


    public const int HERO_UI_NUMBER = 1;
    public const int WIZARD_UI_NUMBER = 2;
    public const int PRIEST_UI_NUMBER = 3;
    public const int KNIGHT_UI_NUMBER = 4;
    public const int FOOTMAN_UI_NUMBER = 5;
    public const int RANGER_UI_NUMBER = 6;
    public const int CATAPULT_UI_NUMBER = 8;
    public const int WORKER_UI_NUMBER = 10;

    public static string GetBuildingRequirementName(CustomActionName r)
    {
        SelectableObject obj = ResourceManager.GetBuilding(r).GetComponent<SelectableObject>();
        return obj != null ? obj.objectName : "";
    }

    public static string GetUnitRequirementName(CustomActionName r)
    {
        SelectableObject obj = ResourceManager.GetUnit(r).GetComponent<SelectableObject>();
        return obj != null ? obj.objectName : "";
    }

}
