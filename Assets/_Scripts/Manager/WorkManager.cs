﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkManager
{
    public static GameObject FindHitObject(Vector3 origin)
    {
        Ray ray = Camera.main.ScreenPointToRay(origin);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, ~(LayerMask.GetMask("CameraGround") | LayerMask.GetMask("Quest")))) return hit.collider.gameObject;
        return null;
    }

    public static Vector3 FindHitPoint(Vector3 origin)
    {
        Ray ray = Camera.main.ScreenPointToRay(origin);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, ~(LayerMask.GetMask("CameraGround") | LayerMask.GetMask("Quest")))) return hit.point;
        return ResourceManager.InvalidPosition;
    }

    public static List<SelectableObject> FindNearbyObjects(Vector3 position, float range)
    {
        Collider[] hitColliders = Physics.OverlapSphere(position, range);
        HashSet<int> nearbyObjectIds = new HashSet<int>();
        List<SelectableObject> nearbyObjects = new List<SelectableObject>();
        for (int i = 0; i < hitColliders.Length; i++)
        {
            // Transform parent = hitColliders[i].transform.parent;
            if (hitColliders[i])
            {
                SelectableObject parentObject = hitColliders[i].GetComponent<SelectableObject>();
                if (parentObject && !nearbyObjectIds.Contains(parentObject.ObjectId))
                {
                    nearbyObjectIds.Add(parentObject.ObjectId);
                    nearbyObjects.Add(parentObject);
                }
            }
        }
        return nearbyObjects;
    }

    public static List<WorldObject> FindNearbyWorldObjects(Vector3 position, float range)
    {
        Collider[] hitColliders = Physics.OverlapSphere(position, range);
        HashSet<int> nearbyObjectIds = new HashSet<int>();
        List<WorldObject> nearbyObjects = new List<WorldObject>();
        for (int i = 0; i < hitColliders.Length; i++)
        {
            // Transform parent = hitColliders[i].transform.parent;
            if (hitColliders[i])
            {
                WorldObject parentObject = hitColliders[i].GetComponent<WorldObject>();
                if (parentObject && !nearbyObjectIds.Contains(parentObject.ObjectId))
                {
                    nearbyObjectIds.Add(parentObject.ObjectId);
                    nearbyObjects.Add(parentObject);
                }
            }
        }
        return nearbyObjects;
    }

    public static List<Resource> FindNearbyResource(Vector3 position, float range)
    {
        Collider[] hitColliders = Physics.OverlapSphere(position, range);
        HashSet<int> nearbyObjectIds = new HashSet<int>();
        List<Resource> nearbyObjects = new List<Resource>();
        for (int i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i])
            {
                Resource parentObject = hitColliders[i].GetComponentInParent<Resource>();
                if (parentObject && !nearbyObjectIds.Contains(parentObject.ObjectId))
                {
                    nearbyObjectIds.Add(parentObject.ObjectId);
                    nearbyObjects.Add(parentObject);
                }
            }
        }
        return nearbyObjects;
    }

    public static bool ObjectIsGround(GameObject obj)
    {
        return obj && obj.CompareTag("Ground");
    }

    public static SelectableObject FindNearestWorldObjectInListToPosition(List<SelectableObject> objects, Vector3 position)
    {
        if (objects == null || objects.Count == 0) return null;
        SelectableObject nearestObject = objects[0];
        float distanceToNearestObject = Vector3.Distance(position, nearestObject.transform.position);
        for (int i = 1; i < objects.Count; i++)
        {
            float distanceToObject = Vector3.Distance(position, objects[i].transform.position);
            if (distanceToObject < distanceToNearestObject)
            {
                distanceToNearestObject = distanceToObject;
                nearestObject = objects[i];
            }
        }
        return nearestObject;
    }

    public static List<SelectableObject> FilterObjectToAttackInListToPosition(List<SelectableObject> objects)
    {
        List<SelectableObject> units = new List<SelectableObject>();
        for (int i = 0; i < objects.Count; i++)
        {
            Unit unit = objects[i] as Unit;
            if (unit)
                units.Add(unit);
        }

        if (units.Count > 0)
            return units;
        return objects;
    }
}
