﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.UI;
using TMPro;

public class ResourceManager
{
    public static int ScrollWidth { get { return 15; } }
    public static float ScrollSpeed { get { return 25; } }
    public static float RotateAmount { get { return 10; } }
    public static float RotateSpeed { get { return 100; } }
    public static float MinCameraHeight { get { return 10; } }
    public static float MaxCameraHeight { get { return 40; } }

    private static Vector3 invalidPosition = new Vector3(-99999, -99999, -99999);
    private static Bounds invalidBounds = new Bounds(new Vector3(-99999, -99999, -99999), new Vector3(0, 0, 0));

    public static Vector3 InvalidPosition { get { return invalidPosition; } }
    public static Bounds InvalidBounds { get { return invalidBounds; } }

    public static bool MenuOpen { get; set; }
    public static string LevelName { get; set; }

    private static float buttonHeight = 40;
    private static float headerHeight = 32, headerWidth = 256;
    private static float textHeight = 25, padding = 10;
    public static float MenuWidth { get { return headerWidth + 2 * padding; } }
    public static float ButtonHeight { get { return buttonHeight; } }
    public static float ButtonWidth { get { return (MenuWidth - 3 * padding) / 2; } }
    public static float HeaderHeight { get { return headerHeight; } }
    public static float HeaderWidth { get { return headerWidth; } }
    public static float TextHeight { get { return textHeight; } }
    public static float Padding { get { return padding; } }

    private static GUISkin selectBoxSkin;
    public static GUISkin SelectBoxSkin { get { return selectBoxSkin; } }

    private static Texture2D healthyTexture, damagedTexture, criticalTexture;
    public static Texture2D HealthyTexture { get { return healthyTexture; } }
    public static Texture2D DamagedTexture { get { return damagedTexture; } }
    public static Texture2D CriticalTexture { get { return criticalTexture; } }

    private static Dictionary<ResourceType, Texture2D> resourceHealthBarTextures;

    public static void StoreSelectBoxItems(GUISkin skin, Texture2D healthy, Texture2D damaged, Texture2D critical)
    {
        selectBoxSkin = skin;
        healthyTexture = healthy;
        damagedTexture = damaged;
        criticalTexture = critical;
    }

    public static int BuildSpeed { get { return 1; } }

    private static GameObjectList gameObjectList;
    public static void SetGameObjectList(GameObjectList objectList)
    {
        gameObjectList = objectList;
    }

    public static GameObject GetBuilding(CustomActionName name)
    {
        return gameObjectList.GetBuilding(name);
    }

    public static GameObject GetUnit(CustomActionName name)
    {
        return gameObjectList.GetUnit(name);
    }

    public static UpgradeAction GetUpgrade(CustomActionName name)
    {
        return gameObjectList.GetUpgrade(name);
    }

    public static GameObject GetWorldObject(string name)
    {
        return gameObjectList.GetWorldObject(name);
    }

    public static GameObject GetPlayerObject()
    {
        return gameObjectList.GetPlayerObject();
    }

    public static Sprite GetBuildImage(string name)
    {
        return gameObjectList.GetBuildImage(name);
    }

    public static void SetResourceHealthBarTextures(Dictionary<ResourceType, Texture2D> images)
    {
        resourceHealthBarTextures = images;
    }

    public static Texture2D GetResourceHealthBar(ResourceType resourceType)
    {
        if (resourceHealthBarTextures != null && resourceHealthBarTextures.ContainsKey(resourceType)) return resourceHealthBarTextures[resourceType];
        return null;
    }

    public static int GetNewObjectId()
    {
        LevelLoaderCstm loader = (LevelLoaderCstm)UnityEngine.Object.FindObjectOfType(typeof(LevelLoaderCstm));
        if (loader) return loader.GetNewObjectId();
        return -1;
    }

    public static Texture2D[] GetAvatars()
    {
        return gameObjectList.GetAvatars();
    }

    public static Resource SearchSimilarResourceInArea(ResourceType type, Vector3 searchPoint, float areaRadius)
    {
        Collider[] hitColliders = Physics.OverlapSphere(searchPoint, areaRadius);
        int i = 0;
        List<Resource> resourcesInArea = new List<Resource>();
        while (i < hitColliders.Length)
        {
            Resource resource = hitColliders[i].GetComponentInParent<Resource>();
            if (resource != null)
            {
                if (type == resource.GetResourceType())
                {
                    resourcesInArea.Add(resource);
                }
            }
            i++;
        }

        Resource closestResource = null;
        if(resourcesInArea.Count > 0)
        {
            resourcesInArea = resourcesInArea.OrderBy(
            x => Vector3.Distance(searchPoint, x.transform.position)
            ).ToList();

            closestResource = resourcesInArea[0];

            foreach (Resource r in resourcesInArea)
            {
                if (!r.IsUnitOnAllQueue())
                {
                    closestResource = r;
                }
            }
        }
        return closestResource;
    }

    internal static Color ColorSlider(float v)
    {
        Color color = Color.white;
        if (v > 0.6)
        {
            color = DataManager.COLOR_HIGH_HEALTH;
        } else if (v > 0.3)
        {
            color = DataManager.COLOR_MIDDLE_HEALTH;
        } else
        {
            color = DataManager.COLOR_LOW_HEALTH;
        }
        return color;
    }

    private IEnumerator HighlightCoroutine(Image panel, TMP_Text text)
    {

        Color warningColor = new Color32(181, 65, 44, 255);
        Color baseColor = new Color32(255, 255, 255, 0);

        for (int i = 0; i < 1; i++)
        {
            panel.color = warningColor;
            yield return new WaitForSeconds(0.3f);
            panel.color = baseColor;
            yield return new WaitForSeconds(0.3f);
        }

        panel.color = baseColor;
    }
    public static IEnumerator HighlightClickedCoroutine(GameObject selectedMarks)
    {
        selectedMarks.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        selectedMarks.SetActive(false);
    }

    public static float GetAngleToFaceObject(Vector3 objectToFacePosition, Vector3 currentObject)
    {
        if (objectToFacePosition == null || objectToFacePosition == invalidPosition)
        {
            Debug.Log("Resource position is null");
            return 0;
        }
        float angle;
        if (objectToFacePosition - currentObject == Vector3.zero)
            angle = Quaternion.LookRotation(objectToFacePosition - currentObject).eulerAngles.y;
        else
            angle = Quaternion.LookRotation(currentObject).eulerAngles.y;
        return angle;
    }

    public static float GetAngleToFaceObject(Vector3 objectToFacePosition, Transform currentObject)
    {
        if (objectToFacePosition == null || objectToFacePosition == invalidPosition)
        {
            Debug.Log("Resource position is null");
            return 0;
        }
        float angle;
        // Debug.Log(objectToFacePosition - currentObject.position);
        if (objectToFacePosition - currentObject.position == Vector3.zero)
            angle = currentObject.eulerAngles.y;
        else
            angle = Quaternion.LookRotation(objectToFacePosition - currentObject.position).eulerAngles.y;
        return angle;
    }
}
