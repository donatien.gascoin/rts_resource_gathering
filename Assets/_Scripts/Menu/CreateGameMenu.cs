﻿using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class CreateGameMenu : MonoBehaviour
{

    public TMP_Dropdown playersDropDown;
    public List<PlayerDetails> playersDetails;

    private void Start()
    {
        /* DropdownValueChanged(playersDropDown);
        playersDropDown.onValueChanged.AddListener(delegate {
             DropdownValueChanged(playersDropDown);
         });*/
    }
    public void PlayGame()
    {
        int nbPlayers = 0;
        for (int i = 0; i < playersDetails.Count; i++)
            if (playersDetails[i].gameObject.activeSelf)
                nbPlayers++;

        string[] gamePlayers = new string[nbPlayers];
        for (int i = 0; i < playersDetails.Count; i++)
        {
            if (playersDetails[i].gameObject.activeSelf)
            {
                gamePlayers[i] = JsonUtility.ToJson(new PlayerDetailsJSON(playersDetails[i], (i == 0)));
                Debug.Log("Add player with parameters [ name : " + playersDetails[i].playerName.text + ", color : " + playersDetails[i].playerColor.text + ", team : " + playersDetails[i].playerTeam.text + ", human : " + (i == 0) + "]");
            }
        }
        PlayerPrefsX.SetStringArray("GameStartupDetails", gamePlayers);
        /*Debug.Log(JsonUtility.ToJson(gamePlayers));
        Debug.Log(JsonHelper.ToJson(gamePlayers));*/
        /*PlayerPrefs.SetString("GameStartupDetails", JsonHelper.ToJson(gamePlayers.ToArray()));*/
        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }

    private void OnDestroy()
    {
        playersDropDown.onValueChanged.RemoveAllListeners();
    }

}