﻿using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerDetails : MonoBehaviour
{
    public TMP_Text playerName;
    public TMP_Text playerColor;
    public TMP_Text playerTeam;

    public TMP_Dropdown colorsDropDown;

    public TMP_Dropdown teamDropDown;

    [HideInInspector]
    public DropDownColor currentColor;
    public List<DropDownColor> colors;

    private void Awake()
    {
        colors = new List<DropDownColor>();

        colorsDropDown.onValueChanged.AddListener(delegate {
            DropdownValueChanged(colorsDropDown);
        });
    }

    public void ChangeDropDownColors(List<DropDownColor> colors)
    {
        if (colors == null || colors.Count == 0)
            return;
        colorsDropDown.ClearOptions();
        List<TMP_Dropdown.OptionData> options = new List<TMP_Dropdown.OptionData>();
        for (int i = 0; i < colors.Count; i++)
        {
            /*Debug.Log(i);
            Debug.Log(colors[i]);*/
            if (colors[i] != null)
                options.Add(new TMP_Dropdown.OptionData(colors[i].colorName.ToString(), colors[i].color));
        }
        colorsDropDown.AddOptions(options);
        
        if (currentColor != null)
        {
            for (int i = 0; i < colorsDropDown.options.Count; i++)
            {
                if (currentColor.colorName.ToString() == colorsDropDown.options[i].text)
                {
                    colorsDropDown.value = i;
                    break;
                }
            }
        }
        this.colors = colors;
    }

    public void SetPlayerTeam(int team)
    {
        playerTeam.text = team.ToString();
    }

    public void SelectColor(DropDownColor colorToUse)
    {
        currentColor = colorToUse;

        for (int i = 0; i < colorsDropDown.options.Count; i++)
        {
            if (colorToUse.colorName.ToString() == colorsDropDown.options[i].text)
            {
                colorsDropDown.value = i;
                break;
            }
        }
    }

    void DropdownValueChanged(TMP_Dropdown change)
    {
        TMP_Dropdown.OptionData data = colorsDropDown.options[change.value];
        for (int i = 0; i < colors.Count; i++)
        {
            if (colors[i] != null && data.text == colors[i].colorName.ToString())
            {
                DropDownColor oldColor = currentColor;
                currentColor = colors[i];
                MenuPlayers.chooseColor(oldColor, colors[i]);
                break;
            }
        }
    }

}

[System.Serializable]
public class PlayerDetailsJSON
{
    public string playerName;
    public string playerColor;
    public int playerTeam;
    public bool human;

    public PlayerDetailsJSON(PlayerDetails pd, bool isHuman)
    {
        playerName = pd.playerName.text;
        playerColor = pd.playerColor.text;
        playerTeam = Int32.Parse(pd.playerTeam.text);
        human = isHuman;
    }

    public PlayerDetailsJSON()
    {
    }
}