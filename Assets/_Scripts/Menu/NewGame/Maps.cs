﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Text;
using System;

public class Maps : MonoBehaviour
{
    public TMP_Dropdown mapsDropDown;
    
    private static string parenthesis_open = "(";
    private static string parenthesis_close = ") ";

    private void Awake()
    {
        mapsDropDown.onValueChanged.AddListener(delegate {
            DropdownValueChanged(mapsDropDown);
        });
    }

    public void InitMaps(Map[] maps)
    {
        mapsDropDown.ClearOptions();
        List<TMP_Dropdown.OptionData> datas = new List<TMP_Dropdown.OptionData>();
        
        for (int i = 0; i < maps.Length; i++)
        {
            TMP_Dropdown.OptionData data = new TMP_Dropdown.OptionData();
            data.text = parenthesis_open + maps[i].nbPlayers + parenthesis_close + maps[i].mapName;
            datas.Add(data);
        }
        mapsDropDown.AddOptions(datas);
        if (maps.Length > 0)
            NewGame.selectMap(0);
    }
    void DropdownValueChanged(TMP_Dropdown change)
    {
        NewGame.selectMap(change.value);
    }

}
