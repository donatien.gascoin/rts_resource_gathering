﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MapDetails : MonoBehaviour
{
    public TMP_Text mapName;
    public TMP_Text mapNbPlayers;
    public TMP_Text mapSize;
    public TMP_Text mapDescription;
    public Image mapImage;

    public void UpdateUI(Map map)
    {
        mapName.text = map.mapName;
        mapNbPlayers.text = map.nbPlayers.ToString();
        mapSize.text = map.size.ToString();
        mapDescription.text = map.mapDescription;
        mapImage.sprite = map.image;
    }
}
