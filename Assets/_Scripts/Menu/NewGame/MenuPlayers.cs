﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPlayers : MonoBehaviour
{
    public Map map;
    public List<PlayerDetails> playerDetails;
    public List<DropDownColor> colors;
    public List<DropDownColor> usedColors;

    public delegate void ChooseColor(DropDownColor oldColor, DropDownColor newColor);
    public static ChooseColor chooseColor;

    public delegate List<DropDownColor> GetTakenColors();
    public static GetTakenColors getTakenColors;

    private void Awake()
    {
        chooseColor += ChangePlayerColor;
        getTakenColors += GetUsedColors;
    }

    private void OnDestroy()
    {
        chooseColor -= ChangePlayerColor;
        getTakenColors -= GetUsedColors;
    }

    private void Start()
    {
        playerDetails[0].SetPlayerTeam(1);
        playerDetails[1].SetPlayerTeam(2);
    }

    public void SetMap(Map map)
    {
        this.map = map;
        for (int i = 0; i < playerDetails.Count; i++)
        {
            playerDetails[i].SelectColor(colors[i]);
            usedColors.Add(colors[i]);
        }
        this.UpdateUI();

    }

    public void UpdateUI()
    {
        int nb = map.nbPlayers;
        usedColors = new List<DropDownColor>();
        for (int i =0; i < playerDetails.Count; i++) 
        {
            if (i < nb)
            {
                usedColors.Add(playerDetails[i].currentColor);
                playerDetails[i].gameObject.SetActive(true);
            } else
            {
                playerDetails[i].gameObject.SetActive(false);
            }
        }

        for (int i = 0; i < playerDetails.Count; i++)
        {
            playerDetails[i].ChangeDropDownColors(GetAvailableColors(playerDetails[i]));
        }
    }

    public void ChangePlayerColor(DropDownColor oldColor, DropDownColor newColor)
    {
        usedColors.Remove(oldColor);
        usedColors.Add(newColor);

        // Update all dropdown to add the old color, and remove the new selected
        for (int i = 0; i < playerDetails.Count; i++)
        {
            playerDetails[i].ChangeDropDownColors(GetAvailableColors(playerDetails[i]));
        }
    }

    public List<PlayerDetailsJSON> GetPlayers()
    {
        List<PlayerDetailsJSON> details = new List<PlayerDetailsJSON>();
        for (int i = 0; i < playerDetails.Count; i++)
        {
            if (playerDetails[i].gameObject.activeSelf)
            {
                details.Add(new PlayerDetailsJSON(playerDetails[i], i == 0));
            }
        }
        return details;
    }

    public List<DropDownColor> GetUsedColors()
    {
        return usedColors;
    }

    public List<DropDownColor> GetAvailableColors(PlayerDetails playerD)
    {
        List<DropDownColor> items = new List<DropDownColor>();
        items.Add(playerD.currentColor);
        for (int j = 0; j < colors.Count; j++)
        {
            DropDownColor c = colors[j];
            bool addToList = true;

            for(int i = 0; i < playerDetails.Count; i++)
            {
                if (i < usedColors.Count && usedColors[i] != null && usedColors[i].colorName == colors[j].colorName)
                {
                    addToList = false;
                    break;
                }
            }
            if (addToList)
                items.Add(colors[j]);
        }
        return items;
    }
}
