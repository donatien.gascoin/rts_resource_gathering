﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Custom map", menuName = "Maps/Custom map")]
public class Map : ScriptableObject
{
    public string mapName;
    public int nbPlayers;
    public MapSize size;
    [TextArea()]
    public string mapDescription;
    public Sprite image;
    public string sceneName;

}

[System.Serializable]
public enum MapSize
{
    SMALL, MEDIUM, LARGE
}