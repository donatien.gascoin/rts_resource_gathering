﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewGame : MonoBehaviour
{
    public MenuPlayers menuPlayer;
    public Maps mapsManager;
    public MapDetails mapDetails;

    public GameObject loadingPanel;
    public float loadTime = 1f;

    private Map selectedMap;

    public delegate void ChangeMap(int index);
    public static ChangeMap selectMap;

    private const string player = "Player ";

    public Animator playGameAnimator;

    public Map[] maps;

    private void Awake()
    {
        loadingPanel.SetActive(false);
        selectMap += ChangeSelectedMap;
        Array.Sort(maps,
            delegate (Map x, Map y) { return x.nbPlayers.CompareTo(y.nbPlayers); });
    }

    private void OnDestroy()
    {
        selectMap -= ChangeSelectedMap;
    }

    private void Start()
    {
        mapsManager.InitMaps(maps);
    }

    public void ChangeSelectedMap(int index)
    {
        selectedMap = maps[index];
        mapDetails.UpdateUI(maps[index]);
        menuPlayer.SetMap(maps[index]);
    }

    public void StartGame()
    {
        /*if (startButtonClick != null) *//*startButtonClick.Play();*/
        loadingPanel.SetActive(true);
        StartCoroutine(SendPlayersData());
    }

    IEnumerator SendPlayersData()
    {
        yield return new WaitForSeconds(loadTime);
        List<PlayerDetailsJSON> playersDetails = menuPlayer.GetPlayers();

        string[] gamePlayers = new string[playersDetails.Count];
        for (int i = 0; i < playersDetails.Count; i++)
        {

            if (playersDetails[i].playerName.Equals(""))
                playersDetails[i].playerName = player + i;

            gamePlayers[i] = JsonUtility.ToJson(playersDetails[i]);
            Debug.Log("Add player with parameters [ name : " + playersDetails[i].playerName + ", color : " + playersDetails[i].playerColor + ", team : " + playersDetails[i].playerTeam + ", human : " + (i == 0) + "]");
        }
        PlayerPrefsX.SetStringArray("GameStartupDetails", gamePlayers);

        SceneManager.LoadScene(selectedMap.sceneName, LoadSceneMode.Single);
    }

    public void SetMenu(bool isOpen)
    {
        playGameAnimator.SetBool("Open", isOpen);
    }

}
