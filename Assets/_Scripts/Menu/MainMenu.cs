﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public Animator mainMenuAnimator;

    private void Start()
    {
        AudioManager.playSound(SoundCategorie.THEME);
    }

    public void QuitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

    public void OpenMenu(bool open)
    {
        mainMenuAnimator.SetBool("Open", open);
    }
}
