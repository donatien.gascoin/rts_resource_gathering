﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * Responsibility : Checking the position of the goldMine/Trees
 */
public class EnvironmentController : MonoBehaviour
{
    public Player player;
    public float timeBetweenScan = 5f;
    private float lastScanTime = 0f;
    private void Awake()
    {
        checkPosition += CheckPosition;
        lastScanTime = 0f;
    }

    private void OnDestroy()
    {
        checkPosition -= CheckPosition;
    }



    public delegate void CheckObjectPosition(SelectableObject obj);
    public static CheckObjectPosition checkPosition;

    private void FixedUpdate()
    {
        lastScanTime += Time.deltaTime;
        if (lastScanTime >= timeBetweenScan)
        {
            for (int i = 0; i < player.units.Count; i++)
            {
                CheckPosition(player.units[i]);
            }
            lastScanTime = 0f;
        }
    }

    public void CheckPosition (SelectableObject obj)
    {
        List<Resource> resources = WorkManager.FindNearbyResource(obj.transform.position, obj.GetStats().detectionRange);

        for (int i = 0; i < resources.Count; i++)
        {
            if (resources[i] != null)
                WorkerController.addResource(resources[i]);

        }
    }
}
