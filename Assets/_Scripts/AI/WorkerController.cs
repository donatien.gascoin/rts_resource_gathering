﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WorkerController : MonoBehaviour
{
    public Player player;
    public List<SelectableObject> workers;
    // Nb workers needed for the player for harvesting gold/wood
    public int nbGoldWorkersNeeded;
    public int nbGoldWorkers;
    public int nbWoodWorkersNeeded;
    public int nbWoodWorkers;

    public bool isWorkerInQueue = false;
    private Building buildingCreatingWorker;

    private List<Resource> foundGoldResource;
    private List<Resource> foundWoodResource;

    public float timeBetweenScan = 5f;
    private float lastScanTime = 0f;

    private bool checkResource = false;

    private void Awake()
    {
        addResource += AddResource;
        foundWoodResource = new List<Resource>();
        foundGoldResource = new List<Resource>();
    }

    private void OnDestroy()
    {
        addResource -= AddResource;
    }

    void Start()
    {
        workers = player.GetWorkers();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        lastScanTime += Time.deltaTime;
        if (lastScanTime >= timeBetweenScan)
        {
            ScanWorkers();
            CheckWorkerNumber();
            CheckWorkerAssignement();
            lastScanTime = 0f;

            if (checkResource)
            {
                foundGoldResource = RemoveDestroyedResource(foundGoldResource);
                foundWoodResource = RemoveDestroyedResource(foundWoodResource);
                checkResource = false;
            }
        }
    }

    private void ScanWorkers()
    {
        List<SelectableObject> newWorkers = new List<SelectableObject>();
        int goldWorkers = 0;
        int woodWorkers = 0;
        for (int i = 0; i < player.GetWorkers().Count; i++)
        {
            if (player.GetWorkers()[i] == null)
                continue;

            Worker w = player.GetWorkers()[i] as Worker;
            if (w.IsWorking())
            {
                if (ResourceType.GOLD == w.harvestType)
                {
                    goldWorkers++;
                }
                else if (ResourceType.WOOD == w.harvestType)
                {
                    woodWorkers++;
                }
            }
            newWorkers.Add(w);
        }
        workers = newWorkers;
        nbGoldWorkers = goldWorkers;
        nbWoodWorkers = woodWorkers;
    }

    private void CheckWorkerNumber()
    {
        if (workers.Count < (nbGoldWorkersNeeded + nbWoodWorkersNeeded))
        {
            // Need to creater a worker
            if (!isWorkerInQueue)
            {
                for (int i = 0; i < player.buildings.Count; i++)
                {
                    if (player.buildings[i].objectName.Equals("Town Hall"))
                    {
                        buildingCreatingWorker = player.buildings[i];
                        buildingCreatingWorker.PerformLeftClickAction(CustomActionName.WORKER);
                        isWorkerInQueue = true;
                        break;
                    }
                }
            }
        }

        if (isWorkerInQueue)
        {
            bool isWorker = buildingCreatingWorker.buildQueue.Count > 0;

            if (!isWorker)
            {
                // Worker has been created
                // workers = player.GetWorkers();
                isWorkerInQueue = false;
                buildingCreatingWorker = null;
            }
        }
    }

    private void CheckWorkerAssignement()
    {
        // At least one worker is doing nothing
        if (player.GetNonWorkingWorkers().Count > 0) 
        {
            for (int i = 0; i < player.GetNonWorkingWorkers().Count; i++)
            {
                if (player.GetNonWorkingWorkers()[i] == null)
                    break;
                Worker worker = player.GetNonWorkingWorkers()[i] as Worker;
                if (!worker.IsWorking())
                {
                    if (player.GetResourceAmount(ResourceType.GOLD) < player.GetResourceAmount(ResourceType.WOOD))
                    {
                        // Less gold than wood
                        AssignTaskToWorker(worker, ResourceType.GOLD);
                    } else
                    {
                        // Less wood than gold
                        AssignTaskToWorker(worker, ResourceType.WOOD);
                    }
                }
            }
        }
    }

    private void AssignTaskToWorker(Worker worker, ResourceType missingResource)
    {
        switch (missingResource)
        {
            case ResourceType.GOLD:
                if (nbGoldWorkers < nbGoldWorkersNeeded)
                {
                    worker.ClickOnResource(SearchResourceInArea(worker, ResourceType.GOLD));
                } else
                {
                    worker.ClickOnResource(SearchResourceInArea(worker, ResourceType.WOOD));
                }
                break;
            case ResourceType.WOOD:
                if (nbWoodWorkers < nbWoodWorkersNeeded)
                {
                    worker.ClickOnResource(SearchResourceInArea(worker, ResourceType.WOOD));
                }
                else if (nbGoldWorkers < nbGoldWorkersNeeded)
                {
                    worker.ClickOnResource(SearchResourceInArea(worker, ResourceType.GOLD));
                } else
                {
                    // Default case, both gold and wood are full, add another worker to wood
                    worker.ClickOnResource(SearchResourceInArea(worker, ResourceType.WOOD));
                }
                break;
        }
    }

    private Resource SearchResourceInArea(Worker worker, ResourceType resourceToSearch)
    {
        switch(resourceToSearch)
        {
            case ResourceType.GOLD:
                if (foundGoldResource.Count == 0)
                    return null;
                List<Resource> g = foundGoldResource.OrderBy(x => (worker.transform.position - (x != null ? x.transform.position : worker.transform.position)).sqrMagnitude).ToList();
                nbGoldWorkers++;
                return GetFirstExistingResource(g);
            case ResourceType.WOOD:
                if (foundWoodResource.Count == 0)
                    return null;
                List<Resource> w = foundWoodResource.OrderBy(x => (worker.transform.position - (x != null ? x.transform.position : worker.transform.position)).sqrMagnitude).ToList();
                nbWoodWorkers++;
                return GetFirstExistingResource(w);
            default:
                return null;
        }
    }

    private Resource GetFirstExistingResource(List<Resource> resources)
    {
        for (int i = 0; i < resources.Count; i++)
        {
            if (resources[i] != null)
                return resources[i];
            else
                checkResource = true;
        }
        return null;

    }

    private List<Resource> RemoveDestroyedResource(List<Resource> resources)
    {
        List<Resource> newResource = new List<Resource>();
        for (int i = 0; i < resources.Count; i++)
        {
            if (resources[i] != null)
                newResource.Add(resources[i]);
        }
        return newResource;
    }

    public void SetPlayer(Player player)
    {
        this.player = player;
    }

    public void AddGoldResource(Resource resource)
    {
        if (foundGoldResource == null)
            foundGoldResource = new List<Resource>();

        if (!foundGoldResource.Contains(resource))
            foundGoldResource.Add(resource);
    }

    public void AddWoodResource(Resource resource)
    {
        if (foundWoodResource == null)
            foundWoodResource = new List<Resource>();

        if (!foundWoodResource.Contains(resource))
            foundWoodResource.Add(resource);
    }


    public delegate void AddResourceToController(Resource resource);
    public static AddResourceToController addResource;
    public void AddResource(Resource resource)
    {
        GoldDeposit goldR = resource as GoldDeposit;
        if (goldR != null)
            AddGoldResource(goldR);
        else
            AddWoodResource(resource);
    }
}
