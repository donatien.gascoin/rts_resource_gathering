﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : Projectile
{
    public GameObject rockExplosionFx;
    public float explosionTime;
    public float explosionEffectArea;
    protected override void Update()
    {
        /*if (target == null)
            range = 0;
        else*/
        range = Vector3.Distance(transform.position, positionToAttack);
        if (range <= 0)
        {
            // The catapult will inflict damage in an area, it does not matter if the targer still exist
            TargetReached();
        }
    }

    public override void TargetReached()
    {
        GameObject explosion = Instantiate(rockExplosionFx, transform.position, Quaternion.FromToRotation(Vector3.up, transform.position));
        InflictDamage();
        Destroy(gameObject);
        Destroy(explosion, explosionTime);
    }

    public override void InflictDamage()
    {
        // Will attack every objects in the range of the explosion
        List<WorldObject> objs = WorkManager.FindNearbyWorldObjects(transform.position, explosionEffectArea);
        for (int i =0; i < objs.Count; i++)
            if (objs[i]) objs[i].TakeDamage(damage, attacker);

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionEffectArea);
    }
}