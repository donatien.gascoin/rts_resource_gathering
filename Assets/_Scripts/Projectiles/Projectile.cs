﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    public float velocity = 1;
    public float damage = 1;

    protected float range = 1;
    protected SelectableObject target;
    protected SelectableObject attacker;
    public Vector3 positionToAttack;

    private void Awake()
    {
        AudioSource audioSource = GetComponentInChildren<AudioSource>();
        if (audioSource != null)
            audioSource.outputAudioMixerGroup = AudioManager.getAudioMixer();
    }

    protected virtual void Update()
    {
        if (range >= 0)
        {
            float positionChange = Time.deltaTime * velocity;
            range -= positionChange;
            transform.position += (positionChange * transform.forward);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag(DataManager.GROUND_TAG) && target != null)
        {
            WorldObject wo = other.GetComponentInParent<WorldObject>();
            if (wo != null && target != null)
            {
                if (wo.ObjectId == target.ObjectId)
                {
                    TargetReached();
                }
            }

        }
    }

    public virtual void TargetReached()
    {
        InflictDamage();
        Destroy(gameObject);
    }

    public void SetProjectileData(int velocity, float damage)
    {
        this.velocity = velocity;
        this.damage = damage;
    }

    public void SetRange(float range)
    {
        this.range = range;
    }

    public void SetSetOpponents(SelectableObject target, SelectableObject attacker)
    {
        if (target != null)
            SetSetOpponents(target, attacker, target.transform.position);
    }

    public void SetSetOpponents(SelectableObject target, SelectableObject attacker, Vector3 positionToAttack)
    {
        this.target = target;
        this.attacker = attacker;
        this.positionToAttack = positionToAttack;
    }

    public virtual void InflictDamage()
    {
        if (target) target.TakeDamage(damage, attacker);
    }
}