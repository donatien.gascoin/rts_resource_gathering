﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bolt : Projectile
{

    public GameObject boltExplosion;

    public override void TargetReached()
    {
        GameObject bolt = Instantiate(boltExplosion, transform.position, Quaternion.FromToRotation(Vector3.up, transform.position));

        AudioSource audioSource = boltExplosion.GetComponent<AudioSource>();
        if (audioSource != null)
            audioSource.outputAudioMixerGroup = AudioManager.getAudioMixer();

        InflictDamage();
        Destroy(gameObject);
        Destroy(bolt, .5f);
    }
}
