﻿using System;
using System.Collections.Generic;

public class WorkerQueue
{

    private ListQueue<Unit> unitQueue;

    public WorkerQueue()
    {
        unitQueue = new ListQueue<Unit>();
    }

    public void AddUnitToQueue(Unit u)
    {
        unitQueue.Enqueue(u);
    }

    public void DequeueUnit()
    {
        unitQueue.Dequeue();
    }

    public Unit GetFirstUnitInQueue()
    {
        return unitQueue.Peek();
    }

    public bool IsUnitFirstInQueue(Unit u)
    {
        if (unitQueue.Peek() == null)
            return false;
        return unitQueue.Peek().ObjectId == u.ObjectId;
    }
    /**
     * Return true if the queue is not empty
     */
    public bool IsUnitInQueue()
    {
        return unitQueue.Count > 0;
    }

    /**
     * Return true if the specific unit is on this queue
     */
    public bool IsUnitInQueue(Unit unit)
    {
        return unitQueue.Contains(unit);
    }

    public void RemoveUnitFromQueue(Unit unit)
    {
        unitQueue.RemoveFromQueue(unit);
    }

    public void EmptyQueue()
    {
        unitQueue.Clear();
    }

    public int GetQueueLength()
    {
        return unitQueue.Count;
    }
}

/**
 * Fake queue, allowing to remove a specific element, and not only the first
 */
 [System.Serializable]
public class ListQueue<T> : List<T>
{

    public void Enqueue(T item)
    {
        base.Add(item);
    }

    public T Dequeue()
    {
        var t = base[0];
        base.RemoveAt(0);
        return t;
    }

    public T Peek()
    {
        if (base.Count == 0)
            return default(T);
        return base[0];
    }

    /**
     * Remove an item from the queue, whatever it position
     * Example: An harvester was waiting to empty is package, and the user move him. We need to remove him from the queue, to prevent the queue to be blocked
     */
    public void RemoveFromQueue(T item)
    {
        base.Remove(item);
    }

    /*
     * Method below are overwritten to prevent the utilisation of a list of our fake queue.
     */
    new public void Add(T item) { throw new NotSupportedException(); }
    new public void AddRange(IEnumerable<T> collection) { throw new NotSupportedException(); }
    new public void Insert(int index, T item) { throw new NotSupportedException(); }
    new public void InsertRange(int index, IEnumerable<T> collection) { throw new NotSupportedException(); }
    new public void Reverse() { throw new NotSupportedException(); }
    new public void Reverse(int index, int count) { throw new NotSupportedException(); }
    new public void Sort() { throw new NotSupportedException(); }
    new public void Sort(Comparison<T> comparison) { throw new NotSupportedException(); }
    new public void Sort(IComparer<T> comparer) { throw new NotSupportedException(); }
    new public void Sort(int index, int count, IComparer<T> comparer) { throw new NotSupportedException(); }
}