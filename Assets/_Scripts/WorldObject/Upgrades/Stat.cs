﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Object stat", menuName = "Selectable object/Stats")]
public class Stat : ScriptableObject
{

    public int buildTime;

    public List<Cost> costs;

    public bool invulnerable;
    public UnitType unitType;
    public AttackType attackType;

    public UpgradeType upgradeType;

    public List<Stats> lvlStats;
}
