﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectStat : MonoBehaviour
{
    public Stat stat;

    [Space]
    public int currentLvl;
    private Stats current;

    public void Init()
    {
        // Debug.Log("Max Lvl : " + lvlStats.Count);
        if (stat.lvlStats.Count > 0)
        {
            current = (stat.lvlStats[0]);
            currentLvl = 0;
        }
        else
            Debug.Log("Object does not have lvl stats.");
    }

    public void InitObjectStats(int lvl)
    {
        if (lvl < stat.lvlStats.Count)
            current = stat.lvlStats[lvl];
        else
            Debug.Log("Lvl " + lvl + " does not exist");
    }

    public int GetCurrentLvl()
    {
        return currentLvl;
    }

    public void UpgradeObjectStats()
    {
        if (currentLvl >= stat.lvlStats.Count) return;
        if (currentLvl == (stat.lvlStats.Count + 1)) return;
        current = stat.lvlStats[currentLvl + 1];
        currentLvl++;
    }

    public Stats GetStats()
    {
        return current;
    }

    public float CalculeNewHitPointValue(float currentHitPoint)
    {
        if (currentLvl == 0)
            return currentHitPoint;

        int maxHitPoint = current.maxHitPoint;
        int previousMaxHitPoint = stat.lvlStats[currentLvl - 1].maxHitPoint;
        return maxHitPoint * currentHitPoint / previousMaxHitPoint;
    }

    public List<Cost> GetCosts()
    {
        return stat.costs;
    }

    public int GetBuildTime()
    {
        return stat.buildTime;
    }

    public bool IsInvulnerable()
    {
        return stat.invulnerable;
    }

    public UnitType GetUnitType()
    {
        return stat.unitType;
    }

    public AttackType GetAttackType()
    {
        return stat.attackType;
    }

    public UpgradeType GetUpgradeType()
    {
        return stat.upgradeType;
    }
}
