﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Upgrade Action", menuName = "Selectable object/Upgrade/Action")]
public class UpgradeAction: ScriptableObject
{
    public CustomActionObject action;
    public float upgradeTime;
    // Allow up to 10 Upgrades
    [Range(0, 9)]
    public int level;

    public List<Cost> upgradeCost;
    public List<CustomActionName> requirements;
}