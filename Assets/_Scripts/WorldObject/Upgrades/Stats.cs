﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stats
{
    public int maxHitPoint;

    public float minAttack;
    public float maxAttack;
    public float defense;

    public float detectionRange;
    public float weaponRange;
    public float weaponRechargeTime;

    public float speed;

    /** Experience given to the unit who kill this one */
    public int exp;

    [Header("Health regeneration")]
    public bool healthRegeneration;
    public float healthRegenerationValue = 2;
    public float healthRegenerationRate = 1;
    public float healthRegenerationDelay = 5;

    [Header("Mana")]
    public bool useMana;
    public int maxManaPoint;

    [Header("Mana regeneration")]
    public bool manaRegeneration;
    public float manaRegenerationValue = .3f;
    public float manaRegenerationRate = 1;
    public float manaRegenerationDelay = 3;

    public float GetAttackValue()
    {
        return RandomManager.RandomValue(minAttack, maxAttack);
    }

}