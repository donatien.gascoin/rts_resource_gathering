﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum UpgradeType
{
    MELEE_UPGRADES, RANGED_UPGRADES, MAGICIAN_UPGRADES, CLERIC_UPGRADES, WORKER_UPGRADES, BUILDING_UPGRADES, NONE
}
