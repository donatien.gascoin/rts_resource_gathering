﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Upgrades: MonoBehaviour
{
    public UpgradeType upgradeType;
    /*public CustomAction associatedAction;*/
    public int currentLvl;

    public List<UpgradeAction> levels;

    public UpgradeAction Upgrade()
    {
        switch (currentLvl)
        {
            case 0:
                currentLvl = 1;
                break;
            case 1:
                currentLvl = 2;
                break;
            case 2:
                currentLvl = 3;
                break;
            default:
                Debug.Log("No upgrade for this level");
                break;
        }
        return GetCurrentUpgradeAction();
    }

    public UpgradeAction GetCurrentUpgradeAction()
    {
        if (currentLvl >= levels.Count)
        {
            return null;
        }
        for (int i = 0; i < levels.Count; i++)
        {
            if (levels[i].level == (currentLvl))
                return levels[i];
        }
        Debug.Log("The upgrade action " + currentLvl + " is not defined !");
        return null;
    }

    public void Init()
    {
        currentLvl = 0;
    }
}
