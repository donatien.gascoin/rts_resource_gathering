﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradesStatus : MonoBehaviour
{
    Upgrades[] upgrades;
    public Upgrades meleeUpgrades;
    public Upgrades rangedUpgrades;
    public Upgrades magicianUpgrades;
    public Upgrades clericUpgrades;
    public Upgrades workerUpgrades;
    public Upgrades buildingUpgrades;

    private void Awake()
    {
        Init();
    }

    public Upgrades GetUpgradeStatus(UpgradeType upgradeName)
    {
        for (int i = 0; i < upgrades.Length; i++)
            if (upgrades[i].upgradeType == upgradeName)
                return upgrades[i];
        Debug.Log("Upgrade name (" + upgradeName + ") not defined !");
        return null;
    }

    public UpgradeAction GetUpgradeAction(UpgradeType upgradeName)
    {
        Upgrades u = GetUpgradeStatus(upgradeName);
        if (u != null)
            return u.GetCurrentUpgradeAction();

        Debug.Log("Upgrade name (" + upgradeName + ") not defined !");
        return null;
    }

    public int GetUpgradeLevel(UpgradeType upgradeName)
    {
        UpgradeAction ua = GetUpgradeAction(upgradeName);
        if (ua != null)
            return ua.level;

        Debug.Log("Upgrade name (" + upgradeName + ") not defined !");
        return -1;
    }

    public void Init()
    {
        upgrades = GetComponentsInChildren<Upgrades>();
        for (int i = 0; i < upgrades.Length; i++)
                upgrades[i].Init();
    }

    public UpgradeAction GetUpgradeActionFromActionName(CustomActionName actionName)
    {
        for (int i = 0; i < upgrades.Length; i++)
        {
            UpgradeAction ac = FindActionInUpgrade(upgrades[i], actionName);
            if (ac != null)
                return ac;
        }
        return null;
    }

    private UpgradeAction FindActionInUpgrade(Upgrades upgrades, CustomActionName actionName)
    {
        if (upgrades.GetCurrentUpgradeAction() != null && upgrades.GetCurrentUpgradeAction().action.GetActionNameData().Equals(actionName))
            return upgrades.GetCurrentUpgradeAction();
        /*for (int i = 0; i < upgrades.levels.Count; i++)
            if (upgrades.levels[i].action.GetActionNameData().Equals(actionName))
                return upgrades.levels[i];*/

        return null;
    }
}
