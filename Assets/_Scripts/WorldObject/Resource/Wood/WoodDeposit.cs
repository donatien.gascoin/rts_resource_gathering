﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodDeposit : Resource
{
    // Start is called before the first frame update

    public GameObject stump;

    protected override void Start()
    {
        base.Start();
        this.resourceType = ResourceType.WOOD;
        this.amountLeft = capacity;
    }

    protected override void ObjectDeath()
    {
        Instantiate(stump, transform.position, transform.rotation, GetComponentInParent<Trees>().transform);
        base.ObjectDeath();
    }

    public override bool CanBeSelected()
    {
        return false;
    }

    public override bool CanBeAttackedBy(SelectableObject attacker)
    {
        return AttackType.SIEGE.Equals(attacker.GetAttackType());
    }
}
