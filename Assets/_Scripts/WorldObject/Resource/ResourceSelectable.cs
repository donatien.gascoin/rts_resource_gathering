﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ResourceSelectable : Resource
{
    /*public ResourceType resourceType;
    public List<WorkerSlot> workerSlots;
    public int capacity;
    public int amountLeft;*/

    protected override void Start()
    {
        this.resourceType = ResourceType.UNKNOWN;
        if (workerSlots == null)
        {
            Destroy(this.gameObject);
        }
    }

    /*public bool IsEmpty()
    {
        return amountLeft <= 0;
    }

    public ResourceType GetResourceType()
    {
        return resourceType;
    }
    public void Remove(int amount)
    {
        amountLeft -= amount;
        if (amountLeft < 0) amountLeft = 0;
    }

    protected void DestroyResource()
    {
        foreach (WorkerSlot wl in workerSlots)
        {
            wl.queue.EmptyQueue();
        }
        // Add destroy animation
        Destroy(this.gameObject);
    }

    public int GetResourceLeft()
    {
        return amountLeft;
    }

    public WorkerSlot AddUnitToWorkerQueue(Unit unit)
    {
        WorkerSlot selectedSlot = null;
        int indexEmptyiestSlot = -1, numberOfUnitAlreadyIn = 999;

        // 1- Sort slots in distance from unit
        List<WorkerSlot> slotsInOrder = workerSlots;
        slotsInOrder = slotsInOrder.OrderBy(
            x => Vector2.Distance(unit.transform.position, x.transform.position)
           ).ToList();
        for (int i = 0; i < slotsInOrder.Count; i++)
        {
            // 2- Find a slot empty
            if (!slotsInOrder[i].queue.IsUnitInQueue())
            {
                slotsInOrder[i].queue.AddUnitToQueue(unit);
                selectedSlot = slotsInOrder[i];
                break;
            }
            else
            {
                // 3- No empty slot, add to the queue with the less worker on
                if (numberOfUnitAlreadyIn > slotsInOrder[i].queue.GetQueueLength())
                {
                    indexEmptyiestSlot = i;
                    numberOfUnitAlreadyIn = slotsInOrder[i].queue.GetQueueLength();
                }
            }
        }
        if (selectedSlot == null)
        {
            if (indexEmptyiestSlot != -1)
            {
                slotsInOrder[indexEmptyiestSlot].queue.AddUnitToQueue(unit);
                selectedSlot = slotsInOrder[indexEmptyiestSlot];
            }
            else
            {
                // 4-No best queue found, add to the first one
                slotsInOrder[0].queue.AddUnitToQueue(unit);
                selectedSlot = slotsInOrder[0];
                // If Index was out of range error pop - You did not set the worker slot to the resource
            }
        }
        return selectedSlot;
    }

    public void RemoveFirstUnitToWorkerQueue(Unit unit)
    {
        for (int i = 0; i < workerSlots.Count; i++)
        {
            if (workerSlots[i].queue.IsUnitInQueue(unit))
            {
                workerSlots[i].queue.DequeueUnit();
                break;
            }
        }
    }

    public void RemoveUnitFromQueue(Unit unit)
    {
        for (int i = 0; i < workerSlots.Count; i++)
        {
            if (workerSlots[i].queue.IsUnitInQueue(unit))
            {
                workerSlots[i].queue.RemoveUnitFromQueue(unit);
                break;
            }
        }
    }

    public bool IsUnitOnAllQueue()
    {
        bool areAllUsed = true;
        foreach (WorkerSlot ws in workerSlots)
        {
            if (!ws.queue.IsUnitInQueue())
            {
                areAllUsed = false;
                break;
            }
        }
        return areAllUsed;
    }*/
}
