﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Wizard : RangedUnit
{
    public SelectableObject slowTarget;

    private SpellActionObject slowAction;
    private bool isDoingSlowAction = false;
    protected override void Start()
    {
        base.Start();
        slowTarget = null;
        uiNumber = DataManager.PRIEST_UI_NUMBER;
        for (int i = 0; i < actions.Length; i++)
            if (actions[i] != null && CustomActionName.SLOW_SPEED.Equals(actions[i].GetActionNameData()))
                slowAction = actions[i] as SpellActionObject;
    }

    new void Update()
    {
        base.Update();
        if (slowTarget != null)
        {
            PerformSlow();
        }
    }
    protected override void MoveToPosition()
    {
        if (target != null)
            attacking = AdjustPosition(target);
    }

    #region Decision engine

    protected override bool ShouldMakeDecision()
    {
        if (slowTarget != null) return false;
        return base.ShouldMakeDecision();
    }

    protected override void DecideWhatToDo()
    {
        if (slowTarget != null) return;
        
        base.DecideWhatToDo();
    }

    #endregion


    public override bool MouseClick(GameObject hitObject, Vector3 hitPoint, Player controller, Behaviour behaviour)
    {
        ResetUnitState();
        return base.MouseClick(hitObject, hitPoint, controller, behaviour);
    }

    public override void PerformLeftClickAction(CustomActionName actionToPerform)
    {
        base.PerformLeftClickAction(actionToPerform);
        if (CustomActionName.SLOW_SPEED.Equals(actionToPerform) && slowAction.CanDoAction())
            player.SetCustomActionClick(slowAction, UserAction.SAME_SQUAD_UNITS);
    }

    public override void PerformRightClickAction(CustomActionName actionToPerform)
    {
        if (CustomActionName.SLOW_SPEED.Equals(actionToPerform))
            slowAction.automaticAction = !slowAction.automaticAction;
    }

    public override bool CanDoAction(CustomActionName actionName)
    {
        base.CanDoAction(actionName);
        if (CustomActionName.SLOW_SPEED.Equals(actionName))
            return slowAction.CanDoAction();

        return false;
    }

    public override void ExecuteAction(CustomActionName actionName, Vector3 hitPoint, GameObject hitObject)
    {
        base.ExecuteAction(actionName, hitPoint, hitObject);
        switch (actionName)
        {
            case CustomActionName.SLOW_SPEED:
                BeginSlowUnit(hitObject.GetComponent<SelectableObject>());
                break;
        }
    }

    #region Slow action

    private void BeginSlowUnit(SelectableObject t)
    {
        if (t == null)
            return;
        sounds.PlaySound(audioSource, SoundCategorie.ACCEPT);
        this.slowTarget = t;
        if (TargetInRange(slowTarget))
        {
            PerformSlow();
        }
        else
        {
            AdjustPosition(slowTarget);
        }
    }

    private void PerformSlow()
    {
        if (isDoingSlowAction)
            return;

        if (slowTarget == null || slowTarget.IsDead())
        {
            slowTarget = null;
            return;
        }
        AimAtTarget(slowTarget);
        if (!TargetInRange(slowTarget)) AdjustPosition(slowTarget);
        else StartCoroutine(SlowUnit());
    }

    private IEnumerator SlowUnit()
    {
        if (slowTarget.IsDead())
            yield return null;

        isDoingSlowAction = true;
        if (animator != null)
            animator.SetTrigger("Attack");
        Debug.Log("Use slow");
        slowAction.ExecuteAction(slowTarget);
        yield return new WaitForSeconds(slowAction.GetActionDuration());
        slowTarget = null;
        isDoingSlowAction = false;
    }
    #endregion


    protected override void ResetUnitState()
    {
        base.ResetUnitState();
        slowTarget = null;
    }
}
