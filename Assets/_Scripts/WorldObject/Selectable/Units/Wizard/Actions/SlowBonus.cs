﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowBonus : Bonus
{
    public SlowBonus(BonusData data) : base(data) {

    }

    public override void ApplyBonus(SelectableObject o)
    {
        o.agent.speed = o.GetStats().speed * GetEffectValue();
    }

    public override void RemoveBonus(SelectableObject o)
    {
        o.agent.speed = o.GetStats().speed;
    }
}
