﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Worker : MeleeUnit
{
    [Header("Worker")]
    // Capacity the unit can load at max
    public int woodCollectionAmount;
    public int goldCollectionAmount;
    private int collectionAmount;
    public int currentLoad = 0, capacity = 10;
    // Is the unit harvesting, or emptying his resources
    [SerializeField]
    private bool harvesting = false, emptying = false;

    // Type of resource currently harvesting
    [HideInInspector]
    public ResourceType harvestType;

    // Resource currently used for harvest
    private Resource resource;
    private Vector3 resourceDepositPosition;
    // The position is usefull to find another resource close to this one when the previous is destroyed
    private WorkerSlot resourceWorkerSlot;

    // Building where to empty the resources once the capacity is full
    public Building resourceStore;
    private WorkerSlot resourceStoreWorkerSlot;

    // Size of the area use when the resource is empty, and the unit is searching for another
    public float searchResourceArea;

    private Building currentProject;
    private bool building = false;
    private bool repairing = false;

    // Change the Actions panel to see the buildings to construct
    private CustomActionObject[] buildingActions;
    private CustomActionObject[] defaultActions;
    public CustomActionObject showBuildingsPanel;
    public CustomActionObject hideBuildingsPanel;


    // Allow the worker to change weapon depending on the activity
    [SerializeField]
    private List<AssociatedWeapon> associatedActivityWeapon;
    [SerializeField]
    private List<AssociatedWeapon> carryBags;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
        harvestType = ResourceType.UNKNOWN;
        /*constructionTime = DataManager.HARVESTER_CONSTRUCTION_TIME;*/
        uiNumber = DataManager.WORKER_UI_NUMBER;
        agentDefaultAvoidancePriority = agent.avoidancePriority;
        defaultActions = actions;
        this.buildingActions = GetComponentInChildren<BuildingActions>().GetActions();
    }

    new void Update()
    {
        base.Update();
        if (!rotating && !moving)
        {
            if (harvesting || emptying)
            {
                ManageResourceBehaviour();
            } else if (building && currentProject)
            {
                ManageBuildingBehaviour();
            }
        }
        else
        {
            // Can the unit go to the deposit or store point ?
            if (harvesting) 
            {
                CheckIfWorkerSlotIsAvailable(resourceWorkerSlot);
            } 
            else if (emptying)
            {
                CheckIfWorkerSlotIsAvailable(resourceStoreWorkerSlot);
            }
        }
    }

    private void ManageResourceBehaviour()
    {
        if (harvesting)
        {
            if (resource != null)
            {
                if (currentLoad < capacity)
                {
                    SetActionAvoidancePriority();
                    TurnUnitToFaceResource(resourceWorkerSlot);
                    if (moving) StopMoving();
                    Collect();
                }
                if (currentLoad >= capacity)
                {
                    animator.SetBool("Harvest", false);
                    harvesting = false;
                    emptying = true;
                    MoveToStore(true);
                }
            }
            else
            {
                SearchAnotherResource();
            }
        }
        else
        {
            // The unit is now at the Building worker slot
            if (resourceStore != null)
            {
                SetActionAvoidancePriority();
                if (moving) StopMoving();
                TurnUnitToFaceResource(resourceStoreWorkerSlot);
                StoreResources();
                if (currentLoad <= 0)
                {
                    // The unit has finished to empty his package
                    resourceStore.RemoveFirstUnitToWorkerQueue(this);
                    emptying = false;

                    MoveToResource();
                }
            }
            else
            {
                SearchNearestStore();
            }
        }
    }

    private void ManageBuildingBehaviour()
    {
        if (repairing)
        {
            if (currentProject.CanDoActions())
            {
                TurnUnitToFaceObject(currentProject.transform.position);
                animator.SetBool("Harvest", true);
                currentProject.Repair(Time.deltaTime);
                if (currentProject.hitPoints == currentProject.GetMaxHitPoints())
                {
                    ResetUnitState();
                }
            }
            else
                ResetUnitState();
        }
        else
        {
            animator.SetBool("Harvest", true);
            TurnUnitToFaceObject(currentProject.transform.position);
            currentProject.Construct(Time.deltaTime);
            if (!currentProject.IsUnderConstruction())
            {
                currentProject.HighlightClicked();
                ResetUnitState();
            }
        }
    }

    protected override bool ShouldMakeDecision()
    {
        if (harvesting || emptying || building) return false;
        return base.ShouldMakeDecision();
    }
    protected override void DecideWhatToDo()
    {
        base.DecideWhatToDo();
        // Is there a building close to repair ?
        Vector3 currentPosition = transform.position;
        if (!moving && !attacking && !movingIntoPosition && !harvesting && !building && !emptying)
        {
            nearbyObjects = WorkManager.FindNearbyObjects(currentPosition, GetStats().detectionRange);
            List<SelectableObject> buildingToRepair = new List<SelectableObject>();
            foreach (SelectableObject nearbyObject in nearbyObjects)
            {
                if (nearbyObject.IsInvulnerable()) continue;
                if (nearbyObject.player != player) continue;
                Building building = nearbyObject.GetComponent<Building>();
                if (building != null && building.CanDoActions() && building.hitPoints < building.GetMaxHitPoints()) buildingToRepair.Add(nearbyObject);
            }
            SelectableObject closestObject = WorkManager.FindNearestWorldObjectInListToPosition(buildingToRepair, currentPosition);
            if (closestObject) SetRepairBuilding(closestObject as Building);
        }
    }

    public override bool MouseClick(GameObject hitObject, Vector3 hitPoint, Player controller, Behaviour behaviour)
    {
        ResetUnitState();
        bool hasDoneAction = base.MouseClick(hitObject, hitPoint, controller, behaviour);
        if (hasDoneAction)
            return true;
        //only handle input if owned by a human player
        if (player && player.human)
        {
            if (hitObject && !WorkManager.ObjectIsGround(hitObject))
            {
                Resource resource = hitObject.GetComponentInParent<Resource>();
                if (resource && !resource.IsEmpty())
                {
                    ClickOnResource(resource);
                    hasDoneAction = true;
                }
                else
                {
                    Building building = hitObject.GetComponentInParent<Building>();
                    // If we click on one of our warehouse (or town all), go empty our capacity
                    if (building != null && building.player == player)
                    {
                        if (building.IsUnderConstruction())
                        {
                            // Go to continue the building construction
                            SetBuilding(building);
                        } else if (building.isWarehouse && currentLoad > 0)
                        {
                            // Go to the building, store our current capacity
                            building.HighlightClicked();
                            harvesting = false;
                            emptying = true;

                            resourceStore = building;
                            MoveToStore(false);
                            hasDoneAction = true;
                        } else if (building.CanDoActions() && building.hitPoints < building.GetMaxHitPoints())
                        {
                            SetRepairBuilding(building);
                            hasDoneAction = true;
                        }
                    }
                    ChooseCurrentWeapon();
                }
            }
        }
        return hasDoneAction;
    }

    public override void PerformLeftClickAction(CustomActionName actionToPerform)
    {
        base.PerformLeftClickAction(actionToPerform);

        if (showBuildingsPanel.GetActionNameData().Equals(actionToPerform))
        {
            actions = buildingActions;
        } else if (hideBuildingsPanel.GetActionNameData().Equals(actionToPerform))
        {
            actions = defaultActions;
        } else
        {
            for(int i = 0; i < actions.Length; i++)
                if (actions[i] != null && actions[i].GetActionNameData() == actionToPerform && actions[i].GetActionType() == ActionType.CREATE_BUILDING)
                    player.squad.ExecuteCustomAction(actionToPerform, Vector3.zero, null, UserAction.SAME_SQUAD_UNITS);
        }

    }

    public override void PerformRightClickAction(CustomActionName actionToPerform)
    {
        // Nothing to do
    }

    public override void ExecuteAction(CustomActionName actionName, Vector3 hitPoint, GameObject hitObject)
    {
        base.ExecuteAction(actionName, hitPoint, hitObject);
        for (int i = 0; i < actions.Length; i++)
            if (actions[i] != null && actions[i].GetActionNameData() == actionName && actions[i].GetActionType() == ActionType.CREATE_BUILDING)
            {
                CreateBuilding(actionName);
                actions = defaultActions;
                break;
            }

    }

    public override bool CanDoAction(CustomActionName actionName)
    {
        return !building;
    }

    protected override void ResetUnitState()
    {
        base.ResetUnitState();
        animator.SetBool("Harvest", false);
        if (resourceStore != null)
        {
            resourceStore.RemoveUnitFromWorkerQueue(this);
        }
        resourceStore = null;
        resourceStoreWorkerSlot = null;
        if (resource != null)
        {
            resource.RemoveUnitFromQueue(this);
        }
        resource = null;
        resourceDepositPosition = ResourceManager.InvalidPosition;
        resourceWorkerSlot = null;

        harvesting = false;
        emptying = false;
        building = false;
        repairing = false;
        currentProject = null;
    }

    public override void StartMove(Vector3 destination)
    {
        base.StartMove(destination);
    }

    public bool IsWorking()
    {
        return emptying || harvesting || moving || rotating || building;
    }

    public override void SetSelection(bool v)
    {
        base.SetSelection(v);
        actions = defaultActions;
    }

    #region Weapon changement

    public void ChangeCurrentWeapon(string name)
    {
        if (associatedActivityWeapon != null && associatedActivityWeapon.Count > 0)
        {
            foreach (AssociatedWeapon aw in associatedActivityWeapon)
            {
                if (aw.weapon != null)
                {
                    if (name.Equals(aw.name))
                    {
                        aw.weapon.SetActive(true);
                    }
                    else
                    {
                        aw.weapon.SetActive(false);
                    }
                }
            }
        }
        // Up to the object to know when to change
    }
 
    public void ChooseCurrentWeapon()
    {
        if (building)
        {
            ChangeCurrentWeapon("hammer");
        } else
        {
            switch (harvestType)
            {
                case ResourceType.WOOD:
                    ChangeCurrentWeapon("axe");
                    break;
                case ResourceType.GOLD:
                    ChangeCurrentWeapon("pick");
                    break;
                default:
                    ChangeCurrentWeapon("");
                    break;
            }
        }
    }

    #endregion

    #region Building creation

    private void OnTriggerEnter(Collider other)
    {
        if (building)
        {
            if (!other.CompareTag(DataManager.GROUND_TAG))
            {
                WorldObject wo = other.GetComponent<WorldObject>();
                if (wo != null && currentProject.ObjectId == wo.ObjectId)
                {
                    moving = false;
                    // The worker have reached the building, he can start construct it
                    agent.SetDestination(transform.position);
                    TurnUnitToFaceObject(wo.transform.position);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
    }

    public override void SetBuilding(Building project)
    {
        ResetUnitState();
        SetActionAvoidancePriority();
        if (player.human)
            PlaySound(SoundCategorie.ACCEPT);
        base.SetBuilding(project);
        currentProject = project;
        currentProject.HighlightClicked();
        Collider c = currentProject.GetComponent<Collider>();
        Vector3 targetLocation = currentProject.transform.position;
        if (c)
        {
            Vector3 pos = c.transform.position;
            Quaternion rot = c.transform.rotation;
            targetLocation = Physics.ClosestPoint(this.transform.position, c, pos, rot);
        }
        StartMove(targetLocation, currentProject.gameObject);
        building = true;
        ChooseCurrentWeapon();
    }

    private void SetRepairBuilding(Building project)
    {
        ResetUnitState();
        SetActionAvoidancePriority();
        project.HighlightClicked();
        base.SetBuilding(project);
        currentProject = project;

        Collider c = currentProject.GetComponent<Collider>();
        Vector3 targetLocation = currentProject.transform.position;
        if (c)
        {
            Vector3 pos = c.transform.position;
            Quaternion rot = c.transform.rotation;
            targetLocation = Physics.ClosestPoint(this.transform.position, c, pos, rot);
        }
        StartMove(targetLocation, currentProject.gameObject);
        repairing = true;
        building = true;
        ChooseCurrentWeapon();
    }

    private void CreateBuilding(CustomActionName buildingName)
    {
        if (player) player.CreateBuilding(buildingName, Input.mousePosition, this);
    }

    #endregion

    #region Resource management

    public void ClickOnResource(Resource resource)
    {
        if (resource == null)
            return;
        // If we change our resource, stop harvesting before reaching the new one
        if (this.resource != null && this.resource.ObjectId != resource.ObjectId)
        {
            ResetUnitState();
        }
        if (player.human)
        {
            resource.HighlightClicked();
            PlaySound(SoundCategorie.ACCEPT);
        }
        // Specific resource code
        if (resource.IsUnitOnAllQueue() && ResourceType.GOLD != resource.GetResourceType())
        {
            harvestType = resource.GetResourceType();
            resourceDepositPosition = resource.transform.position;
            SearchAnotherResource();
        }
        else
        {
            GoToHarvestPoint(resource);
        }        
    }

    private void CheckIfWorkerSlotIsAvailable(WorkerSlot slot)
    {
        if (slot != null)
        {
            if (slot.CanUseResource(this))
            {
                // Unit turn to empty, so go on (to the harvest slot)
                agent.isStopped = false;
            }
            else
            {
                // Another unit is currently using the slot, stop moving when reaching the waiting bounds
                if (slot.IsUnitOnBounds(this))
                {
                    agent.velocity = Vector3.zero;
                    agent.isStopped = true;
                }
            }
        } else
        {
            if (harvesting)
            {
                SearchAnotherResource();
            } else
            {
                SearchNearestStore();
            }
        }
    }

    private void TurnUnitToFaceResource(WorkerSlot workerSlot)
    {
        if (workerSlot != null)
            TurnUnitToFaceObject(workerSlot.resourcePosition.position);
    }

    private void MoveToResource()
    {
        agent.isStopped = false;
        // Is the ressource can still be harvested
        if (resource != null && !resource.IsEmpty())
        {
            harvesting = true;
            // Go back to the resource deposit
            GoToHarvestPoint(resource);
        }
        else
        {
            SearchAnotherResource();
        }
    }
    
    private void MoveToStore(bool searchStore)
    {
        if (searchStore)
            SearchNearestStore();
        agent.isStopped = false;
        // 1- Release the queue
        if (resource != null)
            resource.RemoveFirstUnitToWorkerQueue(this);
        if (resourceStore != null)
        {
            // 2- Find store slot and move
            resourceStoreWorkerSlot = resourceStore.AddUnitToWorkerQueue(this);
            StartMove(resourceStoreWorkerSlot.transform.position, resourceStore.gameObject);
        }
        else
        {
            Debug.Log("Unit without store, waiting for orders");
            // No store has been found, the unit can do nothing on its own, stop moving, waiting for click
        }
    }

    private void SearchAnotherResource()
    {
        // 1- The resource has been removed, search for another one
        // 1.1- Search close to the old deposit
        if (animator != null)
        {
            animator.SetBool("Moving", true);
            animator.SetBool("Harvest", false);
        }
        SetActionAvoidancePriority();
        Resource resource = ResourceManager.SearchSimilarResourceInArea(harvestType, resourceDepositPosition, searchResourceArea);

        if (resource == null)
        {
            // 1.2- Search close to the unit
            resource = ResourceManager.SearchSimilarResourceInArea(harvestType, transform.position, searchResourceArea);
        }

        if (resource != null)
        {
            GoToHarvestPoint(resource);
        } else
        {
            // 2- No other similat resource has been found. Move from the workerSlot to let other unit continue working
            // Current solution - Disable nav mesh agent to let the other unit go though this one
            // Do not forget to re enable it when the unit need to move again
            agent.enabled = false;
            moving = false;
            harvesting = false;
        }

    }

    private void GoToHarvestPoint(Resource resource)
    {
        /*if (audioElement != null) audioElement.Play(startHarvestSound);*/
        this.resource = resource;
        switch (resource.GetResourceType())
        {
            case ResourceType.WOOD:
                collectionAmount = woodCollectionAmount;
                break;
            case ResourceType.GOLD:
                collectionAmount = goldCollectionAmount;
                break;
            default:
                collectionAmount = 0;
                Debug.LogError("Unknown resource, impossible to set the collection amount");
                break;
        }
        // Prevent an action already ready when the worker reach the position
        currentWeaponChargeTime = 0f;

        resourceDepositPosition = resource.transform.position;
        resourceWorkerSlot = this.resource.AddUnitToWorkerQueue(this);
        // If we send all unit on the same resource
        StartMove(resourceWorkerSlot.transform.position, resource.gameObject);

        if (harvestType == ResourceType.UNKNOWN || harvestType != resource.GetResourceType())
        {
            harvestType = resource.GetResourceType();
            currentLoad = 0;
        }
        ChooseCurrentWeapon();

        harvesting = true;
        emptying = false;
    }
    
    private void Collect()
    {
        currentWeaponChargeTime += Time.deltaTime;
        if (currentWeaponChargeTime >= GetStats().weaponRechargeTime)
        {
            currentWeaponChargeTime = 0f;
            animator.SetBool("Harvest", true);
            /*if (audioElement != null && Time.timeScale > 0) audioElement.Play(harvestSound);*/
            int collect = collectionAmount;
            
            // Make sure that the harvester cannot collect more than it can carry
            if (currentLoad + collect > capacity)
            {
                collect = capacity - currentLoad;
            }
            // Make sure we are not gathering more resource than available
            int resourceAmountLeft = resource.GetResourceLeft();
            if (collect > resourceAmountLeft)
            {
                collect = resourceAmountLeft;
            }

            // Add collect to current load
            currentLoad += collect;
            ShowResourceBag();
            if (resource.IsEmpty())
            {
                // ResetUnitState();
                SearchAnotherResource();
            }
            else
            {
                // Remove collect amount from resource
                resource.Remove(collect);
            }
        }
    }

    private void ShowResourceBag()
    {
        foreach (AssociatedWeapon aw in carryBags)
        {
            if (harvestType.ToString().Equals(aw.name))
                aw.weapon.SetActive(true);
            else
                aw.weapon.SetActive(false);
        }
    }

    private void HideResourceBag()
    {
        foreach (AssociatedWeapon aw in carryBags)
        {
           aw.weapon.SetActive(false);
        }
    }

    private void StoreResources()
    {
        currentWeaponChargeTime += Time.deltaTime;
        if (currentWeaponChargeTime >= GetStats().weaponRechargeTime)
        {
            currentWeaponChargeTime = 0f;
            /*if (audioElement != null && Time.timeScale > 0) audioElement.Play(harvestSound);*/
            int deposit = capacity;

            if (deposit > currentLoad) deposit = Mathf.FloorToInt(currentLoad);
            currentLoad -= deposit;
            HideResourceBag();
            player.AddResource(harvestType, deposit);
        }
    }

    private void SearchNearestStore()
    {
        List<Building> warehouses = player.GetAllWarehousesAcceptingThisResource(harvestType);
        float distance = 9999999f;
        Building nearestWarehouse = null;
        for (int i = 0; i < warehouses.Count; i++)
        {
            if (warehouses[i] == null)
                break;
            float d = Vector3.Distance(transform.position, warehouses[i].transform.position);
            if (d < distance)
            {
                distance = d;
                nearestWarehouse = warehouses[i];
            }

        }
        if (nearestWarehouse == null)
        {
            Debug.Log("No warehouse found ! Not an error tho, the unit will just wait");
        }
        resourceStore = nearestWarehouse;
    }

    #endregion
}
