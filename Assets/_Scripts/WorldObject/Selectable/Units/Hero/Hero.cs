﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Hero : Unit
{    
    private CustomActionObject[] weapon1Actions;
    private CustomActionObject[] weapon2Actions;
    private CustomActionObject[] defaultActions;
    
    [Header("Weapon 1")]
    public CustomActionObject weapon1Action;
    public List<GameObject> weapon1;
    public RuntimeAnimatorController weapon1Animator;
    public ObjectStat weapon1Stats;
    public int weapon1Level;

    [Header("Weapon 2")]
    public CustomActionObject weapon2Action;
    public List<GameObject> weapon2;
    public RuntimeAnimatorController weapon2Animator;
    public ObjectStat weapon2Stats;
    public int weapon2Level;

    [Header("Weapon 2 details")]
    public GameObject projectileSlot;
    public GameObject projectile;
    public int projectileVelocity;
    // Use this variable to match the animation
    public float timeBeforeShooting;

    private int currentWeapon = 1;

    protected override void Start()
    {
        base.Start();
        // ChooseObjectStat().GetAttackType = weapon1Stats.attackType;
        uiNumber = DataManager.HERO_UI_NUMBER;
        weapon1Stats.InitObjectStats(weapon1Level);
        weapon2Stats.InitObjectStats(weapon2Level);

        defaultActions = actions;
        weapon1Actions = FilterActions(GetComponentInChildren<Weapon1Actions>().GetActions());
        weapon2Actions = FilterActions(GetComponentInChildren<Weapon2Actions>().GetActions());
        
        AddWeaponActionsToCurrentActions(weapon1Actions);
    }

    private CustomActionObject[] FilterActions(CustomActionObject[] customActionObjects)
    {
        List<CustomActionObject> actions = new List<CustomActionObject>();

        for (int i = 0; i < customActionObjects.Length; i++)
            if (customActionObjects[i] != null)
                actions.Add(customActionObjects[i]);

        return actions.ToArray();
    }

    private void AddWeaponActionsToCurrentActions(CustomActionObject[] acts)
    {
        // Disable auto action before deleting them
        for (int i = 0; i < actions.Length; i++)
        {
            if (actions[i] != null && actions[i].handleAction)
            {
                actions[i].StopAction();
            }
        }

        int nbActionsToAdd = acts.Length;
        int currentActionIndex = 0;
        CustomActionObject[] newActions = new CustomActionObject[actions.Length];
        bool isActionFound;
        for (int i = 0; i < actions.Length; i++)
        {
            isActionFound = false;
            for (int k = 0; k < defaultActions.Length; k++)
            {
                if (defaultActions[k] != null && !defaultActions[k].hideAction && (defaultActions[k].position - 1) == i)
                {
                    newActions[i] = defaultActions[k];
                    isActionFound = true;
                    break;
                }
            }
            if (!isActionFound)
            {
                for (int k = 0; k < acts.Length; k++)
                {
                    if (acts[k] != null && !acts[k].hideAction && (acts[k].position - 1) == i)
                    {
                        newActions[i] = acts[k];

                        if (newActions[i].handleAction)
                            newActions[i].SetupAction(this);

                        break;
                    }
                }
            }
        }

        actions = newActions;
    }

    new void Update()
    {
        base.Update();
    }

    protected override bool ShouldMakeDecision()
    {
        return base.ShouldMakeDecision();
    }

    protected override void UseWeapon()
    {
        if (target.IsDead())
            return;

        SelectableObject wo = target.GetComponent<SelectableObject>();
        if (animator != null)
            animator.SetTrigger("Attack");
        switch(ChooseObjectStat().GetAttackType())
        {
            case AttackType.MELEE:
                wo.TakeDamage(GetStats().GetAttackValue(), this);
                break;
            case AttackType.RANGED:
                StartCoroutine(FireProjectile());
                break;
        }
        
        if (wo.IsDead())
            target = null;
        base.UseWeapon();
    }

    private IEnumerator FireProjectile()
    {
        yield return new WaitForSeconds(timeBeforeShooting);
        GameObject go = Instantiate(projectile, projectileSlot.transform.position, projectileSlot.transform.rotation);
        Projectile p = go.GetComponent<Projectile>();
        p.SetProjectileData(projectileVelocity, GetStats().GetAttackValue());
        p.SetRange(GetStats().weaponRange);
        p.SetSetOpponents(target, this);
    }

    public override bool MouseClick(GameObject hitObject, Vector3 hitPoint, Player controller, Behaviour behaviour)
    {
        ResetUnitState();
        return base.MouseClick(hitObject, hitPoint, controller, behaviour);
    }

    public override ObjectStat ChooseObjectStat()
    {
        return currentWeapon != 1 ? weapon2Stats : weapon1Stats;
    }

    public override void PerformLeftClickAction(CustomActionName actionToPerform)
    {
        base.PerformLeftClickAction(actionToPerform);
        if (actionToPerform.Equals(weapon1Action.GetActionNameData()))
        {
            ShowWeapons(2);
            currentWeapon = 2;
            animator.runtimeAnimatorController = weapon2Animator;

            AddWeaponActionsToCurrentActions(weapon2Actions);
            // ChangeSwitchWeaponAction(weapon2Action, weapon1Action);
        }
        else if (actionToPerform.Equals(weapon2Action.GetActionNameData()))
        {
            ShowWeapons(1);
            currentWeapon = 1;
            animator.runtimeAnimatorController = weapon1Animator;

            AddWeaponActionsToCurrentActions(weapon1Actions);
            // ChangeSwitchWeaponAction(weapon1Action, weapon2Action);
        }
    }

    private void ChangeSwitchWeaponAction(CustomActionObject newAction, CustomActionObject oldAction)
    {
        for(int i = 0; i < actions.Length; i++)
        {
            if (actions[i] != null && actions[i].GetActionNameData() == oldAction.GetActionNameData())
            {
                actions[i] = newAction;
                break;
            }
        }
    }

    private void ShowWeapons(int weaponToShow)
    {
        if (weaponToShow == 1)
        {
            ChangeWeaponState(weapon1, true);
            ChangeWeaponState(weapon2, false);
        } else
        {
            ChangeWeaponState(weapon2, true);
            ChangeWeaponState(weapon1, false);
        }
    }

    private void ChangeWeaponState(List<GameObject> weapons, bool active)
    {
        for (int i = 0; i < weapons.Count; i++)
            weapons[i].SetActive(active);
    }


    protected override void ResetUnitState()
    {
        base.ResetUnitState();
    }

    public override void StartMove(Vector3 destination)
    {
        base.StartMove(destination);
    }

    public override void CheckUpgrade()
    {
        // Not concerned by upgrades
    }
}
