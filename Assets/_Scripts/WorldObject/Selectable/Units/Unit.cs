﻿using System;
using UnityEngine;
using UnityEngine.AI;

public class Unit : SelectableObject
{
    [Header("Unit")]
    // public float moveSpeed = 5;
    public int actionAvoidancePriorityMax;

    private GameObject destinationTarget;
    private Vector3 destination, aggressiveDestination;
    [SerializeField]
    protected bool moving, rotating;

    protected int agentDefaultAvoidancePriority = 20;

    private SelectableObject targetToFollow = null;
    protected override void Start()
    {
        base.Start();
        agent.speed = GetStats().speed;
    }
    protected override void Update()
    {
        base.Update();
        if (moving) MakeMove();

        if (animator != null)
        {
            if (agent.enabled)
            {
                animator.SetBool("Moving", agent.isStopped ? false : moving);
            }
            else
            {
                animator.SetBool("Moving", false);
            }
        }
    }

    public override void SetPlayer(Player p)
    {
        base.SetPlayer(p);
        player.AddUnitToPlayer(this);
        SetMaterials();
        CheckUpgrade();
    }

    public override bool MouseClick(GameObject hitObject, Vector3 hitPoint, Player controller, Behaviour behaviour)
    {
        bool hasDoneAction = false;
        //only handle input if owned by a human player and currently selected
        if (player && player.human && currentlySelected)
        {
            hasDoneAction = base.MouseClick(hitObject, hitPoint, controller, behaviour);
            if (hasDoneAction)
                return true;
            if ((WorkManager.ObjectIsGround(hitObject)) && hitPoint != ResourceManager.InvalidPosition)
            {
                float x = hitPoint.x;
                float y = hitPoint.y;
                float z = hitPoint.z;
                /*Vector3 */destination = new Vector3(x, y, z);
                StartMove(destination);
                if (Behaviour.AGGRESSIVE.Equals(behaviour))
                {
                    PlaySound(SoundCategorie.ATTACK);
                    aggressiveDestination = destination;
                }
                else
                    PlaySound(SoundCategorie.ACCEPT);
                hasDoneAction = true;
            } else
            {
                SelectableObject obj = hitObject.GetComponentInParent<SelectableObject>();
                if (obj && player.team == obj.player.team)
                {
                    Unit unit = obj as Unit;
                    if (unit)
                    {
                        this.behaviour = Behaviour.FOLLOWING;
                        obj.HighlightClicked();
                        targetToFollow = obj;
                        hasDoneAction = true;
                        PlaySound(SoundCategorie.ACCEPT);
                    }
                }
            }
        }
        return hasDoneAction;
    }

    public override void StartMove(Vector3 destination)
    {
        this.destination = destination;
        destinationTarget = null;
        RemoveActionAvoidancePriority();
        moving = true;
        attacking = false;
    }

    public override void StartMove(Vector3 destination, GameObject destinationTarget)
    {
        StartMove(destination);
        this.destinationTarget = destinationTarget;
    }

    public override void SetColliders(bool enable)
    {

        Collider[] colliders = GetComponents<Collider>();
        if (colliders != null)
        {
            for (int i = 0; i < colliders.Length; i++)
                colliders[i].enabled = enable;
        }
    }

    protected void SetActionAvoidancePriority()
    {
        agent.avoidancePriority = UnityEngine.Random.Range(0, actionAvoidancePriorityMax);
    }

    protected void RemoveActionAvoidancePriority()
    {
        agent.avoidancePriority = agentDefaultAvoidancePriority;
    }

    public void StopMoving()
    {
        agent.SetDestination(transform.position);
        moving = false;
        movingIntoPosition = false;
    }

    private void MakeMove()
    {
        if (agent.enabled) // We do that because when a unit empty his resources and does not have new resource to harvest, we disable the agent to let the other unit go though it.
        {
            agent.SetDestination(destination);
            if (!agent.pathPending)
            {
                if (agent.remainingDistance <= agent.stoppingDistance)
                {
                    moving = false;
                    movingIntoPosition = false;
                    // if (audioElement != null) audioElement.Stop(driveSound);
                }
                CalculateBounds();
            }
        }
    }

    public override bool CanAttack()
    {
        return true;
    }

    public void PlayCreatedSound()
    {
        if (player.human)
            PlaySound(SoundCategorie.CREATED, false, true);
    }

    protected override void ObjectDeath()
    {
        player.RemoveUnit(this);
        base.ObjectDeath();
    }

    protected override void DecideWhatToDo()
    {
        base.DecideWhatToDo();
        switch (behaviour)
        {
            case Behaviour.FOLLOWING:
                if (Vector3.Distance(targetToFollow.transform.position, transform.position) > 6.5f)
                {
                    Vector3 newPosition = Squad.GetUnitPositionWithAngle(new Vector3(0f, 0f, -5f), Quaternion.LookRotation(targetToFollow.transform.position - transform.position).eulerAngles.y);
                    StartMove(targetToFollow.transform.position + newPosition);
                } else
                {
                    // We are close enough, should we do something else whiel we are close ?
                    if (CanAttack())
                    {
                        FindNearestEnemy();
                    }
                }
                break;
            case Behaviour.AGGRESSIVE:
                if (target == null && (Vector3.Distance(aggressiveDestination, transform.position) > 2f))
                {
//                    Debug.Log(objectName + " nothing found, continue moving unitl reach the destination");
                    StartMove(aggressiveDestination);
                }
                break;
        }
    }

    protected override void AimAtTarget(SelectableObject t)
    {
        AimAtTarget(t.transform.position);
        aiming = false;
    }

    protected override void AimAtTarget(Vector3 position)
    {
        TurnUnitToFaceObject(position);
        aiming = false;
    }

    protected override bool ShouldMakeDecision()
    {
        if (Behaviour.NORMAL.Equals(behaviour) && (moving ||rotating))
            return false;
        return base.ShouldMakeDecision();
    }

    public virtual void SetBuilding(Building building)
    {
        // Specific initialization for a unit can be specified here to create a building
    }

    protected override void ResetUnitState()
    {
        base.ResetUnitState();
        agent.enabled = true;
        moving = false;
        rotating = false;
        aggressiveDestination = ResourceManager.InvalidPosition;
        RemoveActionAvoidancePriority();
        animator.SetBool("Moving", false);
    }

    public override void SetMaterials()
    {
        base.SetMaterials();
        Material material = ColorManager.getMaterial(player.color, DataManager.UNIT_MATERIAL);
        if (partsToColor != null && partsToColor.Count > 0)
        {
            foreach (Renderer r in partsToColor)
            {
                r.material = material;
            }
        }

        // If it's an enemy unit, will hide it when in the FOG
        if (LevelLoaderCstm.isFoW() && partToHideInFog != null && partToHideInFog.Length > 0)
        {
            int humanPlayerTeam = GameManager.getHumanPlayerTeam();
            for (int i = 0; i < partToHideInFog.Length; i++)
            {
                FoW.HideInFog fog = partToHideInFog[i].AddComponent<FoW.HideInFog>();
                fog.team = humanPlayerTeam;
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(destination, 1f);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(aggressiveDestination, 1f);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, GetStats().detectionRange);
        /*// Attack range of the unit
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, GetStats().weaponRange);
        // Detection range of the unit
        // Defensive maximum distance
        if (positionToDefend != ResourceManager.InvalidPosition)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(positionToDefend, 1f);
        }*/
    }
}

[System.Serializable]
public enum Behaviour
{
    NORMAL, DEFENSIVE, AGGRESSIVE, FOLLOWING
}