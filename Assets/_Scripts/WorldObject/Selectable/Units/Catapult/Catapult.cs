﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Catapult : SiegeUnit
{
    private CustomActionObject fireProjectileAction;
    protected override void Start()
    {
        base.Start();
        uiNumber = DataManager.CATAPULT_UI_NUMBER;
        for (int i = 0; i < actions.Length; i++)
            if (CustomActionName.FIRE_PROJECTILE.Equals(actions[i]))
                fireProjectileAction = actions[i];

    }

    new void Update()
    {
        base.Update();
    }
    protected override void MoveToPosition()
    {
        if (target != null)
            attacking = AdjustPosition(target);
    }

    #region Decision engine

    protected override bool ShouldMakeDecision()
    {
        if (positionToFire != ResourceManager.InvalidPosition)
        {
            if (timeSinceLastDecision > timeBetweenDecisions)
            {
                timeSinceLastDecision = 0f;
                return true;
            } else
            {
                timeSinceLastDecision += Time.deltaTime;
                return false;
            }
        }
        else
        {
            return base.ShouldMakeDecision();
        }
        
    }

    protected override void DecideWhatToDo()
    {
        if (positionToFire != ResourceManager.InvalidPosition)
        {
            PerformFirePosition();
        } else
        {
            base.DecideWhatToDo();
        }
    }

    #endregion


    public override bool MouseClick(GameObject hitObject, Vector3 hitPoint, Player controller, Behaviour behaviour)
    {
        ResetUnitState();
        return base.MouseClick(hitObject, hitPoint, controller, behaviour);
    }

    public override void PerformLeftClickAction(CustomActionName actionToPerform)
    {
        base.PerformLeftClickAction(actionToPerform);
        if (CustomActionName.FIRE_PROJECTILE.Equals(actionToPerform))
            player.SetCustomActionClick(fireProjectileAction, UserAction.SAME_SQUAD_UNITS);
    }
    public override void PerformRightClickAction(CustomActionName actionToPerform)
    {
        // Nothing
    }

    public override void ExecuteAction(CustomActionName actionName, Vector3 hitPoint, GameObject hitObject)
    {
        base.ExecuteAction(actionName, hitPoint, hitObject);
        if (CustomActionName.FIRE_PROJECTILE.Equals(actionName))
        {
            if (hitPoint != ResourceManager.InvalidPosition)
            {
                ResetUnitState();
                BeginFirePosition(hitPoint);
            }
        }
    }
    protected void BeginFirePosition(Vector3 position)
    {
        /*if (audioElement != null) audioElement.Play(attackSound);*/
        positionToFire = position;
        // currentWeaponChargeTime = GetStats().weaponRechargeTime;
        if (TargetInRange(position))
        {
            attacking = true;
            PerformFirePosition();
        }
        else attacking = AdjustPosition(position);
    }
    protected void PerformFirePosition()
    {
        if (positionToFire == ResourceManager.InvalidPosition)
        {
            attacking = false;
            return;
        }
        if (!TargetInRange(positionToFire)) attacking = AdjustPosition(positionToFire);
        AimAtTarget(positionToFire);
        if (ReadyToFire()) UseWeaponToFirePosition();
    }

    protected override void ResetUnitState()
    {
        positionToFire = ResourceManager.InvalidPosition;
        base.ResetUnitState();
    }
}
