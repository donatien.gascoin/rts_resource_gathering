﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealBonus : Bonus
{

    public HealBonus(BonusData data): base(data) { }

    public override void ApplyBonus(SelectableObject o)
    {
        o.Heal(GetEffectValue());
    }
}
