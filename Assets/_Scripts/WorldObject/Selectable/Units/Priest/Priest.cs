﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Priest : RangedUnit
{

    private bool healing;
    private SpellActionObject healAction;

    public GameObject healAura;
    public GameObject weaponHealAura;
    public GameObject weaponHealSlot;
    public float weaponHealEffectDuration;
    private SelectableObject healTarget;
    public int healValue;
    private bool healUnitWithClick;

    protected override void Start()
    {
        healUnitWithClick = false;
        healTarget = null;
        base.Start();
        uiNumber = DataManager.PRIEST_UI_NUMBER;

        for (int i = 0; i < actions.Length; i++)
        {
            if (actions[i] != null && actions[i].GetActionNameData() == CustomActionName.HEAL_UNIT)
            {
                healAction = actions[i] as SpellActionObject;
            }

        }
    }

    new void Update()
    {
        base.Update();

        if (healUnitWithClick && healTarget != null && !movingIntoPosition)
        {
            PerformHeal();
        }
    }
    protected override void MoveToPosition()
    {
        if (target != null)
            attacking = AdjustPosition(target);
        if (healTarget != null)
            healing = AdjustPosition(healTarget);
    }

    #region Decision engine

    protected override bool ShouldMakeDecision()
    {
        if (healUnitWithClick)
        {
            return false;
        }
        return base.ShouldMakeDecision();
    }

    protected override void DecideWhatToDo()
    {
        base.DecideWhatToDo();
        // If not doing anything, then search for close unit to heal
        if (!IsDead() && !attacking && !moving && !movingIntoPosition && !rotating && !aiming && !healUnitWithClick)
        {
            if (healAction != null && healAction.automaticAction)
            {
                if (healTarget != null)
                {
                    PerformHeal();
                } else
                {
                    Vector3 currentPosition = transform.position;
                    nearbyObjects = WorkManager.FindNearbyObjects(currentPosition, GetStats().detectionRange);
                    List<SelectableObject> allyObjects = new List<SelectableObject>();
                    foreach (SelectableObject nearbyObject in nearbyObjects)
                    {
                        if (nearbyObject.IsInvulnerable()) continue;
                        Resource resource = nearbyObject.GetComponent<Resource>();
                        if (resource) continue;
                        Building building = nearbyObject.GetComponent<Building>();
                        if (building != null) continue;
                        if (nearbyObject.player == player
                            && UnitType.NORMAL.Equals(nearbyObject.objectStats.GetUnitType()) 
                            && nearbyObject.hitPoints < nearbyObject.GetStats().maxHitPoint) allyObjects.Add(nearbyObject);
                    }
                    SelectableObject closestObject = WorkManager.FindNearestWorldObjectInListToPosition(allyObjects, currentPosition);
                    if (closestObject) BeginHeal(closestObject);
                }
            }
        }
    }

    #endregion

    #region Healing

    private void BeginHeal(SelectableObject t)
    {
        if (t == null)
            return;
        /*if (audioElement != null) audioElement.Play(attackSound);*/
        this.healTarget = t;
        /*currentWeaponChargeTime = weaponRechargeTime;*/
        if (TargetInRange(healTarget))
        {
            healing = true;
            PerformHeal();
        }
        else
        {
            healing = AdjustPosition(healTarget);
        }
    }

    private void PerformHeal()
    {
        if (healTarget == null || healTarget.IsDead())
        {
            healing = false;
            return;
        }
        AimAtTarget(healTarget);
        if (!TargetInRange(healTarget)) healing = AdjustPosition(healTarget);
        else if (ReadyToFire() && ConsumeMana(healAction.GetManaRequired())) HealUnit();
    }

    private void HealUnit()
    {
        if (healTarget.IsDead())
            return;
        if (animator != null)
            animator.SetTrigger("Attack");
        StartCoroutine(FireHealingProjectile());
        currentWeaponChargeTime = 0.0f;
        if (healTarget.hitPoints >= healTarget.GetStats().maxHitPoint)
        {
            healTarget = null;
            healUnitWithClick = false;
            healing = false;
        }
    }

    private IEnumerator FireHealingProjectile()
    {
        yield return new WaitForSeconds(timeBeforeShooting);
        if (healTarget != null)
        {
            GameObject aura = Instantiate(healAura, healTarget.transform.position, healTarget.transform.rotation, healTarget.transform);
            GameObject weaponAura = Instantiate(weaponHealAura, weaponHealSlot.transform.position, weaponHealSlot.transform.rotation, weaponHealSlot.transform);
            Destroy(weaponAura, weaponHealEffectDuration);
            Aura p = aura.GetComponent<Aura>();
            p.SetProjectileData(healValue, false);
            p.SetTarget(healTarget, this);
            healTarget.AddBonus(new HealBonus(healAction.bonus), true);
        }
    }

    #endregion


    public override bool MouseClick(GameObject hitObject, Vector3 hitPoint, Player controller, Behaviour behaviour)
    {
        ResetUnitState();
        bool hasDoneAction = base.MouseClick(hitObject, hitPoint, controller, behaviour);
        if (hasDoneAction)
            return true;
        if (player && player.human)
        {
            if (hitObject && !WorkManager.ObjectIsGround(hitObject))
            {
                Unit unit = hitObject.GetComponent<Unit>();
                if (unit != null && unit.player.Equals(player))
                {
                    if (unit.hitPoints < unit.GetStats().maxHitPoint)
                    {
                        ResetUnitState();
                        behaviour = Behaviour.NORMAL;
                        healUnitWithClick = true;
                        BeginHeal(unit);
                        unit.HighlightClicked();
                        hasDoneAction = true;
                    }
                }
            }
        }
        return hasDoneAction;
    }

    public override void PerformLeftClickAction(CustomActionName actionToPerform)
    {
        base.PerformLeftClickAction(actionToPerform);
        if (healAction.GetActionNameData().Equals(actionToPerform))
            player.SetCustomActionClick(healAction, UserAction.SAME_SQUAD_UNITS);
    }

    public override void PerformRightClickAction(CustomActionName actionToPerform)
    {
        if (actionToPerform.Equals(CustomActionName.HEAL_UNIT))
        {
            player.squad.PerformRightClickOnAction(actionToPerform);
        }
    }

    public override void ExecuteAction(CustomActionName actionName, Vector3 hitPoint, GameObject hitObject)
    {
        base.ExecuteAction(actionName, hitPoint, hitObject);
        if (healAction.GetActionNameData().Equals(actionName))
        {
            if (!WorkManager.ObjectIsGround(hitObject))
            {
                Unit unit = hitObject.GetComponent<Unit>();
                if (unit != null && unit.player.Equals(player))
                {
                    if (unit.hitPoints < unit.GetStats().maxHitPoint)
                    {
                        ResetUnitState();
                        behaviour = Behaviour.NORMAL;
                        healUnitWithClick = true;
                        BeginHeal(unit);
                        unit.HighlightClicked();
                        sounds.PlaySound(this, audioSource, SoundCategorie.ACCEPT);
                    }
                }
            }
        }
    }

    public override bool CanDoAction(CustomActionName actionName)
    {
        if (healAction != null && healAction.GetActionNameData().Equals(actionName))
        {
            return !healing;
        }

        return false;
    }

    public override void ExecuteRightClickOnAction(CustomActionName actionName)
    {
        if (healAction.GetActionNameData().Equals(actionName))
        {
            healAction.automaticAction = !healAction.automaticAction;
        }
    }


    protected override void ResetUnitState()
    {
        base.ResetUnitState();
        healUnitWithClick = false;
        healTarget = null;
        healing = false;
    }
}
