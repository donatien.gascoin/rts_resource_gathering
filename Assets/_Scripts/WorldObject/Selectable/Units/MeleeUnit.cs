﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeUnit : Unit
{
    protected override void Start()
    {
        base.Start();
        // ChooseObjectStat().attackType = AttackType.MELEE;
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void UseWeapon()
    {
        if (target.IsDead())
            return;
        if (animator != null)
            animator.SetTrigger("Attack");
        SelectableObject wo = target.GetComponent<SelectableObject>();
        wo.TakeDamage(GetStats().GetAttackValue(), this);
        if (wo.IsDead())
            target = null;
        base.UseWeapon();
    }
}
