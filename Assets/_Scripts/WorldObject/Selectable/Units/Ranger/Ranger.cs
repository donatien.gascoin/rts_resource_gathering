﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ranger : RangedUnit
{
    protected override void Start()
    {
        base.Start();
        uiNumber = DataManager.RANGER_UI_NUMBER;
    }

    new void Update()
    {
        base.Update();
    }

    protected override bool ShouldMakeDecision()
    {
        return base.ShouldMakeDecision();
    }

    public override bool MouseClick(GameObject hitObject, Vector3 hitPoint, Player controller, Behaviour behaviour)
    {
        ResetUnitState();
        return base.MouseClick(hitObject, hitPoint, controller, behaviour);
    }

    public override void PerformLeftClickAction(CustomActionName actionToPerform)
    {
        base.PerformLeftClickAction(actionToPerform);
    }


    protected override void ResetUnitState()
    {
        base.ResetUnitState();
    }

    public override void StartMove(Vector3 destination)
    {
        base.StartMove(destination);
    }
}
