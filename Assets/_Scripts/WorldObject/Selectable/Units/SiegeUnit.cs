﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SiegeUnit : Unit
{
    public GameObject projectileSlot;
    public GameObject projectile;
    public GameObject fakeProjectile;
    public int projectileVelocity;
    // public float timeBeforeReleaseProjectile;
    // Use this variable to match the animation
    public float timeBeforeShooting;
    public float firingAngle = 45.0f;
    public float gravity = 9.8f;

    public Vector3 positionToFire = ResourceManager.InvalidPosition;

    protected override void Start()
    {
        base.Start();
        // ChooseObjectStat().attackType = AttackType.SIEGE;
    }


    protected override void UseWeapon()
    {
        if (target.IsDead())
            return;
        if (animator != null)
            animator.SetTrigger("Attack");
        StartCoroutine(FireProjectile());
        base.UseWeapon();
    }

    private IEnumerator FireProjectile()
    {
        GameObject fake = Instantiate(fakeProjectile, projectileSlot.transform.position, projectileSlot.transform.rotation, projectileSlot.transform);
        yield return new WaitForSeconds(timeBeforeShooting);
        if (target != null)
        {
            float angleX = Quaternion.LookRotation(target.transform.position - projectileSlot.transform.position).eulerAngles.x;
            float angleY = Quaternion.LookRotation(target.transform.position - projectileSlot.transform.position).eulerAngles.y;
            float angleZ = Quaternion.LookRotation(target.transform.position - projectileSlot.transform.position).eulerAngles.z;
            projectileSlot.transform.rotation = Quaternion.Euler(angleX, angleY, angleZ);

            GameObject go = Instantiate(projectile, projectileSlot.transform.position, projectileSlot.transform.rotation);
            Projectile p = go.GetComponent<Projectile>();
            p.SetProjectileData(projectileVelocity, GetStats().GetAttackValue());
            p.SetRange(GetStats().weaponRange);
            p.SetSetOpponents(target, this);
            StartCoroutine(ThrowProjectile(go, target.transform.position));
            Destroy(fake);
        }
    }


    protected void UseWeaponToFirePosition()
    {
        if (positionToFire == ResourceManager.InvalidPosition)
            return;
        if (animator != null)
            animator.SetTrigger("Attack");
        StartCoroutine(FireProjectileToPosition());
        base.UseWeapon();
    }

    private IEnumerator FireProjectileToPosition()
    {
        GameObject fake = Instantiate(fakeProjectile, projectileSlot.transform.position, projectileSlot.transform.rotation, projectileSlot.transform);
        yield return new WaitForSeconds(timeBeforeShooting);
        if (positionToFire != ResourceManager.InvalidPosition)
        {
            float angleX = Quaternion.LookRotation(positionToFire - projectileSlot.transform.position).eulerAngles.x;
            float angleY = Quaternion.LookRotation(positionToFire - projectileSlot.transform.position).eulerAngles.y;
            float angleZ = Quaternion.LookRotation(positionToFire - projectileSlot.transform.position).eulerAngles.z;
            projectileSlot.transform.rotation = Quaternion.Euler(angleX, angleY, angleZ);

            GameObject go = Instantiate(projectile, projectileSlot.transform.position, projectileSlot.transform.rotation);
            Projectile p = go.GetComponent<Projectile>();
            p.SetProjectileData(projectileVelocity, GetStats().GetAttackValue());
            p.SetRange(GetStats().weaponRange);
            p.SetSetOpponents(null, this, positionToFire);
            StartCoroutine(ThrowProjectile(go, positionToFire));
            Destroy(fake);
        }
    }

    IEnumerator ThrowProjectile(GameObject projectile, Vector3 targetPosition)
    {
        float target_Distance = Vector3.Distance(projectile.transform.position, targetPosition);

        // Calculate the velocity needed to throw the object to the target at specified angle.
        float projectile_Velocity = target_Distance / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / gravity);

        // Extract the X  Y componenent of the velocity
        float Vx = Mathf.Sqrt(projectile_Velocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
        float Vy = Mathf.Sqrt(projectile_Velocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);

        // Calculate flight time.
        float flightDuration = target_Distance / Vx;

        // Rotate projectile to face the target.
        projectile.transform.rotation = Quaternion.LookRotation(targetPosition - projectile.transform.position);

        float elapse_time = 0;

        while (elapse_time < flightDuration)
        {
            if (projectile == null)
            {
                elapse_time = flightDuration;
            } else
            {
                projectile.transform.Translate(0, (Vy - (gravity * elapse_time)) * Time.deltaTime, Vx * Time.deltaTime);

                elapse_time += Time.deltaTime;

                yield return null;
            }
        }
        // Flight is over, destroy the projectile
        if (projectile != null)
        {
            Projectile p = projectile.GetComponent<Projectile>();
            p.TargetReached();
        }
    }
}
