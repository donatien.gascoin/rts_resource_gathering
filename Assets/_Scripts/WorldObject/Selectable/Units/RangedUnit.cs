﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedUnit : Unit
{
    public GameObject projectileSlot;
    public GameObject projectile;
    public int projectileVelocity;
    // Use this variable to match the animation
    public float timeBeforeShooting;
    protected override void Start()
    {
        base.Start();
    }


    protected override void UseWeapon()
    {
        if (target.IsDead())
            return;
        if (animator != null)
            animator.SetTrigger("Attack");
        StartCoroutine(FireProjectile());
        base.UseWeapon();
    }

    private IEnumerator FireProjectile()
    {
        yield return new WaitForSeconds(timeBeforeShooting);
        if (target != null) {
            float angleX = Quaternion.LookRotation(target.transform.position - projectileSlot.transform.position).eulerAngles.x;
            float angleY = Quaternion.LookRotation(target.transform.position - projectileSlot.transform.position).eulerAngles.y;
            float angleZ = Quaternion.LookRotation(target.transform.position - projectileSlot.transform.position).eulerAngles.z;
            projectileSlot.transform.rotation = Quaternion.Euler(angleX, angleY, angleZ);

            GameObject go = Instantiate(projectile, projectileSlot.transform.position, projectileSlot.transform.rotation);
            Projectile p = go.GetComponent<Projectile>();
            p.SetProjectileData(projectileVelocity, GetStats().GetAttackValue());
            p.SetRange(GetStats().weaponRange);
            p.SetSetOpponents(target, this);
        }
    }
}
