﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Cost
{
    [SerializeField]
    public ResourceType type;
    public int amount;

    public Cost()
    {

    }
    public Cost(ResourceType type, int amount)
    {
        this.type = type;
        this.amount = amount;
    }
}

