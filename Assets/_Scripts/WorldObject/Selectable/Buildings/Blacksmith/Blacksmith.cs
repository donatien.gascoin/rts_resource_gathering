﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blacksmith : Building
{
    public Upgrades upgradeActionMelee;
    private bool upgradeActionMeleeOnQueue;
    public Upgrades upgradeActionRanged;
    private bool upgradeActionRangedOnQueue;

    protected override void Start()
    {
        base.Start();
        uiNumber = DataManager.BLACKSMITH_UI_NUMBER;
        upgradeActionMeleeOnQueue = false;
        upgradeActionRangedOnQueue = false;
    }

    protected override void Update()
    {
        base.Update();
        if (upgradeActionMeleeOnQueue)
        {
            upgradeActionMeleeOnQueue = !CheckIfUpgradeHasBeenRemovedFromQueue(upgradeActionMelee.upgradeType.ToString());
            if (!upgradeActionMeleeOnQueue)
                actions[0] = upgradeActionMelee.GetCurrentUpgradeAction().action;
        }
        if (upgradeActionRangedOnQueue)
        {
            upgradeActionRangedOnQueue = !CheckIfUpgradeHasBeenRemovedFromQueue(upgradeActionRanged.upgradeType.ToString());
            if (!upgradeActionMeleeOnQueue)
                actions[1] = upgradeActionRanged.GetCurrentUpgradeAction().action;
        }
    }

    private bool CheckIfUpgradeHasBeenRemovedFromQueue(string name)
    {
        bool removed = true;
        for (int i = 0; i < buildQueue.Count; i++)
        {
            if (buildQueue[i].buildName == name)
                removed = false;
        }

        return removed;
    }

    public override void InitActions()
    {
        // actions.Clear();
        upgradeActionMelee = player.GetUpgradeStatus(UpgradeType.MELEE_UPGRADES);
        if (upgradeActionMelee.GetCurrentUpgradeAction() != null)
            actions[0] = upgradeActionMelee.GetCurrentUpgradeAction().action;

        upgradeActionRanged = player.GetUpgradeStatus(UpgradeType.RANGED_UPGRADES);
        if (upgradeActionRanged.GetCurrentUpgradeAction() != null)
            actions[1] = upgradeActionRanged.GetCurrentUpgradeAction().action;
    }

    public override void PerformLeftClickAction(CustomActionName actionToPerform)
    {
        if (upgradeActionMelee.GetCurrentUpgradeAction() != null && upgradeActionMelee.GetCurrentUpgradeAction().action.GetActionNameData() == actionToPerform)
        {
            if (!upgradeActionMeleeOnQueue)
            {
                upgradeActionMeleeOnQueue = true;
                CreateUpgrade(upgradeActionMelee);
            }
        } else if (upgradeActionRanged.GetCurrentUpgradeAction() != null && upgradeActionRanged.GetCurrentUpgradeAction().action.GetActionNameData() == actionToPerform)
        {
            if (!upgradeActionRangedOnQueue)
            {
                upgradeActionRangedOnQueue = true;
                CreateUpgrade(upgradeActionRanged);
            }
        }
    }

    protected override bool ShouldMakeDecision()
    {
        return false;
    }

    protected override void SetRallyPoint()
    {
        // Nothing to do
    }

}
