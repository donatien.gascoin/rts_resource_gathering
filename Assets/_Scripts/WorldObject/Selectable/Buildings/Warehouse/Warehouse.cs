﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warehouse : Building
{
    protected override void Start()
    {
        base.Start();
        isWarehouse = true;
    }

    public override void PerformLeftClickAction(CustomActionName actionToPerform)
    {
        base.PerformLeftClickAction(actionToPerform);
        CreateUnit(actionToPerform);
    }

    protected override bool ShouldMakeDecision()
    {
        return false;
    }

    protected override void SetRallyPoint()
    {
        base.SetRallyPoint();

        Vector3 mousePosition = Input.mousePosition;
        GameObject hitObject = WorkManager.FindHitObject(mousePosition);
        Vector3 hitPoint = WorkManager.FindHitPoint(mousePosition);
        if (hitObject && hitPoint != ResourceManager.InvalidPosition)
        {
            if (!WorkManager.ObjectIsGround(hitObject))
            {
                // If clicked on a resource, the created unit should automatically go harvest 
                // TODO

                // If clicked on anotherunit, the created unit should follow it
                // TODO
            }
        }
    }

}
