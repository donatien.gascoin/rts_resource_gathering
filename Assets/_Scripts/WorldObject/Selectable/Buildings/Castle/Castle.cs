﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Castle : Building
{
    private bool isHero;
    private bool isFirstHeroCheck;
    protected override void Start()
    {
        base.Start();
        isHero = true;
        isFirstHeroCheck = true;
        uiNumber = DataManager.TOWN_HALL_UI_NUMBER;
    }

    public override void PerformLeftClickAction(CustomActionName actionToPerform)
    {
        if (actionToPerform.Equals(CustomActionName.HERO) && player.HaveHero())
            return;
        CreateUnit(actionToPerform);
    }

    protected override void Update()
    {
        if (!isUnderConstruction && !isCurrentlyPlacingOnField)
        {
            base.Update();
        }
    }

    public override void SetSelection(bool v)
    {
        base.SetSelection(v);
        if (v)
        {
            for (int i = 0; i < actions.Length; i++)
                if (actions[i] != null && actions[i].GetActionNameData().Equals(CustomActionName.HERO))
                    actions[i].disableAction = player.HaveHero();
        }
    }

    protected override bool ShouldMakeDecision()
    {
        return false;
    }

    protected override void SetRallyPoint()
    {
        base.SetRallyPoint();
        Vector3 mousePosition = Input.mousePosition;
        GameObject hitObject = WorkManager.FindHitObject(mousePosition);
        Vector3 hitPoint = WorkManager.FindHitPoint(mousePosition);
        if (hitObject && hitPoint != ResourceManager.InvalidPosition)
        {
            if (!WorkManager.ObjectIsGround(hitObject))
            {
                // If clicked on a resource, the created unit should automatically go harvest 
                // TODO

                // If clicked on anotherunit, the created unit should follow it
                // TODO
            }
        }
    }

}
