﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BuildObject
{
    public string buildName;
    public Sprite image;
    public BuildType type;

    public BuildObject(string unitName, Sprite image, BuildType type)
    {
        this.buildName = unitName;
        this.image = image;
        this.type = type;
    }
}

[System.Serializable]
public enum BuildType
{
    UNIT, UPGRADE
}
