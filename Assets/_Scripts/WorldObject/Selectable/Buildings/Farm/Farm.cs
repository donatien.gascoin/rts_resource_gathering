﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Farm : Building
{
    protected override void Start()
    {
        base.Start();
        uiNumber = DataManager.FARM_UI_NUMBER;
    }

    public override void PerformLeftClickAction(CustomActionName actionToPerform)
    {
        // Nothing to do
    }

    protected override bool ShouldMakeDecision()
    {
        return false;
    }

    protected override void SetRallyPoint()
    {
        // Nothing to do
    }

}
