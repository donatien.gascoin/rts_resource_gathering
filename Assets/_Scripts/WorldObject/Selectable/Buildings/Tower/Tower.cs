﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : Building
{
    public GameObject projectileSlot;
    public GameObject projectile;
    public int projectileVelocity;
    // Use this variable to match the animation
    public float timeBeforeShooting;

    protected override void Start()
    {
        base.Start();
        uiNumber = DataManager.FARM_UI_NUMBER;
    }

    public override bool CanAttack()
    {
        return true;
    }
    protected override void AimAtTarget(SelectableObject t)
    {
        float angleX = Quaternion.LookRotation(t.transform.position - projectileSlot.transform.position).eulerAngles.x;
        float angleY = Quaternion.LookRotation(t.transform.position - projectileSlot.transform.position).eulerAngles.y;
        float angleZ = Quaternion.LookRotation(t.transform.position - projectileSlot.transform.position).eulerAngles.z;
        projectileSlot.transform.rotation = Quaternion.Euler(angleX, angleY, angleZ);
        aiming = false;
    }

    protected override bool ShouldMakeDecision()
    {
        return base.ShouldMakeDecision();
    }

    public override bool MouseClick(GameObject hitObject, Vector3 hitPoint, Player controller, Behaviour behaviour)
    {
        ResetUnitState();
        return base.MouseClick(hitObject, hitPoint, controller, behaviour);
    }
    protected override void UseWeapon()
    {
        if (target.IsDead())
            return;
        if (animator != null)
            animator.SetTrigger("Attack");
        StartCoroutine(FireProjectile());
        base.UseWeapon();
    }

    private IEnumerator FireProjectile()
    {
        yield return new WaitForSeconds(timeBeforeShooting);
        GameObject go = Instantiate(projectile, projectileSlot.transform.position, projectileSlot.transform.rotation);
        go.transform.localScale = new Vector3(2, 2, 2);
        Projectile p = go.GetComponent<Projectile>();
        p.SetProjectileData(projectileVelocity, GetStats().GetAttackValue());
        p.SetRange(GetStats().weaponRange);
        p.SetSetOpponents(target, this);
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, GetStats().weaponRange);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, GetStats().detectionRange);
    }
}
