﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownHall : Building
{
    protected override void Start()
    {
        base.Start();
        uiNumber = DataManager.TOWN_HALL_UI_NUMBER;
    }

    public override void PerformLeftClickAction(CustomActionName actionToPerform)
    {
        if (actionToPerform.Equals(CustomActionName.HERO) && player.HaveHero())
            return;
        CreateUnit(actionToPerform);
    }

    protected override void Update()
    {
        if (!isUnderConstruction && !isCurrentlyPlacingOnField)
        {
            base.Update();
        }
    }

    public override void SetSelection(bool v)
    {
        base.SetSelection(v);
        if (v)
        {
            for (int i = 0; i < actions.Length; i++)
                if (actions[i] != null && actions[i].GetActionNameData().Equals(CustomActionName.HERO))
                    actions[i].disableAction = player.HaveHero();
        }
    }

    protected override bool ShouldMakeDecision()
    {
        return false;
    }

    protected override void SetRallyPoint()
    {
        base.SetRallyPoint();
    }

}
