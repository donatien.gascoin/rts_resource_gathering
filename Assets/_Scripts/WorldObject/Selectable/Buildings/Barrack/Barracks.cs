﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barracks : Building
{
    protected override void Start()
    {
        base.Start();
        isWarehouse = false;
        uiNumber = DataManager.BARRACKS_UI_NUMBER;
    }

    public override void PerformLeftClickAction(CustomActionName actionToPerform)
    {
        base.PerformLeftClickAction(actionToPerform);
        CreateUnit(actionToPerform);
    }

    protected override bool ShouldMakeDecision()
    {
        return false;
    }

    protected override void SetRallyPoint()
    {
        base.SetRallyPoint();
        Vector3 mousePosition = Input.mousePosition;
        GameObject hitObject = WorkManager.FindHitObject(mousePosition);
        Vector3 hitPoint = WorkManager.FindHitPoint(mousePosition);
        if (hitObject && hitPoint != ResourceManager.InvalidPosition)
        {
            if (!WorkManager.ObjectIsGround(hitObject))
            {
                // If clicked on anotherunit, the created unit should follow it
                // TODO
            }
        }
    }

}
