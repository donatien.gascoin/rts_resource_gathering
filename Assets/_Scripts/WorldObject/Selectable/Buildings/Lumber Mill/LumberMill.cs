﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LumberMill : Building
{
    protected override void Start()
    {
        base.Start();
        isWarehouse = true;
        uiNumber = DataManager.LUMBER_MILL_UI_NUMBER;
    }
    public override void PerformLeftClickAction(CustomActionName actionToPerform)
    {
        CreateUnit(actionToPerform);
    }

    protected override bool ShouldMakeDecision()
    {
        return false;
    }

    protected override void SetRallyPoint()
    {
        base.SetRallyPoint();
    }

}
