﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.EventSystems;
using UnityEngine.AI;

public class Building : SelectableObject
{
    [Header("Building")]
    [SerializeField]
    private NavMeshObstacle obstacle;
    // Show to the human if the building can be placed or not
    public Renderer placeableMarker;
    public List<WorkerSlot> workerSlots;

    [Header("Building construction")]
    public bool isUnderConstruction = false;
    [SerializeField]
    protected List<GameObject> constructionSteps;
    protected int currentShape;
    [SerializeField]
    protected GameObject shape;

    [Header("Building upgrade")]
    public GameObject upgradedBuilding;
    private string upgradeName;

    private bool isUpgradingBuilding = false;
    protected int currentNewShape;
    [HideInInspector]
    public float upgradeTime;
    [HideInInspector]
    public float currentUpgradeTime;
    [HideInInspector]
    public Sprite upgradeImage;

    [Header("Building destruction")]
    public GameObject destructionLvl1;

    public GameObject destructionLvl2;
    public GameObject buildingDestroyed;

    /*public float maxBuildProgress = 10.0f;*/
    public AudioClip finishedJobSound;
    public float finishedJobVolume = 1.0f;


    [HideInInspector]
    public ListQueue<BuildObject> buildQueue;
    private SelectableObject objCurrentlyConstructed;
    private Upgrades upgradeCurrentlyConstructed;
 
    // Time for the build created by the building, not the creation time for this building.
    [HideInInspector]
    public float currentBuildProgress = 0.0f;
    [HideInInspector]
    public float currentBuildCreationTime = 0.0f;

    [HideInInspector]
    public List<Collider> colliders = new List<Collider>();

    [Header("Resource management")]
    public bool isWarehouse = false;
    public List<ResourceType> resourcesAccepted;
    // Food provided by the building
    public int additionalFood;


    [Header("Rally point")]
    public GameObject spawnPoint, rallyPoint;
    public Animator rallyPointAnimator;
    public float rallyPointRadius = 2f;

    protected bool isCurrentlyPlacingOnField;

    #region RallyPoint on object

    // On another object
    private SelectableObject objectToFollow;

    // On a resource
    private ResourceType resourceToFind;

    #endregion

    #region Monobehavior

    protected override void Awake()
    {
        base.Awake();
        upgradeName = "";
        isUnderConstruction = false;
        isUpgradingBuilding = false;
        buildQueue = new ListQueue<BuildObject>();
        SetObstacle(false);
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        if (!isUnderConstruction && !isUpgradingBuilding)
        {
            base.Update();
            ProcessConstructionQueue();
            if (Input.GetMouseButtonUp(1) && currentlySelected && player.human)
            {
                if (EventSystem.current.IsPointerOverGameObject())
                    return;
                SetRallyPoint();
            }
        } else
        {
            if (isUpgradingBuilding)
            {
                Upgrade();
            }
        }
    }

    #endregion

    #region Decision engine

    protected override bool ShouldMakeDecision()
    {
        if (isUnderConstruction || isCurrentlyPlacingOnField) return false;
        return base.ShouldMakeDecision();
    }

    protected override void ObjectDeath()
    {
        hitPoints = 0;
        buildingDestroyed.SetActive(true);
        Debug.Log(player);
        player.RemoveBuilding(this);
        base.ObjectDeath();
    }

    #endregion

    #region Setter

    public override void TakeDamage(float damage, SelectableObject attacker)
    {
        base.TakeDamage(damage, attacker);
        CheckHitPoints();
    }

    public override void CheckHitPoints()
    {
        int maxHitPoints = GetStats().maxHitPoint;
        if (hitPoints < ((float)1 / 3 * maxHitPoints))
        {
            destructionLvl1.SetActive(true);
            destructionLvl2.SetActive(true);
        }
        else if (hitPoints < ((float)2 / 3 * maxHitPoints))
        {
            destructionLvl1.SetActive(true);
            destructionLvl2.SetActive(false);
        }
        else
        {
            destructionLvl1.SetActive(false);
            destructionLvl2.SetActive(false);
        }
    }

    public virtual void InitActions() { }

    public override void SetPlayer(Player p)
    {
        base.SetPlayer(p);
        player.AddBuildingToPlayer(this);
        SetMaterials();
        ChangeBuildingBanner();
        InitActions();
        CheckUpgrade();
    }

    public override void SetObstacle(bool enable)
    {
        if (obstacle != null)
            obstacle.enabled = enable;
    }

    public override void SetColliders (bool enable)
    {

        Collider[] colliders = GetComponents<Collider>();
        if (colliders != null)
        {
            for (int i = 0; i < colliders.Length; i++)
                colliders[i].enabled = enable;
        }
    }

    public void SetPlaceableMarker(bool enable)
    {
        placeableMarker.gameObject.SetActive(enable);
    }

    protected virtual void SetRallyPoint()
    {
        SetRallyPoint(Input.mousePosition);
    }

    public virtual void SetRallyPoint(Vector3 position)
    {
        if (rallyPoint != null)
        {
            GameObject hitObject = WorkManager.FindHitObject(position);
            Vector3 hitPoint = WorkManager.FindHitPoint(position);
            if (hitObject && hitPoint != ResourceManager.InvalidPosition)
            {
                if (WorkManager.ObjectIsGround(hitObject))
                {
                    rallyPoint.transform.position = new Vector3(hitPoint.x, hitPoint.y, hitPoint.z);
                    if (rallyPointAnimator != null)
                        rallyPointAnimator.SetTrigger("Deploy");
                }
                else
                {
                    // If we click on itself, set the rally point at the spawn point
                    WorldObject wo = hitObject.GetComponent<WorldObject>();
                    if (wo != null && ObjectId == wo.ObjectId)
                    {
                        rallyPoint.transform.position = spawnPoint.transform.position;
                        if (rallyPointAnimator != null)
                            rallyPointAnimator.SetTrigger("Deploy");
                        return;
                    }

                    // Click on another ally unit
                    SelectableObject obj = wo as SelectableObject;
                    if (obj)
                    {

                        return;
                    }

                    // Click on a resource
                    Resource resource = wo as Resource;
                    if (resource)
                    {

                        return;
                    }
                }
            }
        }
    }

    public override void SetSelection(bool v)
    {
        base.SetSelection(v);
        if (rallyPoint != null && player != null && player.human)
        {
            rallyPoint.SetActive(v);
        }
    }

    #endregion

    #region Getter

    public override bool CanDoActions()
    {
        return !IsUnderConstruction() && !IsUpgrading();
    }

    public int GetMaxHitPoints()
    {
        return GetStats().maxHitPoint;
    }

    public bool IsUnderConstruction()
    {
        return isUnderConstruction;
    }

    public bool IsUpgrading()
    {
        return isUpgradingBuilding;
    }

    public string GetUpgradeName()
    {
        if (upgradeName.Equals(""))
            upgradeName = upgradedBuilding.GetComponent<Building>().objectName;
        return upgradeName;
    }

    public bool IsWarehouse()
    {
        return isWarehouse;
    }

    #endregion

    #region Worker queue

    public void RemoveUnitFromWorkerQueue(Unit unit)
    {
        for (int i = 0; i < workerSlots.Count; i++)
        {
            if (workerSlots[i].queue.IsUnitInQueue(unit))
            {
                workerSlots[i].queue.RemoveUnitFromQueue(unit);
                break;
            }
        }
    }

    public Vector3 GetAvailableWorkerSlot(Unit unit)
    {
        List<WorkerSlot> slots = new List<WorkerSlot>();
        for (int i = 0; i < slots.Count; i++)
        {
            if (!slots[i].queue.IsUnitInQueue() || slots[i].queue.IsUnitFirstInQueue(unit))
            {
                return slots[i].transform.position;
            }
        }
        return ResourceManager.InvalidPosition;
    }

    public WorkerSlot AddUnitToWorkerQueue(Unit unit)
    {
        WorkerSlot selectedSlot = null;
        int indexEmptyiestSlot = -1, numberOfUnitAlreadyIn = 999;

        // 1- Sort slots in distance from unit
        List<WorkerSlot> slotsInOrder = workerSlots;
        slotsInOrder = slotsInOrder.OrderBy(
            x => Vector2.Distance(unit.transform.position, x.transform.position)
           ).ToList();
        for (int i = 0; i < slotsInOrder.Count; i++)
        {
            // 2- Find a slot empty
            if (!slotsInOrder[i].queue.IsUnitInQueue())
            {
                selectedSlot = slotsInOrder[i];
                break;
            }
            else
            {
                // 3- No empty slot, add to the queue with the less worker on
                if (numberOfUnitAlreadyIn > slotsInOrder[i].queue.GetQueueLength())
                {
                    indexEmptyiestSlot = i;
                    numberOfUnitAlreadyIn = slotsInOrder[i].queue.GetQueueLength();
                }
            }
        }
        if (selectedSlot == null)
        {
            if (indexEmptyiestSlot != -1)
            {
                selectedSlot = slotsInOrder[indexEmptyiestSlot];
            }
            else
            {
                // 4-No best queue found, add to the first one
                selectedSlot = slotsInOrder[0];
            }
        }
        selectedSlot.queue.AddUnitToQueue(unit);
        return selectedSlot;
    }

    public WorkerSlot GetUnitBestWorkerQueue(Unit unit)
    {
        WorkerSlot selectedSlot = null;
        int indexEmptyiestSlot = -1, numberOfUnitAlreadyIn = 999;

        // 1- Sort slots in distance from unit
        List<WorkerSlot> slotsInOrder = workerSlots;
        slotsInOrder = slotsInOrder.OrderBy(
            x => Vector2.Distance(unit.transform.position, x.transform.position)
           ).ToList();
        for (int i = 0; i < slotsInOrder.Count; i++)
        {
            // 2- Find a slot empty
            if (!slotsInOrder[i].queue.IsUnitInQueue())
            {
                selectedSlot = slotsInOrder[i];
                break;
            }
            else
            {
                // 3- No empty slot, add to the queue with the less worker on
                if (numberOfUnitAlreadyIn > slotsInOrder[i].queue.GetQueueLength())
                {
                    indexEmptyiestSlot = i;
                    numberOfUnitAlreadyIn = slotsInOrder[i].queue.GetQueueLength();
                }
            }
        }
        if (selectedSlot == null)
        {
            if (indexEmptyiestSlot != -1)
            {
                selectedSlot = slotsInOrder[indexEmptyiestSlot];
            }
            else
            {
                // 4-No best queue found, add to the first one
                selectedSlot = slotsInOrder[0];
            }
        }
        return selectedSlot;
    }

    public void RemoveFirstUnitToWorkerQueue(Unit unit)
    {
        for (int i = 0; i < workerSlots.Count; i++)
        {
            if (workerSlots[i].queue.IsUnitInQueue(unit))
            {
                workerSlots[i].queue.DequeueUnit();
                break;
            }
        }
    }

    #endregion

    #region Construction queue

    protected void CreateUnit(CustomActionName unitName)
    {
        if (buildQueue.Count < DataManager.MAX_UNIT_PER_QUEUE)
        {
            GameObject unit = ResourceManager.GetUnit(unitName);
            Unit unitObject = unit.GetComponent<Unit>();
            if (player && unitObject)
            {
                bool canCreate = player.RemoveResources(unitObject.ChooseObjectStat().GetCosts());
                if (canCreate)
                {
                    buildQueue.Enqueue(new BuildObject(unitName.ToString(), unitObject.image, BuildType.UNIT));
                }
            }
        }
    }

    protected void CreateUpgrade(Upgrades upgrade)
    {
        if (buildQueue.Count < DataManager.MAX_UNIT_PER_QUEUE)
        {
            if (player)
            {
                bool canCreate = player.RemoveResources(upgrade.GetCurrentUpgradeAction().upgradeCost);
                if (canCreate)
                {
                    buildQueue.Enqueue(new BuildObject(upgrade.upgradeType.ToString(), upgrade.GetCurrentUpgradeAction().action.GetImage(), BuildType.UPGRADE));
                    sounds.PlaySound(this, audioSource, SoundCategorie.RESEARCH);
                }
            }
        }
    }

    protected void ProcessConstructionQueue()
    {
        if (buildQueue != null && buildQueue.Count > 0)
        {
            BuildObject buildObject = buildQueue.Peek();

            if (BuildType.UNIT.Equals(buildObject.type))
            {
                if (objCurrentlyConstructed == null)
                {
                    Enum.TryParse(buildObject.buildName, out CustomActionName myStatus);
                    SelectableObject currentBuild = ResourceManager.GetUnit(myStatus).GetComponent<SelectableObject>();
                    objCurrentlyConstructed = currentBuild;
                    currentBuildCreationTime = currentBuild.objectStats.GetBuildTime();
                }
            } else if (BuildType.UPGRADE.Equals(buildObject.type))
            {
                if (upgradeCurrentlyConstructed == null)
                {
                    // upgradeCurrentlyConstructed = ResourceManager.GetUpgrade(buildObject.buildName);
                    Enum.TryParse(buildObject.buildName, out UpgradeType myStatus);
                    upgradeCurrentlyConstructed = player.GetUpgradeStatus(myStatus);

                    currentBuildCreationTime = upgradeCurrentlyConstructed.GetCurrentUpgradeAction().upgradeTime;
                }
            }

            currentBuildProgress += Time.deltaTime * ResourceManager.BuildSpeed;
            buildObject = buildQueue.Peek();
            if (BuildType.UNIT.Equals(buildObject.type))
            {
                if (currentBuildProgress > objCurrentlyConstructed.objectStats.GetBuildTime())
                {
                    if (player)
                    {
                        objCurrentlyConstructed = null;
                        Vector3 originPoint = rallyPoint.transform.position;
                        originPoint.x += UnityEngine.Random.Range(-rallyPointRadius, rallyPointRadius);
                        originPoint.z += UnityEngine.Random.Range(-rallyPointRadius, rallyPointRadius);
                        Enum.TryParse(buildQueue.Dequeue().buildName, out CustomActionName customActionName);
                        player.AddUnit(customActionName, spawnPoint.transform.position, originPoint, transform.rotation, this);
                        /*if (audioElement != null) audioElement.Play(finishedJobSound);*/
                        currentBuildProgress = 0.0f;
                    }
                }
            }
            else if (BuildType.UPGRADE.Equals(buildObject.type))
            {
                if (currentBuildProgress > upgradeCurrentlyConstructed.GetCurrentUpgradeAction().upgradeTime)
                {
                    buildQueue.Dequeue();
                    player.MakeUpgrade(upgradeCurrentlyConstructed);
                    upgradeCurrentlyConstructed = null;
                    InitActions();
                    currentBuildProgress = 0.0f;
                    sounds.PlaySound(this, audioSource, SoundCategorie.RESEARCH_COMPLETE);
                }
            }
                
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (rallyPoint)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(rallyPoint.transform.position, rallyPointRadius);
        }
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, GetStats().weaponRange);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, GetStats().detectionRange);
    }

    public void RemoveFromConstructionQueue(int removePosition)
    {
        BuildObject[] objectsToBuild = buildQueue.ToArray();
        ListQueue<BuildObject> newQueue = new ListQueue<BuildObject>();
        if (removePosition > objectsToBuild.Length)
            return;
        List<Cost> deleted = null;
        for (int i = 0; i < objectsToBuild.Length; i++)
        {
            if (i != removePosition)
            {
                newQueue.Enqueue(objectsToBuild[i]);
            } else
            {
                if (BuildType.UNIT.Equals(objectsToBuild[i].type))
                {
                    Enum.TryParse(objectsToBuild[i].buildName, out CustomActionName myStatus);
                    deleted = ResourceManager.GetUnit(myStatus).GetComponent<SelectableObject>().ChooseObjectStat().GetCosts();
                }
                else if (BuildType.UPGRADE.Equals(objectsToBuild[i].type))
                {
                    Enum.TryParse(objectsToBuild[i].buildName, out UpgradeType myStatus);
                    deleted = player.GetUpgradeStatus(myStatus).GetCurrentUpgradeAction().upgradeCost;
                    upgradeCurrentlyConstructed = null;
                }
            }
        }
        if (deleted != null)
        {
            player.AddResources(deleted);
            Cost cost = Array.Find(deleted.ToArray(), (Cost c) => c.type == ResourceType.FOOD);
            if (cost != null)
                player.RemoveConsummedFood(cost.amount);
            // objCurrentlyConstructed = null;
        }
        buildQueue = newQueue;
        if (removePosition == 0)
        {
            currentBuildProgress = 0.0f;
        }
    }

    public override void SetMaterials()
    {
        base.SetMaterials();
        SetMaterials(player);
    }

    public void SetMaterials(Player player)
    {
        Material material = ColorManager.getMaterial(player.color, DataManager.BUILDING_MATERIAL);
        if (partsToColor != null && partsToColor.Count > 0)
        {
            foreach (Renderer r in partsToColor)
            {
                r.material = material;
            }
        }
    }

    public void SetCurrentlyPlaced(bool v)
    {
        isCurrentlyPlacingOnField = v;
    }

    public bool IsCurrentlyPlacingOnField()
    {
        return isCurrentlyPlacingOnField;
    }

    #endregion

    #region This building creation process

    public void StartConstruction()
    {
        // isCurrentlyPlacingOnField = true;
        SetPlaceableMarker(false);
        SetObstacle(true);
        CalculateBounds();
        ChangeBuildingBanner();
        isUnderConstruction = true;
        currentConstructionTime = 0f;
        ChangeBuildingShape();
        hitPoints = 1;
    }

    public void Construct(float amount)
    {
        currentConstructionTime += amount;


        if (currentShape < constructionSteps.Count - 2) // Last step is when the building is over only
        {
            float stepSize = (float)(currentShape + 1) / (constructionSteps.Count - 1);
            float percentageComplete = currentConstructionTime / objectStats.GetBuildTime();

            if (percentageComplete > stepSize)
            {
                currentShape++;

                if (currentShape == constructionSteps.Count)
                    currentShape -= 1;
                ChangeBuildingShape();
            }
        }

        // Building constructed
        if (currentConstructionTime >= objectStats.GetBuildTime())
        {
            sounds.PlaySound(audioSource, SoundCategorie.BUILDING_CONSTRUCTED);
            currentShape = constructionSteps.Count - 1;
            isUnderConstruction = false;
            player.UpdateMaxAllowedFood();
            ChangeBuildingShape();
            ChangeBuildingBanner();

            // hitPoints = GetStats().maxHitPoint;
            if (currentlySelected)
            {
                player.UpdateUISquad();
            }
        }
        else
        {
            // hitPoints = (int) (currentConstructionTime * GetStats().maxHitPoint / objectStats.buildTime);
            hitPoints += (float)amount * GetMaxHitPoints() / objectStats.GetBuildTime();
            if (hitPoints > GetMaxHitPoints())
                hitPoints = GetMaxHitPoints();
        }
    }

    public void Repair(float amount)
    {
        Heal(((float)amount * GetMaxHitPoints() / objectStats.GetBuildTime()));
        if (hitPoints > GetMaxHitPoints())
            hitPoints = GetMaxHitPoints();
    }

    private void ChangeBuildingBanner()
    {
        ChangeBuildingBanner(player);
    }

    public void ChangeBuildingBanner(Player p)
    {
        if (rallyPoint != null)
        {
            Transform transform = rallyPoint.transform;
            Quaternion rotation = rallyPoint.transform.rotation;
            Destroy(rallyPoint);
            rallyPoint = Instantiate(ColorManager.getBanner(p.color), transform.position, rotation, this.transform);
            if (!currentlySelected)
            {
                rallyPoint.SetActive(false);
            }
            rallyPointAnimator = rallyPoint.GetComponent<Animator>();
        }
    }

    private void ChangeBuildingShape()
    {
        int children = shape.transform.childCount;
        Vector3 position = ResourceManager.InvalidPosition;
        Quaternion quaternion = Quaternion.identity;
        Vector3 scale = ResourceManager.InvalidPosition;
        for (int i = 0; i < children; ++i)
        {
            position = shape.transform.GetChild(i).gameObject.transform.position;
            quaternion = shape.transform.GetChild(i).gameObject.transform.rotation;
            scale = shape.transform.GetChild(i).gameObject.transform.localScale;
            partsToColor.Remove(shape.transform.GetChild(i).gameObject.GetComponent<Renderer>());
            Destroy(shape.transform.GetChild(i).gameObject);
        }
        GameObject go = Instantiate(constructionSteps[currentShape], position, quaternion, shape.transform);
        go.transform.localScale = scale;
        partsToColor.Add(go.GetComponent<Renderer>());
        
        SetMaterials();
    }

    #endregion

    #region This building upgrade process

    public void UpgradeBuilding()
    {
        Building b = upgradedBuilding.GetComponent<Building>();
        if (player.RemoveResources(b.objectStats.GetCosts())) {
            upgradeTime = b.ChooseObjectStat().GetBuildTime();
            upgradeImage = b.image;
            currentUpgradeTime = 0f;
            isUpgradingBuilding = true;
            sounds.PlaySound(this, audioSource, SoundCategorie.UPGRADING);
        }
    }

    private void Upgrade()
    {
        currentUpgradeTime += Time.deltaTime;

        // Building constructed
        if (currentUpgradeTime >= upgradeTime)
        {
            GameObject go = Instantiate(upgradedBuilding, transform.position, transform.rotation, player.GetBuildingsTransform());
            Building b = go.GetComponent<Building>();
            b.SetPlayer(player);
            b.hitPoints = hitPoints * b.GetStats().maxHitPoint / GetStats().maxHitPoint;
            b.ChangeBuildingBanner();
            b.rallyPoint.transform.position = rallyPoint.transform.position;
            // b.SetRallyPoint(rallyPoint.transform.position);

            player.RemoveBuilding(this);
            b.PlaySound(SoundCategorie.UPGRADE_COMPLETE, false, true);
            UIManager.replaceUnitFromUI(this, b);
            if (!player.human && this.IsCurrentlySelected())
            {
                player.squad.RemoveSquadUnit(this);
            }

            Destroy(this.gameObject);
        }
    }

    public void CancelUpgrade()
    {
        this.currentUpgradeTime = 0f;
        this.isUpgradingBuilding = false;

        Building b = upgradedBuilding.GetComponent<Building>();
        player.AddResources(b.ChooseObjectStat().GetCosts());
    }

    #endregion

    #region Creation building position

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag(DataManager.GROUND_TAG))
        {
            colliders.Add(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag(DataManager.GROUND_TAG))
            colliders.Remove(other);
    }

    #endregion
}