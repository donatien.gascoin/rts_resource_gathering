﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[System.Serializable]
public class Squad
{

    public int maximumUnitPerSelection;
    public List<SelectableObject> selectableObjects;
    public List<SelectableObject> unitsSelectedInUI;
    private Player player;
    public BasicFormations formations;
    private bool isNewSelection;

    public Behaviour nextClickBehaviour;

    public Squad(Player player)
    {
        selectableObjects = new List<SelectableObject>();
        unitsSelectedInUI = new List<SelectableObject>();
        maximumUnitPerSelection = DataManager.MAX_UNIT_PER_SELECTION;
        this.player = player;
        formations = new BasicFormations();
        nextClickBehaviour = Behaviour.NORMAL;
    }

    #region Default Squad methods

    public List<SelectableObject> GetSquadUnits()
    {
        return selectableObjects;
    }

    public SelectableObject GetFirstSquadUnit()
    {
        return selectableObjects.Count > 0 ? selectableObjects[0] : null;
    }

    public SelectableObject GetFirstSquadUnitInUnitsSelectedOnUI()
    {
        if (GetSquadSize() == 1)
            return GetFirstSquadUnit();
        return unitsSelectedInUI.Count > 0 ? unitsSelectedInUI[0] : null;
    }

    public void SetUnitsSelectedInUI(List<SelectableObject> unitsSelectedInUI)
    {
        this.unitsSelectedInUI = unitsSelectedInUI;
    }

    public List<SelectableObject> GetUnitsSelectedInUI()
    {
        return this.unitsSelectedInUI;
    }

    public void UseFirstUnitToSelectedUnitsOnUI()
    {
        unitsSelectedInUI.Clear();
        unitsSelectedInUI.Add(GetFirstSquadUnit());
    }

    public Behaviour GetNextBehaviour()
    {
        return nextClickBehaviour;
    }

    public SelectableObject GetLastSquadUnit()
    {
        if (selectableObjects.Count == 0)
            return null;
        return selectableObjects[selectableObjects.Count - 1];
    }

    public void SetSquadUnits(List<SelectableObject> newUnits)
    {
        RemoveAllSquadUnits();
        for (int i =0; i < newUnits.Count; i++)
        {
            AddSquadUnit(newUnits[i]);
        }
        selectableObjects.Sort(new SquadComparer());
        if (GetSquadSize() > 0)
        {
            isNewSelection = true;
        } else
        {
            isNewSelection = false;
        }
        nextClickBehaviour = Behaviour.NORMAL;
        SelectableObject obj = GetFirstSquadUnit();
        if (obj != null)
        {
            Unit u = obj as Unit;
            if (u != null && IsUnitSelectedAHumanPlayerUnit())
                u.PlaySound(SoundCategorie.HELLO, false);
        }
        // ChangeSquadBehaviour(Behaviour.NORMAL);
    }

    public void AddSquadUnit(SelectableObject unit)
    {
        if (maximumUnitPerSelection > selectableObjects.Count && !selectableObjects.Contains(unit))
        {
            selectableObjects.Add(unit);
            unit.SetSelection(true);
            selectableObjects.Sort(new SquadComparer());
            nextClickBehaviour = Behaviour.NORMAL;
            // ChangeSquadBehaviour(Behaviour.NORMAL);
        }
    }

    public int GetSquadSize()
    {
        return selectableObjects != null ? selectableObjects.Count : 0;
    }

    public void RemoveSquadUnit(SelectableObject unit)
    {
        if (unit != null)
        {
            unit.SetSelection(false);
            selectableObjects.Remove(unit);
            if (unitsSelectedInUI.Contains(unit))
            {
                unitsSelectedInUI.Remove(unit);
            }
            selectableObjects.Sort(new SquadComparer());
            nextClickBehaviour = Behaviour.NORMAL;
            // ChangeSquadBehaviour(Behaviour.NORMAL);
        }
    }

    public bool IsNewSelection()
    {
        return isNewSelection;
    }

    public bool IsUnitOnSquad(SelectableObject obj)
    {
        return GetSquadUnits().Contains(obj);
    }

    public void CheckNewSelection()
    {
        isNewSelection = false;
    }

    public SelectableObject RemoveSquadUnitAtPosition(int removePosition)
    {
        int size = GetSquadSize();
        SelectableObject deleted = null;
        if (removePosition > size)
            return deleted;
        
        for (int i = 0; i < size; i++)
        {
            if (i == removePosition)
            {
                deleted = selectableObjects[i];
                RemoveSquadUnit(selectableObjects[i]);
            }
        }
        nextClickBehaviour = Behaviour.NORMAL;
        // ChangeSquadBehaviour(Behaviour.NORMAL);
        return deleted;
    }

    public SelectableObject SelectUnitAtPosition(int position)
    {
        int size = GetSquadSize();
        SelectableObject obj = null;
        if (position > size)
            return obj;
        for (int i = 0; i < size; i++)
        {
            if (i == position)
            {
                obj = selectableObjects[i];
            }
        }
        RemoveAllSquadUnits();
        AddSquadUnit(obj);
        nextClickBehaviour = Behaviour.NORMAL;
        // ChangeSquadBehaviour(Behaviour.NORMAL);
        return obj;
    }

    public void RemoveAllSquadUnits()
    {
        for (int i = (selectableObjects.Count -1); i >= 0; i--)
        {
            if (selectableObjects[i] == null) //Unit Died
                continue;
            RemoveSquadUnit(selectableObjects[i]);
        }
        selectableObjects.Clear();
        nextClickBehaviour = Behaviour.NORMAL;
        // ChangeSquadBehaviour(Behaviour.NORMAL);
        isNewSelection = true;
    }

    public void ReplaceSquadUnit(SelectableObject oldObj, SelectableObject newObj)
    {
        for (int i = 0; i < selectableObjects.Count; i++)
            if (selectableObjects[i].ObjectId == oldObj.ObjectId)
            {
                selectableObjects[i] = newObj;
                oldObj.SetSelection(false);
                newObj.SetSelection(true);
            }

        for (int i = 0; i < unitsSelectedInUI.Count; i++)
            if (unitsSelectedInUI[i].ObjectId == oldObj.ObjectId)
                unitsSelectedInUI[i] = newObj;
    }

    public bool IsUnitSelectedAHumanPlayerUnit()
    {
        SelectableObject so = GetFirstSquadUnit();
        if (so != null)
            return so.player == player;
        return false;
    }

    public void SelectThisTypeOfUnit(SelectableObject u)
    {
        List<SelectableObject> newUnits = new List<SelectableObject>();

        for (int i = 0; i < selectableObjects.Count; i++)
        {
            if (u.objectName.Equals(selectableObjects[i].objectName))
            {
                newUnits.Add(selectableObjects[i]);
            }
            else
            {
                RemoveSquadUnit(selectableObjects[i]);
            }
        }
        SetSquadUnits(newUnits);
    }

    public void SelectThisTypeOfUnit(int position)
    {
        if (position > GetSquadSize())
            return;
        SelectableObject obj = null;
        for (int i = 0; i < GetSquadSize(); i++)
        {
            if (i == position)
                obj = selectableObjects[i];
        }
        if(obj)
            SelectThisTypeOfUnit(obj);
    }

    public Dictionary<int, List<SelectableObject>> GetSquadUnitsByUiNumber()
    {
        Dictionary<int, List<SelectableObject>> unitsByUiNumber = new Dictionary<int, List<SelectableObject>>();

        int index = -1;
        int currentUINumber = -1;
        string name = "";
        foreach (SelectableObject unit in selectableObjects)
        {
            if (name != unit.objectName || currentUINumber != unit.uiNumber)
            {
                index++;
                currentUINumber = unit.uiNumber;
                name = unit.objectName;
                unitsByUiNumber.Add(index, new List<SelectableObject>());
            }
            unitsByUiNumber[index].Add(unit);
        }
        return unitsByUiNumber;
    }

    public void ExecuteCustomAction(CustomActionName actionName, Vector3 hitPoint, GameObject hitObject, UserAction action)
    {
        List<SelectableObject> objs = new List<SelectableObject>();
        if (UserAction.SQUAD_ACTION.Equals(action))
            objs = selectableObjects;
        else
            objs = unitsSelectedInUI;
        bool unitFound = false;
        for(int i = 0; i < objs.Count; i++)
        {
            if (objs[i] == null)
                continue;
            if (objs[i].CanDoAction(actionName))
            {
                objs[i].ExecuteAction(actionName, hitPoint, hitObject);
                unitFound = true;
                if (UserAction.SAME_SQUAD_UNITS.Equals(action))
                    break;
            }
        }

        if (!unitFound)
        {
            // All units are busy. As it's an order, the closest one will stop doing his action and go to the new one
            SelectableObject obj = WorkManager.FindNearestWorldObjectInListToPosition(unitsSelectedInUI, hitPoint);
            obj.ExecuteAction(actionName, hitPoint, hitObject);
        }
        // ChangeSquadBehaviour(Behaviour.NORMAL);
        nextClickBehaviour = Behaviour.NORMAL;
        player.StopCustomAction();
    }

    public void PerformRightClickOnAction(CustomActionName actionName)
    {
        for (int i = 0; i < unitsSelectedInUI.Count; i++)
        {
            if (unitsSelectedInUI[i] == null)
                continue;
            unitsSelectedInUI[i].ExecuteRightClickOnAction(actionName);
        }
    }

    internal void PerformLeftClickOnAction(CustomActionName actionName)
    {
        if (GetSquadSize() == 0)
            return;

        for (int i = 0; i < selectableObjects.Count; i++)
        {
            if (selectableObjects[i] == null)
                continue;
            selectableObjects[i].PerformLeftClickAction(actionName);
        }
        if (nextClickBehaviour != GetFirstSquadUnit().behaviour)
            ChangeSquadNextClickBehaviour(GetFirstSquadUnit().behaviour);
    }

    private List<SelectableObject> GetUnitsWithType(AttackType type)
    {
        List<SelectableObject> unitsWithType = new List<SelectableObject>();
        for (int i = 0; i < selectableObjects.Count; i++)
        {
            if (type.Equals(selectableObjects[i].GetComponent<SelectableObject>().GetAttackType()))
            {
                unitsWithType.Add(selectableObjects[i]);
            }
        }
        return unitsWithType;
    }

    private List<SelectableObject> GetUnitsWithTypeDifferentOf(AttackType type)
    {
        List<SelectableObject> othersUnit = new List<SelectableObject>();
        for (int i = 0; i < selectableObjects.Count; i++)
        {
            if (!type.Equals(selectableObjects[i].GetComponent<SelectableObject>().GetAttackType()))
            {
                othersUnit.Add(selectableObjects[i]);
            }
        }
        return othersUnit;
    }

    public bool isAlreadySelected(SelectableObject obj)
    {
        return selectableObjects.Contains(obj);
    }

    #endregion

    #region Unit Squad movement

    public float GetSquadSpeed()
    {
        if (selectableObjects != null && selectableObjects.Count > 0)
        {
            float speed = selectableObjects[0].GetComponent<NavMeshAgent>().speed;
            for (int i = 0; i < selectableObjects.Count; i++)
            {
                if (selectableObjects[i].GetComponent<NavMeshAgent>().speed < speed)
                {
                    speed = selectableObjects[i].GetComponent<NavMeshAgent>().speed;
                }
            }
            return speed;
        }
        Debug.LogError("Squad is empty, this method should not be caled");
        return 10;
    }

    public void RightClick(Vector3 point, GameObject obj, Behaviour behaviour)
    {
        if (player && player.human && selectableObjects != null && selectableObjects.Count > 0 && CanMove())
        {
            if (!WorkManager.ObjectIsGround(obj) || selectableObjects.Count == 1)
            {
                /*float speed = GetSquadSpeed();*/
                for (int i = 0; i < selectableObjects.Count; i++)
                {
                    selectableObjects[i].MouseClick(obj, point, player, behaviour);
                }
            } else
            {
                List<SelectableObject> meleeUnits = GetUnitsWithType(AttackType.MELEE);
                // Add units to formations : MAGIC / SIEGE / RANGED
                List<SelectableObject> rangedUnits = GetUnitsWithTypeDifferentOf(AttackType.MELEE);

                Vector3 center = GetSquadCenter();
                float angle = Quaternion.LookRotation(point - center).eulerAngles.y;
                if (angle > 180f) angle -= 360f;

                Vector3[] unitsPos = GetSquadFuturPosition(meleeUnits, rangedUnits);
                int count = 0;
                for (int j = 0; j < meleeUnits.Count; j++)
                {
                    if (meleeUnits[j] != null)
                    {
                        Vector3 startVector = GetUnitPositionWithAngle(unitsPos[count], angle);
                        meleeUnits[j].MouseClick(obj, point + startVector, player, behaviour);
                        count++;
                    }
                }
                for (int j = 0; j < rangedUnits.Count; j++)
                {
                    if (rangedUnits[j] != null)
                    {
                        Vector3 startVector = GetUnitPositionWithAngle(unitsPos[count], angle);
                        rangedUnits[j].MouseClick(obj, point + startVector, player, behaviour);
                        count++;
                    }
                }
            }
        }
        nextClickBehaviour = Behaviour.NORMAL;
        // ChangeSquadBehaviour(Behaviour.NORMAL);
    }

    private Vector3[] GetSquadFuturPosition(List<SelectableObject> meleeUnits, List<SelectableObject> rangedUnits)
    {
        int nbUnit = meleeUnits.Count + rangedUnits.Count;
        Vector3[] positions;
        switch (nbUnit)
        {
            case 1:
                positions = new Vector3[1];
                positions[0] = Vector3.zero;
                break;
            case 2:
                if (meleeUnits.Count > 1 || rangedUnits.Count > 1)
                {
                    positions = formations.TwoSameUnitsFormation();
                }
                else
                {
                    positions = formations.TwoDiffrentUnitsFormation();
                }
                break;
            case 3:
                if (meleeUnits.Count == 1)
                {
                    positions = formations.TwoRangedUnitsAndOneMeleeUnitFormation();
                }
                else if (rangedUnits.Count == 1)
                {
                    positions = formations.OneRangedUnitAndTwoMeleeUnitsFormation();
                }
                else
                {
                    positions = formations.ThreeSameUnitsFormation();
                }
                break;
            case 4:
                if (meleeUnits.Count == 1)
                {
                    positions = formations.OneMeleeUnitAndThreeRangedUnitsFormation();
                }
                else if (rangedUnits.Count == 1)
                {
                    positions = formations.ThreeMeleeUnitsAndOneRangedUnitFormation();
                }
                else
                {
                    positions = formations.FourSameUnitsFormation();
                }
                break;
            case 5:
                if (meleeUnits.Count == 1)
                {
                    positions = formations.OneMeleeUnitAndFourRangedUnitsFormation();
                }
                else if (meleeUnits.Count == 2)
                {
                    positions = formations.TwoMeleeUnitsAndThreeRangedUnitsFormation();
                }
                else if (rangedUnits.Count == 1)
                {
                    positions = formations.FourMeleeUnitsAndOneRangedUnitFormation();
                }
                else
                {
                    positions = formations.FiveSameUnitsFormation();
                }
                break;
            case 6:
                if (meleeUnits.Count == 2)
                {
                    positions = formations.TwoMeleeUnitsAndFourRangedUnitsFormation();
                }
                else if (rangedUnits.Count == 2)
                {
                    positions = formations.FourMeleeUnitsAndTwoRangedUnitsFormation();
                }
                else
                {
                    positions = formations.SixSameUnitsFormation();
                }
                break;
            case 7:
                positions = formations.SevenSameUnitsFormation();
                break;
            case 8:
                positions = formations.HeightSameUnitsFormation();
                break;
            case 9:
                positions = formations.NineSameUnitsFormation();
                break;
            default:
                positions = formations.TwelveSameUnitsFormation();
                break;
        }
        return positions;
    }

    /**
     * Allow the pointer manager to adapt depending of the next click
     */
    public void ChangeSquadNextClickBehaviour(Behaviour behaviour)
    {
        switch(behaviour)
        {
            case Behaviour.NORMAL:
            case Behaviour.AGGRESSIVE:
                if (GetSquadSize() > 0 && IsUnitSelectedAHumanPlayerUnit() && CanMove())
                    nextClickBehaviour = behaviour;
                break;
        }
        // SetBehaviourToSquad(behaviour);
    }

    public void SetBehaviourToSquad(Behaviour behaviour)
    {
        for (int i = 0; i < GetSquadSize(); i++)
        {
            selectableObjects[i].SetBehaviour(behaviour);
        }
    }

    private Vector3 GetSquadCenter()
    {
        List<SelectableObject> units = GetSquadUnits();
        Vector3 centerPoint = Vector3.zero;
        for (int i = 0; i < units.Count; i++)
        {
            if (units[i] != null)
            {
                centerPoint += units[i].transform.position;
            }
        }
        return centerPoint / units.Count;  //Center point of grouped units 
    }

    public static Vector3 GetUnitPositionWithAngle(Vector3 unitPos, float rotationAngle)
    {
        Vector3 vector = Quaternion.AngleAxis(rotationAngle, Vector3.up) * unitPos;
        // vector.y = 0;
        return vector;
    }

    public bool CanMove()
    {
        if (selectableObjects.Count == 0)
            return false;
        return GetFirstSquadUnit().CompareTag(DataManager.UNIT_TAG) ? true : false;
    }

    #endregion


}

public class SquadComparer : IComparer<SelectableObject>
{
    public int Compare(SelectableObject y, SelectableObject x)
    {
        if (x == null || y == null)
        {
            return 0;
        }

        if (x == null)
        {
            if (y == null)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }
        else
        {
            if (y == null)
            {
                return 1;
            }
            else
            {
                // Less uiNumber, more priority for the object
                if (x.uiNumber < y.uiNumber)
                {
                    return 1;
                }
                else if (x.uiNumber > y.uiNumber)
                {
                    return -1;
                }
                // uiNumber equal, sort by object name
                return x.objectName.CompareTo(y.objectName);
            }
        }
    }
}