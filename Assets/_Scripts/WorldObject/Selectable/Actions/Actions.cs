﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Actions : MonoBehaviour
{
    private CustomActionObject[] actions;

    private void Awake()
    {
        actions = GetComponentsInChildren<CustomActionObject>();
    }

    public CustomActionObject[] GetActions()
    {
        CustomActionObject[] objs = new CustomActionObject[DataManager.MAX_ACTIONS];
        if (actions == null || actions.Length == 0)
            return objs;
        for (int i = 0; i < objs.Length; i++)
        {
            for (int k = 0; k < actions.Length; k++)
            {
                if (!actions[k].hideAction && (actions[k].position - 1) == i)
                {
                    objs[i] = actions[k];
                    break;
                }
            }
        }
        return objs;
    }
}

