﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuraActionObject : SpellActionObject
{
    private AuraAction auraAction;

    public override void DoActionAutomatically()
    {
        if (coolDownTimer >= auraAction.coolDown)
        {
            coolDownTimer = 0f;
            List<SelectableObject> objs = WorkManager.FindNearbyObjects(transform.position, auraAction.effectRange);

            if (objs != null && objs.Count > 0)
            {
                for (int i = 0; i < objs.Count; i++)
                {
                    ExecuteAction(objs[i]);
                }
            }

        } else
        {
            coolDownTimer += Time.deltaTime;
        }
    }

    protected override void Update()
    {
        if (doAction)
        {
            DoActionAutomatically();
        }
    }

    public override void SetupAction(SelectableObject obj)
    {
        base.SetupAction(obj);
        this.doAction = true;
        auraAction = action as AuraAction;
    }

    public override void ExecuteAction(SelectableObject obj)
    {
        if (auraAction.applyEffectOnlyOnAlly && obj.IsSameTeam(userObject))
        {
            obj.AddBonus(new Bonus(bonus));
        }
        else if (auraAction.applyEffectOnlyOnEnemy && !obj.IsSameTeam(userObject))
        {
            obj.AddBonus(new Bonus(bonus));
        }
        else if (auraAction.applyEffectOnlyOnAlly && auraAction.applyEffectOnlyOnEnemy)
        {
            obj.AddBonus(new Bonus(bonus));
        }
    }

    private void OnDrawGizmos()
    {
        if (auraAction != null)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, auraAction.effectRange);
        }
    }
}
