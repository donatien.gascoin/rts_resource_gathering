﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Aura Action", menuName = "Selectable object/Aura action")]
public class AuraAction : SpellAction
{
    public float effectRange;

    public bool applyEffectOnlyOnAlly;

    public bool applyEffectOnlyOnEnemy;

    public List<UnitType> affectedUnits;
}
