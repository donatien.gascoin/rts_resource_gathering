﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Object stat", menuName = "Selectable object/Spell action")]
public class SpellAction : CustomAction
{
    public int manaRequired;
    public float coolDown;
}
