﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellActionObject : CustomActionObject
{

    protected override void Update()
    {
        base.Update();
        if (automaticAction)
        {
            DoActionAutomatically();
        }
    }
    public int GetManaRequired ()
    {
        SpellAction sa = action as SpellAction;
        if (sa != null)
            return sa.manaRequired;
        return -1;
    }

    public float GetCoolDown()
    {
        SpellAction sa = action as SpellAction;
        if (sa != null)
            return sa.coolDown;
        return -1;
    }

    public override bool CanDoAction()
    {
        return coolDownTimer >= GetCoolDown();
    }

    public float GetCoolDownPercentage()
    {
        return coolDownTimer / (float)GetCoolDown();
    }
}
