﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealActionObject : SpellActionObject
{
    private SpellAction spellAction;
    private Priest userPriest;

    public override void DoActionAutomatically()
    {
        /*List<SelectableObject> objs = WorkManager.FindNearbyObjects(transform.position, userObject.GetStats().detectionRange);

        if (objs != null && objs.Count > 0)
            for (int i = 0; i < objs.Count; i++)
                if (!objs[i].IsSameTeam(userObject) && CanExecuteAction(objs[i]))
                    userObject.ExecuteAction(CustomActionName.SLOW_SPEED, objs[i].transform.position, objs[i].gameObject);*/
        // ExecuteAction(objs[i]);
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void SetupAction(SelectableObject obj)
    {
        base.SetupAction(obj);
        if (userPriest == null)
            userPriest = userObject as Priest;

        spellAction = action as SpellAction;
        coolDownTimer = GetCoolDown();
    }

    private bool CanExecuteAction(SelectableObject obj)
    {
        return coolDownTimer >= spellAction.coolDown && !obj.HaveBonus(bonus.bonusEffect);
    }

    public override void ExecuteAction(SelectableObject obj)
    {
        if (CanExecuteAction(obj) && userObject.ConsumeMana(spellAction.manaRequired))
        {
            Debug.Log("Execute action !");
            base.ExecuteAction(obj);
            obj.AddBonus(new HealBonus(bonus));
            coolDownTimer = 0;
        }
    }

    protected override IEnumerator DoActionFx()
    {
        if (userPriest == null)
            userPriest = userObject as Priest;
        GameObject gfx = Instantiate(GetActionFx(), userPriest.projectileSlot.transform.position, userPriest.projectileSlot.transform.rotation, userPriest.projectileSlot.transform);
        yield return new WaitForSeconds(GetActionFxDuration());
        Destroy(gfx);
    }

    public override Texture2D ManagerPointer(GameObject obj, Player player)
    {
        Unit unit = obj.GetComponent<Unit>();
        if (unit == null || /*unit.HaveBonus(BonusEffects.HEAL) ||*/unit.hitPoints >= unit.GetStats().maxHitPoint || unit.player.team != player.team)
            return action.actionPointer;

        return action.actionPointerValidate;
    }
}
