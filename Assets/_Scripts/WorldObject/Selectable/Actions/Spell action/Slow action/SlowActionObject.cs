﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowActionObject : SpellActionObject
{
    private SpellAction spellAction;
    private Wizard userWizard;
    public override void DoActionAutomatically()
    {
        List<SelectableObject> objs = WorkManager.FindNearbyObjects(transform.position, userObject.GetStats().detectionRange);

        if (objs != null && objs.Count > 0)
            for (int i = 0; i < objs.Count; i++)
                if (!objs[i].IsSameTeam(userObject) && CanExecuteAction(objs[i]))
                    userObject.ExecuteAction(CustomActionName.SLOW_SPEED, objs[i].transform.position, objs[i].gameObject);
                    // ExecuteAction(objs[i]);
    }

    protected override void Update()
    {
        base.Update();
        /*if (automaticAction) {
            DoAction();
        }*/
    }

    public override void SetupAction(SelectableObject obj)
    {
        base.SetupAction(obj);
        spellAction = action as SpellAction;
        coolDownTimer = GetCoolDown();
    }

    private bool CanExecuteAction(SelectableObject obj)
    {
        return coolDownTimer >= spellAction.coolDown && !obj.HaveBonus(bonus.bonusEffect);
    }

    public override void ExecuteAction(SelectableObject obj)
    {
        if (CanExecuteAction(obj) && userObject.ConsumeMana(spellAction.manaRequired))
        {
            Debug.Log("Execute action !");
            base.ExecuteAction(obj);
            obj.AddBonus(new SlowBonus(bonus));
            coolDownTimer = 0;
        }
    }

    protected override IEnumerator DoActionFx()
    {
        if (userWizard == null)
            userWizard = userObject as Wizard;
        GameObject go = Instantiate(GetActionFx(), userWizard.projectileSlot.transform.position, userWizard.projectileSlot.transform.rotation, userWizard.projectileSlot.transform);
        yield return new WaitForSeconds(GetActionFxDuration());
        Destroy(go);
    }

    public override Texture2D ManagerPointer(GameObject obj, Player player)
    {
        Unit unit = obj.GetComponent<Unit>();
        if (unit == null || unit.HaveBonus(BonusEffects.SLOW_SPEED) /*|| unit.player.team == player.team*/)
            return action.actionPointer;

        return action.actionPointerValidate;
    }
}
