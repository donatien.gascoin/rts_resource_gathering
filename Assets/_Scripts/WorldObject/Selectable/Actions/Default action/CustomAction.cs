﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Object stat", menuName = "Selectable object/Action")]
public class CustomAction : ScriptableObject
{
    public string actionName;

    public CustomActionName actionNameData;

    public Sprite image;

    public ActionType type;

    [TextArea(3, 6)]
    public string description;

    public List<CustomActionName> requirements;

    public KeyCode shortcut;

    public GameObject actionFx;
    public float actionFxDuration;

    [Tooltip("Amount of time needed to do the action, the unit will ot be able to do anything else automatically during this time.")]
    public float actionDuration;

    public Texture2D actionPointer;
    public Texture2D actionPointerValidate;
    public Texture2D actionPointerError;

    public AudioClip audioClip;
}

[System.Serializable]
public enum ActionType
{
    CREATE_UNIT, CREATE_BUILDING, OTHER
}