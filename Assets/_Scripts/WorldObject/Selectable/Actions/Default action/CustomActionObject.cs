﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class CustomActionObject : MonoBehaviour
{
    public CustomAction action;

    public bool disableAction;

    [HideInInspector]
    public bool isDoingThisAction;

    public bool automaticAction;

    public bool hideAction;

    [Tooltip("Handle action will call DoAction by default when the action is instantiated")]
    public bool handleAction;

    [SerializeField]
    protected bool doAction = false;

    [Range(1, 9)]
    public int position;

    public BonusData bonus;

    [HideInInspector]
    public SelectableObject userObject;

    [HideInInspector]
    public float coolDownTimer;

    private AudioSource audioSource;

    private Sound actionSound;
    private void Awake()
    {
        if (action.audioClip != null)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.playOnAwake = false;
            actionSound = new Sound();
            actionSound.SetAudioSource(audioSource);
            actionSound.AddClip(action.audioClip);
        }
    }

    private void Start()
    {
        if (action.audioClip != null)
        {
            actionSound.SetAudioMixerGroup(AudioManager.getAudioMixer());
        }
    }

    protected virtual void Update ()
    {
        coolDownTimer += Time.deltaTime;
    }

    /**
     * Use for check if the action can be done automatically
     */
    public virtual void DoActionAutomatically()
    {
        // It's up to the children to determine what to do
    }

    public virtual void SetupAction(SelectableObject obj)
    {
        this.userObject = obj;
    }

    /**
     * Allow the custom action to use specific pointer.
     * 
     * Return null to keep the default pointer.
     */
    public virtual Texture2D ManagerPointer(GameObject obj, Player player)
    {
        return action.actionPointer;
    }

    /**
     *  Execute the action as one shot
     */ 
    public virtual void ExecuteAction(SelectableObject obj)
    {
        // Up to child
        StartCoroutine(DoActionFx());
        actionSound.PlaySound();
    }

    protected virtual IEnumerator DoActionFx()
    {
        // Up to child to determine how to use the action Fx
        yield return null;
    }

    public virtual void StopAction()
    {
        this.doAction = false;
        if (actionSound != null)
            actionSound.StopSound();
    }

    public string GetActionName()
    {
        return action.actionName;
    }

    public CustomActionName GetActionNameData()
    {
        return action.actionNameData;
    }

    public Sprite GetImage()
    {
        return action.image;
    }

    public ActionType GetActionType()
    {
        return action.type;
    }

    public string GetDescription()
    {
        return action.description;
    }

    public List<CustomActionName> GetRequirements()
    {
        return action.requirements;
    }

    public KeyCode GetShortcut()
    {
        return action.shortcut;
    }

    public void SetIsDoing(bool v)
    {
        isDoingThisAction = v;
    }

    public Bonus GetBonus()
    {
        if (bonus != null)
            return new Bonus(bonus);
        else
            return null;
    }

    public GameObject GetActionFx()
    {
        return action.actionFx;
    }

    public float GetActionFxDuration()
    {
        return action.actionFxDuration;
    }

    public virtual bool CanDoAction()
    {
        return true;
    }

    public float GetActionDuration()
    {
        return action.actionDuration;
    }
}
