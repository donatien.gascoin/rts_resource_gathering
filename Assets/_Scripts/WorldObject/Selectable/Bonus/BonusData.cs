﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "Bonus", menuName = "Selectable object/Bonus")]
public class BonusData : ScriptableObject
{
    public string bonusName;
    public BonusEffects bonusEffect;

    public bool isPercentageEffect;

    [Tooltip("Effect as a percentage, up to 500%")]
    public float effectValuePercentage;
    [Tooltip("Effect as a number")]
    public float effectValueNumber;

    [Tooltip("In seconds")]
    public float duration;

    public Sprite bonusImage;

    public GameObject bonusFx;
}

#if UNITY_EDITOR
[CustomEditor(typeof(BonusData))]
public class BonusDataEditor : Editor
{
    override public void OnInspectorGUI()
    {
        var data = target as BonusData;

        data.bonusName = EditorGUILayout.TextField("Bonus name", data.bonusName);

        data.bonusEffect = (BonusEffects)EditorGUILayout.EnumPopup("Bonus effect", data.bonusEffect);

        data.isPercentageEffect = GUILayout.Toggle(data.isPercentageEffect, "Is a percentage effect");

        if (data.isPercentageEffect)
        {
            data.effectValuePercentage = EditorGUILayout.Slider("Value", data.effectValuePercentage, 0, 500);

        } else
        {
            data.effectValueNumber = EditorGUILayout.FloatField("Value", data.effectValueNumber);
        }

        data.duration = EditorGUILayout.FloatField("Effect duration", data.duration);

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Bonus Image");
        data.bonusImage = (Sprite)EditorGUILayout.ObjectField(data.bonusImage, typeof(Sprite), allowSceneObjects: false);

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Bonus FX");
        data.bonusFx = (GameObject)EditorGUILayout.ObjectField(data.bonusFx, typeof(GameObject), allowSceneObjects: false);
        
        EditorGUILayout.EndHorizontal();

        EditorUtility.SetDirty(data);
    }
}

#endif