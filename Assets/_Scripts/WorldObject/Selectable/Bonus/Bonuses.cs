﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonuses
{
    List<Bonus> bonuses;

    private float checkTime = 0f;
    private float currentCheckTime = 1f;
    private SelectableObject selectableObject;

    internal List<Bonus> GetAllBonus()
    {
        return bonuses;
    }

    // Start is called before the first frame update
    public Bonuses(SelectableObject obj)
    {
        bonuses = new List<Bonus>();
        selectableObject = obj;

    }

    // Update is called once per frame
    public void CheckBonuses()
    {
        if (currentCheckTime >= checkTime)
        {
            currentCheckTime = 0f;
            if (bonuses.Count == 0)
                return;

            float deltaTime = Time.deltaTime;
            for (int i = bonuses.Count -1 ; i >= 0; i--)
            {
                bonuses[i].AddEffectTime(deltaTime);
                if (!bonuses[i].IsEffectStillValid())
                {
                    RemoveFxToUnit(bonuses[i]);
                    bonuses[i].RemoveBonus(selectableObject);
                    bonuses.RemoveAt(i);
                }
            }
        }
        currentCheckTime += Time.deltaTime;
    }

    public void AddBonus(Bonus bonus)
    {
        if (!BonusIsPresent(bonus.GetBonusEffects()))
        {
            bonuses.Add(bonus);
            AddFxToUnit(bonus);
            bonus.ApplyBonus(selectableObject);
        } else
        {
            // Replace existing one
            for (int i = bonuses.Count - 1; i >= 0; i--)
            {
                if (bonuses[i].GetBonusEffects().Equals(bonus.GetBonusEffects()))
                {
                    ReplaceBonusFx(bonuses[i], bonus);
                    bonuses[i] = bonus;
                    break;
                }
            }
            // bonuses.Add(bonus);
        }
    }

    /*public void AddAndApplyBonus(Bonus bonus)
    {
        if (FindBonus(bonus.GetBonusEffects()) != null)
            return;

        AddBonus(bonus);
        bonus.ApplyBonus(selectableObject);
    }*/

    public bool IsEffect(BonusEffects effect)
    {
        return FindBonus(effect) != null;
    }

    public Bonus GetBonus(BonusEffects effect)
    {
        return FindBonus(effect);
    }

    private bool BonusIsPresent(BonusEffects bonusEffect)
    {
        return FindBonus(bonusEffect) != null ? true : false;
    }

    private Bonus FindBonus(BonusEffects effect)
    {
        for (int i = 0; i < bonuses.Count; i++)
            if (bonuses[i].GetBonusEffects().Equals(effect))
                return bonuses[i];
        return null;
    }

    private void AddFxToUnit(Bonus bonus)
    {
        bonus.fx = selectableObject.AddBonusFx(bonus.GetFx());
    }

    private void RemoveFxToUnit(Bonus bonus)
    {
        selectableObject.RemoveBonusFx(bonus.fx);
    }

    private void ReplaceBonusFx(Bonus oldBonus, Bonus newBonus)
    {
        newBonus.fx = oldBonus.fx;
    }
}
