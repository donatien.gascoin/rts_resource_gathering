﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BonusEffects
{
    HEAL,
    DEFENSE,
    ATTACK,
    SLOW_SPEED
}
