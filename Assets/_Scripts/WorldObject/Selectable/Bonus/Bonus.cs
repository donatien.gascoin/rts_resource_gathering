﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus
{
    public BonusData data;
    public float effectTime;
    public GameObject fx;

    public Bonus(BonusData data)
    {
        this.data = data;
        effectTime = 0f;
    }

    public string GetBonusName()
    {
        return data.bonusName;
    }

    public BonusEffects GetBonusEffects()
    {
        return data.bonusEffect;
    }

    public Sprite GetBonusImage()
    {
        return data.bonusImage;
    }

    public float GetEffectDuration()
    {
        return data.duration;
    }

    /**
     * Will return a percentage or a number, depending on the bonus effect (isPercentageEffect)
     */
    public float GetEffectValue()
    {
        return data.isPercentageEffect ? GetEffectValueInPercentage() : data.effectValueNumber;
    }

    private float GetEffectValueInPercentage()
    {
        return data.effectValuePercentage / 100;
    }

    public float GetEffectTime()
    {
        return effectTime;
    }

    public void AddEffectTime(float time)
    {
        effectTime += time;
    }

    public bool IsEffectStillValid()
    {
        return GetEffectTime() < GetEffectDuration();
    }

    public GameObject GetFx()
    {
        return data.bonusFx;
    }

    public virtual void ApplyBonus(SelectableObject o)
    {
        // It's up to child to determine how to apply
        Debug.Log("Something wrong with your bonus, a child must handle that");
    }

    public virtual void RemoveBonus(SelectableObject o)
    {
        // It's up to child to determine how to apply
        Debug.Log("Something wrong with your bonus, a child must handle that");
    }
}
