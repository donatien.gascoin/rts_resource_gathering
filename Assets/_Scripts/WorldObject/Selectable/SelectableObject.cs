using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(AudioSource))]
public class SelectableObject : WorldObject
{
    [Header("Selectable object")]
    public CustomActionName actionName;
    [Tooltip("0 if you want your object to have the maximum hit point at start")]
    public float hitPoints = 0;
    [Tooltip("0 if you want your object to have the maximum mana point at start")]
    public float manaPoints = 0;

    public ObjectStat objectStats;
    public float deathTiming;
    [Space(10)]
    public Behaviour behaviour;

    /*[Space(10)]
    [Tooltip("Up to 9 actions")]*/
    [HideInInspector]
    public CustomActionObject[] actions;

    [Space(10)]
    public NavMeshAgent agent;
    public Animator animator;
    public MiniMap miniMap;
    public List<Renderer> partsToColor;
    public Sounds sounds;

    [HideInInspector]
    protected AudioSource audioSource { get { return GetComponent<AudioSource>(); } }

    protected float lastDamagesTime = -1;
    private float currentHealthTime;
    private float currentManaTime;

    [HideInInspector]
    public Player player;

    protected float timeSinceLastDecision = 0.0f, timeBetweenDecisions = 0.1f;
    protected SelectableObject target = null;

    // Unit priority in the UI management (less the number, more the priority)
    [HideInInspector]
    public int uiNumber;
    protected float healthPercentage = 1.0f;
    [SerializeField]
    protected bool attacking = false, movingIntoPosition = false, aiming = false, dead = false;

    protected bool currentlySelected = false;
    protected List<SelectableObject> nearbyObjects;
    protected float currentWeaponChargeTime;

    // This V3 allow the unit to remember the position it need to defend when it is in defensive behaviour
    protected Vector3 positionToDefend;

    // [HideInInspector]
    public Bonuses bonuses;

    private CustomActionObject moveAction;
    private CustomActionObject aggressiveAction;

    #region Monobehavior methods

    protected override void Awake()
    {
        ChooseObjectStat().Init();
        if (hitPoints == 0)
            hitPoints = GetStats().maxHitPoint;
        if (hitPoints < GetStats().maxHitPoint)
            lastDamagesTime = Time.time;

        if (manaPoints == 0)
            manaPoints = GetStats().maxManaPoint;
        CheckHitPoints();
        if (behaviour.Equals(Behaviour.DEFENSIVE))
            positionToDefend = transform.position;
        else
            positionToDefend = ResourceManager.InvalidPosition;

    }

    protected override void Start()
    {
        base.Start();
        currentWeaponChargeTime = GetStats().weaponRechargeTime;
        actions = GetComponentInChildren<DefaultActions>().GetActions();
        for (int i = 0; i < actions.Length; i++)
        {
            if (actions[i] != null)
            {
                actions[i].SetupAction(this);

                if (CustomActionName.MOVE.Equals(actions[i].GetActionNameData()))
                    moveAction = actions[i];
                else if (CustomActionName.AGGRESSIVE.Equals(actions[i].GetActionNameData()))
                    aggressiveAction = actions[i];
            }

        }

        bonuses = new Bonuses(this);
        sounds.InitSoundMixer(audioSource);
    }

    protected override void Update()
    {
        base.Update();

        bonuses.CheckBonuses();

        if (ShouldMakeDecision()) DecideWhatToDo();
        currentWeaponChargeTime += Time.deltaTime;
        // Allwo, if the target move, to update the destination
        if (movingIntoPosition)
        {
            MoveToPosition();
        }
        if (target != null && attacking && !movingIntoPosition && !aiming)
        {
            PerformAttack();
        }
        if (target == null && attacking)
        {
            attacking = false;
        }
        if (GetStats().healthRegeneration && lastDamagesTime >= 0 && Time.time - lastDamagesTime >= GetStats().healthRegenerationDelay)
        {
            RegenerateHealth();
        }

        RegenerateMana();
    }

    #endregion

    #region Decision engine

    public virtual bool MouseClick(GameObject hitObject, Vector3 hitPoint, Player controller, Behaviour behaviour)
    {
        MarkActionAsDoing(CustomActionName.NONE);
        bool hasDoneAction = false;
        //only handle input if currently selected
        if (currentlySelected && hitObject)
        {
            this.behaviour = behaviour;
            if (!WorkManager.ObjectIsGround(hitObject))
            {
                SelectableObject obj = hitObject.GetComponentInParent<SelectableObject>();
                //clicked on another selectable object
                if (obj && !obj.IsInvulnerable() && obj.CanBeAttackedBy(this))
                {
                    if (Behaviour.AGGRESSIVE.Equals(behaviour) && ObjectId != obj.ObjectId)
                    {
                        // Specific behaviour, force the unit to attack, no matter who
                        obj.HighlightClicked();
                        BeginAttack(obj);
                        hasDoneAction = true;
                        PlaySound(SoundCategorie.ATTACK);
                    } else if (player.team != obj.player.team && CanAttack())
                    {
                        obj.HighlightClicked();
                        BeginAttack(obj);
                        hasDoneAction = true;
                        PlaySound(SoundCategorie.ATTACK);
                    }
                }
            }
        }
        return hasDoneAction;
    }

    /**
	 * A child class should only determine other conditions under which a decision should
	 * not be made. This could be 'harvesting' for a harvester, for example. Alternatively,
	 * an object that never has to make decisions could just return false.
	 */
    protected virtual bool ShouldMakeDecision()
    {
        if (!IsDead() && timeSinceLastDecision > timeBetweenDecisions)
        {
            //we are not doing anything at the moment
            if (Behaviour.DEFENSIVE.Equals(behaviour) && movingIntoPosition)
            {
                // We are tring to reach the target, but in defensive state, we need to check if we are not goind to far from our positionToDefend point
                timeSinceLastDecision = 0.0f;
                return true;
            }
            else if (Behaviour.FOLLOWING.Equals(behaviour))
            {
                timeSinceLastDecision = 0.0f;
                return true;
            } 
            else if (!attacking && !movingIntoPosition && !aiming)
            {
                timeSinceLastDecision = 0.0f;
                return true;
            }
        }
        timeSinceLastDecision += Time.deltaTime;
        return false;
    }

    protected override void DecideWhatToDo()
    {
        //determine what should be done by the world object at the current point in time
        Vector3 currentPosition = transform.position;
        if (CanAttack())
        {
            switch(behaviour)
            {
                case Behaviour.NORMAL:
                case Behaviour.AGGRESSIVE:
                    FindNearestEnemy();
                    break;
                case Behaviour.DEFENSIVE:
                    // If the unit is still in the defensive range
                    if (Vector3.Distance(positionToDefend, currentPosition) - GetStats().detectionRange < 0)
                    {
                        if (target != null)
                            BeginAttack(target);
                        else
                            FindNearestEnemy();
                    } else
                    {
                        if (target != null)
                        {
                            ResetUnitState();
                            StartMove(positionToDefend);
                        }
                    }
                    break;
            }
        }
    }

    protected void FindNearestEnemy()
    {
        Vector3 currentPosition = transform.position;
        nearbyObjects = WorkManager.FindNearbyObjects(currentPosition, GetStats().detectionRange);
        List<SelectableObject> enemyObjects = new List<SelectableObject>();
        foreach (SelectableObject nearbyObject in nearbyObjects)
        {
            if (nearbyObject.IsInvulnerable()) continue;
            if (!nearbyObject.CanBeAttackedBy(this)) continue;
            Resource resource = nearbyObject.GetComponent<Resource>();
            if (resource) continue;
            Building building = nearbyObject.GetComponent<Building>();
            if (building != null && building.IsCurrentlyPlacingOnField()) continue;
            if (nearbyObject.player != player) enemyObjects.Add(nearbyObject);
        }
        SelectableObject closestObject = WorkManager.FindNearestWorldObjectInListToPosition(WorkManager.FilterObjectToAttackInListToPosition(enemyObjects), currentPosition);
        if (closestObject) BeginAttack(closestObject);
    }

    /**
     * Left click on an action
     */
    public virtual void PerformLeftClickAction(CustomActionName actionToPerform)
    {
        // It is up to children with specific actions to determine what to do with each of those actions

        // Default actions behaviour
        switch(actionToPerform)
        {
            case CustomActionName.MOVE:
                player.SetCustomActionClick(moveAction, UserAction.SQUAD_ACTION);
                player.squad.ChangeSquadNextClickBehaviour(Behaviour.NORMAL);
                break;
            case CustomActionName.AGGRESSIVE:
                player.SetCustomActionClick(aggressiveAction, UserAction.SQUAD_ACTION);
                player.squad.ChangeSquadNextClickBehaviour(Behaviour.AGGRESSIVE);
                break;
            case CustomActionName.DEFENSE:
                player.squad.SetBehaviourToSquad(Behaviour.DEFENSIVE);
                PlaySound(SoundCategorie.ACCEPT);
                MarkActionAsDoing(CustomActionName.DEFENSE);
                player.StopCustomAction();
                break;
        }
    }

    /**
     * Right click on an action
     */
    public virtual void PerformRightClickAction(CustomActionName actionToPerform)
    {
        // It is up to children with specific actions to determine what to do with each of those actions
    }

    /**
     * This method allow children to say if they are currently doing something, or if they're up to do a custom action.
     * 
     */
    public virtual bool CanDoAction(CustomActionName actionName)
    {
        // It's up to children to determine how to handleswitch (actionName)
        switch (actionName)
        {
            case CustomActionName.MOVE:
            case CustomActionName.AGGRESSIVE:
                return true;
        }
        return false;
    }

    /**
     * Execute the custom action given by the player
     */
    public virtual void ExecuteAction(CustomActionName actionName, Vector3 hitPoint, GameObject hitObject)
    {
        // It's up to children to determine how to handle this action
        switch (actionName)
        {
            case CustomActionName.MOVE:
                player.squad.RightClick(hitPoint, hitObject, Behaviour.NORMAL);
                break;
            case CustomActionName.AGGRESSIVE:
                player.squad.RightClick(hitPoint, hitObject, Behaviour.AGGRESSIVE);
                break;
        }
    }


    /**
     * Determine children behavior with right click on an action
     */
    public virtual void ExecuteRightClickOnAction(CustomActionName actionName)
    {
        // It's up to children to determine how to handle this action
    }

    #endregion

    #region Setter (player/selection/materials)

    private void MarkActionAsDoing(CustomActionName name)
    {
        for (int i = 0; i < actions.Length; i++)
        {
            if (actions[i] == null)
                continue;
            if(actions[i].GetActionNameData().Equals(name))
                actions[i].SetIsDoing(true);
            else
                actions[i].SetIsDoing(false);
        }
    }

    public void PlaySound(SoundCategorie categorie, bool checkIfUnitIsSelected = true, bool forceSound = false)
    {
        sounds.PlaySound(this, audioSource, categorie, checkIfUnitIsSelected, forceSound);
    }

    public void RegenerateHealth()
    {
        currentHealthTime += Time.deltaTime;
        if (currentHealthTime >= GetStats().healthRegenerationRate)
        {
            currentHealthTime = 0;
            Heal(GetStats().healthRegenerationValue);
        }
    }

    public void RegenerateMana()
    {
        currentManaTime += Time.deltaTime;
        if (currentManaTime >= GetStats().healthRegenerationRate)
        {
            currentManaTime = 0;
            AddMana(GetStats().healthRegenerationValue);
        }
    }

    protected virtual void MoveToPosition()
    {
        attacking = AdjustPosition(target);
    }

    public virtual void CheckUpgrade() {

        if (UpgradeType.NONE.Equals(ChooseObjectStat().GetUpgradeType()))
            return;

        if (ChooseObjectStat().currentLvl < player.GetUpgradeStatus(ChooseObjectStat().GetUpgradeType()).currentLvl)
        {
            ChooseObjectStat().UpgradeObjectStats();
            hitPoints = ChooseObjectStat().CalculeNewHitPointValue(hitPoints);
            if (agent != null)
                agent.speed = ChooseObjectStat().GetStats().speed;
        }
    }

    public virtual void SetPlayer(Player p)
    {
        player = p;
        if (miniMap != null)
        {
            miniMap.SetPlayer(player);
        }
        UpdateFOW(p.team, GetStats().detectionRange);
    }

    public virtual void SetSelection(bool v)
    {
        /*Debug.LogException(new Exception("What are you doing here ?"));*/
        currentlySelected = v;
        selectedMarks.SetActive(v);
    }

    public virtual void SetMaterials()
    {
        if (player != null && miniMap != null)
        {
            miniMap.SetPlayer(player);
        }
    }

    public virtual void SetObstacle(bool enable) { }

    public virtual void SetColliders(bool enable) { }

    protected void TurnUnitToFaceObject(Vector3 position)
    {
        float angle = ResourceManager.GetAngleToFaceObject(position, transform);
        transform.rotation = Quaternion.Euler(transform.rotation.x, angle, transform.rotation.z);
    }

    public virtual void StartMove(Vector3 destination)
    {
    }

    public virtual void StartMove(Vector3 destination, GameObject destinationTarget)
    {
    }

    protected virtual void ResetUnitState()
    {
        // currentWeaponChargeTime = GetStats().weaponRechargeTime;
        attacking = false;
        movingIntoPosition = false;
        aiming = false;
        target = null;
        // behaviour = Behaviour.NORMAL;
        /*if (animator != null)
        {
            animator.SetBool("Attacking", false);
        }*/
    }

    public void SetBehaviour(Behaviour b)
    {
        this.behaviour = b;
        switch(this.behaviour)
        {
            case Behaviour.DEFENSIVE:
                positionToDefend = transform.position;
                MarkActionAsDoing(CustomActionName.DEFENSE);
                break;
        }
    }

    #endregion

    #region Getter

    internal bool IsSameTeam(SelectableObject obj)
    {
        if (obj == null || obj.player == null)
            return false;

        return player.team == obj.player.team;
    }

    public virtual void CheckHitPoints() { }

    public bool IsFirstUnitSelected()
    {
        SelectableObject obj = player.squad.GetFirstSquadUnitInUnitsSelectedOnUI();
        if (obj == null)
        {
            return false;
        }
        return this.ObjectId == obj.ObjectId;
    }

    public bool IsCurrentlySelected()
    {
        return currentlySelected;
    }

    public virtual bool CanDoActions()
    {
        return true;
    }

    public Stats GetStats()
    {
        return ChooseObjectStat().GetStats();
    }

    public virtual ObjectStat ChooseObjectStat()
    {
        return objectStats;
    }

    public bool IsInvulnerable()
    {
        return ChooseObjectStat().IsInvulnerable();
    }

    public AttackType GetAttackType()
    {
        return ChooseObjectStat().GetAttackType();
    }

    public bool IsDead()
    {
        return dead;
    }

    public bool IsHumanPlayer()
    {
        if (!player)
            return false;
        return player.human;
    }

    public override bool CanBeAttackedBy(SelectableObject attacker)
    {
        // It's up to the chil to determine if there can't be attacked by anyone
        // Example: Trees can only be attacked by siege weapon
        return true;
    }

    #endregion

    #region Attack

    protected Vector3 FindNearestAttackPosition(SelectableObject t)
    {
        if (t != null)
        {
            Collider c = t.GetComponent<Collider>();
            Vector3 pos = ResourceManager.InvalidPosition;
            Quaternion rot = Quaternion.identity;
            if (c)
            {
                pos = c.transform.position;
                rot = c.transform.rotation;
            }

            Vector3 targetLocation = t.transform.position;
            Vector3 direction = targetLocation - transform.position;
            float targetDistance = direction.magnitude;
            float distanceToTravel = targetDistance - (0.9f * GetStats().weaponRange);
            if (c)
                return Vector3.Lerp(transform.position, Physics.ClosestPoint(this.transform.position, c, pos, rot), distanceToTravel / targetDistance);
            else
                return Vector3.Lerp(transform.position, targetLocation, distanceToTravel / targetDistance);
        }
        else
        {
            return transform.position;
        }
    }

    protected Vector3 FindNearestAttackPosition(Vector3 position)
    {
        if (position != ResourceManager.InvalidPosition)
        {

            Vector3 targetLocation = position;
            Vector3 direction = targetLocation - transform.position;
            float targetDistance = direction.magnitude;
            float distanceToTravel = targetDistance - (0.9f * GetStats().weaponRange);
            return Vector3.Lerp(transform.position, targetLocation, distanceToTravel / targetDistance);
        }
        else
        {
            return transform.position;
        }
    }

    protected void BeginAttack(SelectableObject target)
    {
        this.target = target;
        if (TargetInRange(this.target))
        {
            attacking = true;
            PerformAttack();
        }
        else attacking = AdjustPosition(target);
    }

    protected void PerformAttack()
    {
        if (target == null || target.IsDead())
        {
            attacking = false;
            return;
        }
        if (!TargetInRange(target)) attacking = AdjustPosition(target);
        AimAtTarget(target);
        if (ReadyToFire()) UseWeapon();
    }

    protected bool TargetInRange(SelectableObject t)
    {
        if (t != null)
        {
            Vector3 targetLocation = t.transform.position;
            Collider c = t.GetComponent<Collider>();
            if (c)
            {
                Vector3 pos = c.transform.position;
                Quaternion rot = c.transform.rotation;
                targetLocation = Physics.ClosestPoint(this.transform.position, c, pos, rot);
            }

            Vector3 direction = targetLocation - transform.position;
            if (direction.sqrMagnitude < GetStats().weaponRange * GetStats().weaponRange)
            {
                Unit self = this as Unit;
                if (self) self.StopMoving();
                return true;
            }
            return false;
        }
        else
        {
            return false;
        }
    }

    protected bool TargetInRange(Vector3 position)
    {
        if (position != ResourceManager.InvalidPosition)
        {
            Vector3 targetLocation = position;

            Vector3 direction = targetLocation - transform.position;
            if (direction.sqrMagnitude < GetStats().weaponRange * GetStats().weaponRange)
            {
                Unit self = this as Unit;
                if (self) self.StopMoving();
                return true;
            }
            return false;
        }
        else
        {
            return false;
        }
    }

    protected bool AdjustPosition(SelectableObject t)
    {
        Unit self = this as Unit;
        if (self)
        {
            // If the unit is still in the defensive range
            if (behaviour.Equals(Behaviour.DEFENSIVE)) {
                if (Vector3.Distance(positionToDefend, transform.position) - GetStats().detectionRange < 0 )
                {
                    movingIntoPosition = true;
                    Vector3 attackPosition = FindNearestAttackPosition(t);
                    self.StartMove(attackPosition);
                } else
                {
                    ResetUnitState();
                    StartMove(positionToDefend);
                }
            } 
            else
            {
                movingIntoPosition = true;
                Vector3 attackPosition = FindNearestAttackPosition(t);
                self.StartMove(attackPosition);
            }
            return true;
        }
        else return false;
    }

    protected bool AdjustPosition(Vector3 position)
    {
        Unit self = this as Unit;
        if (self)
        {
            movingIntoPosition = true;
            Vector3 attackPosition = FindNearestAttackPosition(position);
            self.StartMove(attackPosition);
            return true;
        }
        else return false;
    }

    protected bool ReadyToFire()
    {
        if (currentWeaponChargeTime >= GetStats().weaponRechargeTime) return true;
        return false;
    }

    protected virtual void AimAtTarget(SelectableObject target)
    {
        aiming = true;
        //this behaviour needs to be specified by a specific object
    }

    protected virtual void AimAtTarget(Vector3 position)
    {
        aiming = true;
        //this behaviour needs to be specified by a specific object
    }

    protected virtual void UseWeapon()
    {
        //this behaviour needs to be specified by a specific object
        currentWeaponChargeTime = 0.0f;
        /*if (audioElement != null && Time.timeScale > 0) audioElement.Play(useWeaponSound);*/
    }

    public override void TakeDamage(float damage, SelectableObject attacker)
    {
        if (damage <= 0)
        {
            Debug.Log("Use Heal() to add hitPoint to object, nothing will be done here");
            return;
        }
        hitPoints = hitPoints - damage;
        hitPoints = Mathf.Clamp(hitPoints, 0, GetStats().maxHitPoint);
        if (hitPoints == 0)
        {
            // attacker.GiveExp(objectStats.GetStats().exp);
            dead = true;
            ObjectDeath();
            return;
        }

        lastDamagesTime = Time.time;

        if (attacker != null && attacker.player != player && (target == null || !attacking) && ShouldMakeDecision())
        {
            target = attacker;
            attacking = true;
        }
    }

    public virtual void Heal(float heal)
    {
        if (hitPoints == 0)
        {
            return;
        }

        hitPoints = hitPoints + heal;
        hitPoints = Mathf.Clamp(hitPoints, 0, GetStats().maxHitPoint);
        CheckHitPoints();
        if (hitPoints == GetStats().maxHitPoint)
            lastDamagesTime = -1;
    }

    private void GiveExp(int exp)
    {
        throw new NotImplementedException();
    }

    #endregion

    #region Mana 

    public virtual void AddMana(float additionalMana)
    {
        manaPoints = manaPoints + additionalMana;
        manaPoints = Mathf.Clamp(manaPoints, 0, GetStats().maxManaPoint);
    }

    public bool ConsumeMana(int manaRequired)
    {
        if (!GetStats().useMana)
        {
            Debug.Log("Unit " + objectName + " does not use mana ! ");
            return false;
        }
        if (manaPoints - manaRequired < 0)
            return false;

        manaPoints -= manaRequired;

        return true;
    }

    #endregion

    #region Bonuses
      
    /**
     * Bonus can be either positive (attack improvement) and negative (slow speed)
     */
    public void AddBonus(Bonus bonus, bool applyDirectly = false)
    {
        if (applyDirectly)
            bonuses.AddBonus/*AddAndApplyBonus*/(bonus);
        else
            bonuses.AddBonus(bonus);
    }

    public void RemoveBonusFx(GameObject fx)
    {
        Destroy(fx);
    }

    public GameObject AddBonusFx(GameObject fx)
    {
        if (fx != null)
            return Instantiate(fx, new Vector3(transform.position.x, fx.transform.position.y, transform.position.z), fx.transform.rotation, transform);
        
        return null;
    }

    public Bonus GetBonus(BonusEffects effect)
    {
        return bonuses.GetBonus(effect);
    }

    public bool HaveBonus(BonusEffects effect)
    {
        return bonuses.GetBonus(effect) != null;
    }

    #endregion

    #region Death

    protected override void ObjectDeath()
    {
        StartCoroutine(HideFromUI());
        ResetUnitState();
        SetObstacle(false);
        SetColliders(false);
        hitPoints = 0;
        if (agent != null)
            agent.SetDestination(transform.position);
        Destroy(gameObject, deathTiming);
        if (animator != null)
        {
            System.Random rand = new System.Random();
            if (rand.NextDouble() >= 0.5)
                animator.SetTrigger("Death");
            else
                animator.SetTrigger("Death2");
        }
    }

    IEnumerator HideFromUI()
    {
        yield return new WaitForSeconds(deathTiming / 2);
        // If the human is currently selecting this unit
        UIManager.removeUnitFromUI(this);

        player.squad.RemoveSquadUnit(this);
    }

    #endregion

    public override bool Equals(object obj)
    {
        return obj is SelectableObject @object &&
               base.Equals(obj);
    }
}
