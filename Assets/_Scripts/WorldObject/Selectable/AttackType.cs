﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum AttackType
{
    MELEE, RANGED, MAGIC, SIEGE, NONE
}
