﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneGate : MonoBehaviour
{
    public Animator gateAnimator;

    public Collider detectionCollider;

    [SerializeField]
    private int nbUnitsInFrontOfTheDoor = 0;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag(DataManager.GROUND_TAG))
        {
            SelectableObject obj = other.GetComponent<SelectableObject>();
            if (obj != null)
            {
                nbUnitsInFrontOfTheDoor++;
                CheckDoorStatus();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag(DataManager.GROUND_TAG))
        {
            SelectableObject obj = other.GetComponent<SelectableObject>();
            if (obj != null)
            {
                nbUnitsInFrontOfTheDoor--;
                CheckDoorStatus();
            }
        }
    }

    private void CheckDoorStatus()
    {
        if(nbUnitsInFrontOfTheDoor > 0)
        {
            gateAnimator.SetBool("Open", true);
        } else
        {
            gateAnimator.SetBool("Open", false);
        }
    }
}
