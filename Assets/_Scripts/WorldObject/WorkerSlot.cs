﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkerSlot : MonoBehaviour
{
    public Transform waitingLimit;
    public float waitingRadius;

    public WorkerQueue queue;
    public Transform resourcePosition;


    void Start()
    {
        queue = new WorkerQueue();
    }

    void Update()
    {

    }

    public bool IsUnitOnBounds(Unit unit)
    {
        Collider[] hitColliders = Physics.OverlapSphere(waitingLimit.position, waitingRadius);
        int i = 0;
        while (i < hitColliders.Length)
        {
            Unit unitInRadius = hitColliders[i].GetComponentInParent<Unit>();
            if (unitInRadius != null)
            {
                if (unitInRadius.ObjectId == unit.ObjectId)
                {
                    return true;
                }
            }
            i++;
        }
        return false;
    }
    public bool CanUseResource(Unit u)
    {
        return queue.IsUnitFirstInQueue(u);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(waitingLimit.position, waitingRadius);
    }
}
