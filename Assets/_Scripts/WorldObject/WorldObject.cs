﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldObject : MonoBehaviour
{

    [Header("World Object")]
    public string objectName = "WorldObject";
    public Sprite image;

    # region Data related to the construction of this object

    protected float currentConstructionTime = 0f;

    #endregion
    protected Bounds selectionBounds;
    [HideInInspector]
    public int ObjectId;

    // Allow to show/hide maks such as life bar, or selection circle
    public GameObject selectedMarks;

    [Header("Fog of war")]
    public FoW.FogOfWarUnit fogOfWar;

    public GameObject[] partToHideInFog;

    protected virtual void Awake()
    {
        selectionBounds = ResourceManager.InvalidBounds;
        CalculateBounds();
        UpdateFOW(0, 0);
        
    }

    protected virtual void Start()
    {
    }

    protected virtual void Update()
    {
    }

    public virtual bool CanAttack()
    {
        //default behaviour needs to be overidden by children
        return false;
    }
    public virtual bool CanBeAttackedBy(SelectableObject attacker)
    {
        // It's up to the chil to determine if there can't be attacked by anyone
        return true;
    }

    protected virtual void DecideWhatToDo() { }

    public virtual bool CanBeSelected() {
        return true;
    }

    protected virtual void ObjectDeath(){}

    public void CalculateBounds()
    {
        selectionBounds = new Bounds(transform.position, Vector3.zero);
        foreach (Renderer r in GetComponentsInChildren<Renderer>())
        {
            selectionBounds.Encapsulate(r.bounds);
        }
    }
    public virtual void TakeDamage(float damage, SelectableObject attacker)
    {
    }

    public void HighlightClicked()
    {
        if (selectedMarks != null)
        {
            StartCoroutine(ResourceManager.HighlightClickedCoroutine(selectedMarks));
            
        }
    }

    public override bool Equals(object obj)
    {
        return obj is WorldObject @object &&
               ObjectId == @object.ObjectId;
    }

    /*protected void SetUnitPosition()
    {
        transform.position = new Vector3(transform.position.x, 0, transform.position.z);
    }*/

    protected void UpdateFOW(int team, float sightRadius)
    {
        // Resource team : 0
        if (fogOfWar != null)
        {
            fogOfWar.team = team;
            fogOfWar.circleRadius = sightRadius;
        }
    }
}

[Serializable]
public class AssociatedWeapon
{
    public GameObject weapon;
    public string name;
}