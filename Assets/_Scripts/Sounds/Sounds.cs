﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


[CreateAssetMenu(fileName = "Sounds", menuName = "Selectable object/Sound/SoundsManager")]
public class Sounds: ScriptableObject
{
    public Sound[] sounds;

    private SoundCategorie previousSound;

    public void InitSoundMixer(AudioSource source)
    {
        source.outputAudioMixerGroup = AudioManager.getAudioMixer();
    }

    public void PlaySound(SelectableObject _object, AudioSource source, SoundCategorie soundName)
    {
        PlaySound(_object, source, soundName, true);

    }

    public void PlaySound(SelectableObject _object, AudioSource source, SoundCategorie soundName, bool checkIfOnUI = false)
    {
        PlaySound(_object, source, soundName, checkIfOnUI, false);
    }

    public void PlaySound(SelectableObject _object, AudioSource source, SoundCategorie soundName, bool checkIfOnUI = false, bool interruptCurrentClip = true)
    {
        Sound s = Array.Find(sounds, sounds => sounds.soundCategorie == soundName);
        if (s == null)
        {
            Debug.LogWarning("Sounds " + soundName + " not found !");
            return;
        }

        if (!source.isPlaying || soundName != previousSound || interruptCurrentClip)
        {
            if (!checkIfOnUI || (!_object.IsDead() && _object.IsFirstUnitSelected()))
            {
                // Debug.Log("Play sound " + soundName);
#if UNITY_EDITOR
                if (s.volume == 0 || s.pitch == 0)
                {
                    Debug.LogWarning(soundName + " : The volume or the pitch has been set to 0");
                }
#endif
                source.volume = s.volume /** (1f + UnityEngine.Random.Range(-s.volumeVariance / 2f, s.volumeVariance / 2f))*/;
                source.pitch = s.pitch/* * (1f + UnityEngine.Random.Range(-s.pitchVariance / 2f, s.pitchVariance / 2f))*/;
                source.loop = s.loop;
                source.clip = s.GetNextClip();
                source.Play();
                previousSound = soundName;
            }
        }
    }

    public void PlaySound(AudioSource source, SoundCategorie soundName, bool interruptCurrentClip = false)
    {
        Sound s = Array.Find(sounds, sounds => sounds.soundCategorie == soundName);
        if (s == null)
        {
            Debug.LogWarning("Sounds " + soundName + " not found !");
            return;
        }

        if (!source.isPlaying || soundName != previousSound || interruptCurrentClip)
        {
            source.volume = s.volume /** (1f + UnityEngine.Random.Range(-s.volumeVariance / 2f, s.volumeVariance / 2f))*/;
            source.pitch = s.pitch/* * (1f + UnityEngine.Random.Range(-s.pitchVariance / 2f, s.pitchVariance / 2f))*/;
            source.loop = s.loop;
            source.clip = s.GetNextClip();
            source.Play();
            previousSound = soundName;
        }
    }
}
 