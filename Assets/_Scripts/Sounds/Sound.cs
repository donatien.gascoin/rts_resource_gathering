﻿using System;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class Sound 
{
    public SoundCategorie soundCategorie;
    public AudioClip[] clip;

    private AudioSource audioSource;

    [Range(0, 1)]
    public float volume = .75f;

    [Range(.1f, 3f)]
    public float pitch = 1f;

    public bool loop = false;

    private int nextPlayedClip = 0;

    public AudioClip GetNextClip()
    {
        if (clip == null || clip.Length == 0)
            return null;
        if (nextPlayedClip == clip.Length)
            nextPlayedClip = 0;

        AudioClip c = clip[nextPlayedClip];
        nextPlayedClip++;
        return c;
    }

    public void PlaySound()
    {
        audioSource.volume = volume;
        audioSource.pitch = pitch;
        audioSource.loop = loop;
        audioSource.clip = GetNextClip();
        audioSource.Play();
    }

    public void StopSound()
    {
        audioSource.Stop();
    }

    public void SetAudioSource(AudioSource source)
    {
        audioSource = source;
    }

    internal void AddClip(AudioClip audioClip)
    {
        if (clip == null)
        {
            clip = new AudioClip[1];
            clip[0] = audioClip;
        }
        else
        {
            AudioClip[] newClips = new AudioClip[clip.Length + 1];
            for (int i = 0; i < newClips.Length; i++)
            {
                if (i < newClips.Length -2)
                {
                    newClips[i] = clip[i];
                } else
                {
                    newClips[i] = audioClip;
                }
            }
        } 
    }

    public void SetAudioMixerGroup(AudioMixerGroup mixer)
    {
        if (audioSource != null)
            audioSource.outputAudioMixerGroup = mixer;
    }
}

[System.Serializable]
public enum SoundCategorie
{
    HELLO,
    ACCEPT,
    REFUSE,
    IMPOSSIBLE,
    ATTACK,
    UNDER_ATTACK,
    BUILDING_CONSTRUCTED,
    IMPOSSIBLE_TO_CONSTRUCT_HERE,
    CREATED,

    BUTTON_CLICK_VALIDATE,
    BUTTON_CLICK_CANCEL,
    BUTTON_HOVER,
    START_NEW_GAME,
    THEME,

    ADDITIONAL_GOLD_REQUIRED,
    ADDITIONAL_WOOD_REQUIRED,
    ADDITIONAL_FOOD_REQUIRED,

    UPGRADING,
    UPGRADE_COMPLETE,
    RESEARCH,
    RESEARCH_COMPLETE,

    NONE,

    QUEST_COMPLETED,
    QUEST_NEW
}