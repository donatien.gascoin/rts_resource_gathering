﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aura : MonoBehaviour
{
    public int damage = 1;
    public float destroyTime;

    private SelectableObject target;
    private SelectableObject attacker;
    private bool attack;

    void Start()
    {
        TargetReached();
    }

    public virtual void TargetReached()
    {
        if (attack)
            InflictDamage();
        else
            HealTarget();

        Destroy(gameObject, destroyTime);
    }

    public void SetProjectileData(int damage, bool attack)
    {
        this.damage = damage;
        this.attack = attack;
    }

    public void SetTarget(SelectableObject target, SelectableObject attacker)
    {
        this.target = target;
        this.attacker = attacker;
    }

    protected void InflictDamage()
    {
        if (target) target.TakeDamage(damage, attacker);
    }

    protected void HealTarget()
    {
        // if (target) target.Heal(damage);
    }
}
