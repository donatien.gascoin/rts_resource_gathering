﻿using UnityEngine;
using System.Collections;
using RTS_Cam;

[RequireComponent(typeof(RTS_Camera))]
public class TargetManager : MonoBehaviour 
{
    private RTS_Camera cam;
    private new Camera camera;
    public string targetsTag;

    private void Start()
    {
        cam = gameObject.GetComponent<RTS_Camera>();
        camera = gameObject.GetComponent<Camera>();
    }

    public delegate void TargetObject(SelectableObject obj);
    public static TargetObject targetObject;

    public delegate void ReleaseTarget();
    public static ReleaseTarget releaseTarget;

    private void Awake()
    {
        targetObject += Target;
        releaseTarget += Release;
    }

    private void OnDestroy()
    {
        targetObject -= Target;
        releaseTarget -= Release;
    }


    private void Target(SelectableObject obj)
    {
        cam.SetTarget(obj.transform);
    }

    private void Release()
    {
        cam.ResetTarget();
    }
    /*
        private void Update()
        {
            if(Input.GetMouseButtonDown(0))
            {
                Ray ray = camera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if(Physics.Raycast(ray, out hit))
                {
                    if (hit.transform.CompareTag(targetsTag))
                        cam.SetTarget(hit.transform);
                    else
                        cam.ResetTarget();
                }
            }
        }*/
}
