﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMapCrossBridgeOld : ReachPointQuest
{
    public GameObject[] units;
    public GameObject[] buildings;
    public override void QuestCompletedActions()
    {
        Player player = GameManager.getHumanPlayer();
        for (int i = 0; i < units.Length; i++)
        {
            Unit obj = units[i].GetComponent<Unit>();
            if (obj != null) { 
                obj.enabled = true;
                obj.SetPlayer(player);
                obj.HighlightClicked();
                obj.transform.parent = player.GetUnitsTransform();
            }
            else
            {
                Debug.LogError("A unit is not an unit");
            }
        }

        for (int i = 0; i < buildings.Length; i++)
        {
            Building obj = buildings[i].GetComponent<Building>();
            if (obj != null)
            {
                obj.enabled = true;
                obj.SetPlayer(player);
                obj.HighlightClicked();
                obj.transform.parent = player.GetBuildingsTransform();
            } else
            {
                Debug.LogError("A building is not a building");
            }
        }
    }
}
